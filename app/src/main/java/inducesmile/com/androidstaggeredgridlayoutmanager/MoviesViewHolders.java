package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bulletnoid.android.widget.StaggeredGridViewDemo.STGVImageView;
import com.stux.R;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;

public class MoviesViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    LinearLayout ll_c_main;
    RelativeLayout rl_c_main;
    ImageView img_loading;
    carbon.widget.TextView txt_c_name;
    public STGVImageView img_content;

    public MoviesViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        rl_c_main = (RelativeLayout) itemView.findViewById(R.id.rl_c_main);
        img_loading = (ImageView) itemView.findViewById(R.id.img_loading);
        img_content = (STGVImageView) itemView.findViewById(R.id.img_content);

        txt_c_name = (carbon.widget.TextView) itemView.findViewById(R.id.txt_c_name);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
