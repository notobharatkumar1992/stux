package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.EventModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class EventRecyclerViewAdapter extends RecyclerView.Adapter<EventViewHolders> {

    private ArrayList<EventModel> eventArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public EventRecyclerViewAdapter(Context mContext, ArrayList<EventModel> eventArray, OnListItemClickListener itemClickListener) {
        this.eventArray = eventArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public EventViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item, null);
        EventViewHolders rcv = new EventViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final EventViewHolders holder, final int position) {
        try {
            String time = eventArray.get(position).created.substring(0, eventArray.get(position).created.lastIndexOf("+"));
//        2016-06-27T06:32
            try {
//            AppDelegate.LogT("time before = " + time);
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText(time);
            holder.txt_c_name.setText(eventArray.get(position).event_name);
            holder.txt_c_name.setText(eventArray.get(position).details);

            if (eventArray.get(position).gate_fees_type == 0) {
                holder.txt_c_free_entry.setVisibility(View.VISIBLE);
                holder.txt_c_ticket.setVisibility(View.GONE);
            } else {
                holder.txt_c_free_entry.setVisibility(View.GONE);
                holder.txt_c_ticket.setVisibility(View.VISIBLE);
            }
//        holder.txt_c_description.setText("N" + eventArray.get(position).price);
            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            Picasso.with(mContext).load(eventArray.get(position).banner_image_thumb).into(holder.img_events, new Callback() {
                @Override
                public void onSuccess() {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.event_name, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.eventArray.size();
    }
}
