package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Models.ProductModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductViewHolders> {

    private ArrayList<ProductModel> productArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public ProductRecyclerViewAdapter(Context mContext, ArrayList<ProductModel> productArray, OnListItemClickListener itemClickListener) {
        this.productArray = productArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ProductViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item, null);
        ProductViewHolders rcv = new ProductViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ProductViewHolders holder, final int position) {
        try {
            holder.txt_c_product_name.setText(productArray.get(position).title);
            holder.txt_c_price.setText("N" + productArray.get(position).price);

            if (productArray.get(position).item_condition.equalsIgnoreCase("1")) {
                holder.label_image.setLabelText("NEW");
            } else if (productArray.get(position).item_condition.equalsIgnoreCase("2")) {
                holder.label_image.setLabelText("ALMOST  NEW");
            } else if (productArray.get(position).item_condition.equalsIgnoreCase("3")) {
                holder.label_image.setLabelText("USED");
            }

            holder.img_loading.setVisibility(View.VISIBLE);
            if (holder.img_loading != null) {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }

            Picasso.with(mContext).load(productArray.get(position).image_1_thumb).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.img_content.mWidth = bitmap.getWidth();
                    holder.img_content.mHeight = bitmap.getHeight();
//                    if (listenerWithHeight != null) {
//                        listenerWithHeight.setOnListItemClickListener(Tags.HEIGHT, position, holder.img_content.mHeight);
//                    }
                    holder.img_content.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });

            holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));

            switch (productArray.get(position).rating) {
                case 0:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 1:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 2:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 3:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 4:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 5:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    break;
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.product, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.productArray.size();
    }
}
