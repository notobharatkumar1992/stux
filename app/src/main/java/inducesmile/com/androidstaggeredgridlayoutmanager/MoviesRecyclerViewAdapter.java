package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Models.MoviesModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

public class MoviesRecyclerViewAdapter extends RecyclerView.Adapter<MoviesViewHolders> {

    private ArrayList<MoviesModel> moviesArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public MoviesRecyclerViewAdapter(Context mContext, ArrayList<MoviesModel> moviesArray, OnListItemClickListener itemClickListener) {
        this.moviesArray = moviesArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MoviesViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_list_item, null);
        MoviesViewHolders rcv = new MoviesViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText( moviesArray.get(position).title);

            holder.img_loading.setVisibility(View.VISIBLE);
            if (holder.img_loading != null) {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }

            Picasso.with(mContext).load(moviesArray.get(position).banner_image).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.img_content.mWidth = bitmap.getWidth();
                    holder.img_content.mHeight = bitmap.getHeight();
                    holder.img_content.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.movies, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.moviesArray.size();
    }
}
