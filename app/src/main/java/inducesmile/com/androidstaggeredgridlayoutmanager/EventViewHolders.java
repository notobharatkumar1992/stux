package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class EventViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img_events, img_loading;
    TextView txt_c_time, txt_c_name, txt_c_description, txt_c_free_entry, txt_c_ticket, txt_c_ticket_sold_out;
    LinearLayout ll_c_main;

    public EventViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        img_events = (ImageView) convertView.findViewById(R.id.img_events);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_free_entry = (TextView) convertView.findViewById(R.id.txt_c_free_entry);
        txt_c_ticket = (TextView) convertView.findViewById(R.id.txt_c_ticket);
        txt_c_ticket_sold_out = (TextView) convertView.findViewById(R.id.txt_c_ticket_sold_out);
        ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
