package com.stux;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;
import com.stux.Models.InstitutionModel;
import com.stux.Models.UserDataModel;
import com.stux.constants.Tags;
import com.stux.parser.JSONParser;

import org.json.JSONObject;

/**
 * Created by bharat on 2/4/16.
 */
public class PushNotificationService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, final Bundle data) {
        AppDelegate.LogGC("PushNotificationService message = " + data);
        try {
            if (data.getString(Tags.type).equalsIgnoreCase(Tags.follow))
                showPushNotification(data);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void showPushNotification(Bundle data) {
        try {
            JSONObject object = new JSONObject(data.getString(Tags.message));
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.role = object.getString(Tags.role);
            userDataModel.image = object.getString(Tags.image);
            userDataModel.status = object.getString(Tags.status);
            userDataModel.is_verified = object.getString(Tags.is_verified);
            userDataModel.first_name = object.getString(Tags.first_name);
            userDataModel.last_name = object.getString(Tags.last_name);
            userDataModel.email = object.getString(Tags.email);
            userDataModel.created = object.getString(Tags.created);

            userDataModel.facebook_id = object.getString(Tags.social_id);
            userDataModel.following_count = JSONParser.getInt(object, Tags.following_count);
            userDataModel.followers_count = JSONParser.getInt(object, Tags.followers_count);
            userDataModel.total_product = JSONParser.getInt(object, Tags.total_product);
            userDataModel.follow_status = JSONParser.getInt(object, Tags.follow_status);

            JSONObject studentObject = object.getJSONObject(Tags.student_detail);
            userDataModel.str_Gender = studentObject.getString(Tags.gender);
            userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
            userDataModel.userId = studentObject.getString(Tags.user_id);
            userDataModel.dob = studentObject.getString(Tags.dob);
            userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

            userDataModel.institutionModel = new InstitutionModel();
            userDataModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                userDataModel.institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                userDataModel.institutionModel.institution_name = studentObject.getJSONObject(Tags.institute).getString(Tags.institute_name);
            } else {
                userDataModel.institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
            }
            userDataModel.institutionModel.department_name = studentObject.getString(Tags.department_name);

            String message = userDataModel.first_name + " " + userDataModel.last_name + " has followed you.";

            Notification.Builder notificationBuilder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle("Stux")
                        .setStyle(new Notification.BigTextStyle().bigText(message))
                        .setContentText(message);
            } else {
                notificationBuilder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Stux")
                        .setContentText(message);
            }

            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, Tags.follow);
            bundle.putParcelable(Tags.user, userDataModel);

            Intent push = new Intent(this, NotificationHandler.class);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            push.putExtras(bundle);

            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, push, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);

            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.drawable.logo,
                        message, System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }

            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(1, notification);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}