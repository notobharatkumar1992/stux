package com.stux.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.R;
import com.stux.constants.Tags;

import carbon.widget.ImageView;

/**
 * Created by Bharat on 06/23/2016.
 */
public class LargeImageActivity extends FragmentActivity {

    private ImageView img_large;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.large_image_layout);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.VISIBLE);

        findViewById(R.id.img_c_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_large = (ImageView) findViewById(R.id.img_large);
        Picasso.with(this).load(getIntent().getExtras().getString(Tags.image)).into(img_large,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        progressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressbar.setVisibility(View.GONE);
                        AppDelegate.showToast(LargeImageActivity.this, "Image failed to load, please try after some time.");
                    }
                });

    }
}
