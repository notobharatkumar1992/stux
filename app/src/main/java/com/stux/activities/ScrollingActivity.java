package com.stux.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.bulletnoid.android.widget.StaggeredGridView.StaggeredGridView;
import com.stux.Adapters.CampusListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.R;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ScrollingActivity extends AppCompatActivity implements OnReciveServerResponse {

    // Campus list
    private CampusListAdapter mCampusListAdapter;
    private StaggeredGridView stgv;
    private int campusCounter = 1, campusTotalPage = -1;
    public static ArrayList<ProductModel> productArray = new ArrayList<>();

    private boolean campusAsyncExcecuting = false;

    private View footerView1, footerView2, footerView3;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        footerView1 = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer_view, null, false);

        // Campus list Data init
        mCampusListAdapter = new CampusListAdapter(ScrollingActivity.this, R.id.txt_line1, productArray, null, null);
        stgv = (StaggeredGridView) findViewById(R.id.stgv);
        stgv.setItemMargin(8);
        stgv.setPadding(8, 0, 8, 0);
        stgv.setAdapter(mCampusListAdapter);
        stgv.setOnLoadmoreListener(new StaggeredGridView.OnLoadmoreListener() {
            @Override
            public void onLoadmore() {
                if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                    footerView1.setVisibility(View.VISIBLE);
                    mHandler.sendEmptyMessage(2);
                    callCampusListAsync();
                    campusAsyncExcecuting = true;
                }
            }
        });
//        stgv.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });
        setHandler();
        callCampusListAsync();
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(ScrollingActivity.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(ScrollingActivity.this).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, "26");
            AppDelegate.getInstance(ScrollingActivity.this).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(ScrollingActivity.this).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(ScrollingActivity.this,
                    this, ServerRequestConstants.CAMPUS_LIST,
                    mPostArrayList, null);
            if (campusCounter == 1) {
                mHandler.sendEmptyMessage(14);
            }
            AppDelegate.LogT("callCampusListAsync called");
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(ScrollingActivity.this.getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(ScrollingActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(ScrollingActivity.this);
                } else if (msg.what == 12) {
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified ");
//                        txt_c_no_list.setVisibility(productArray.size() > 0 ? View.GONE : View.VISIBLE);
//                        txt_c_no_list.setText("No product available");
                    mCampusListAdapter.notifyDataSetChanged();
                    stgv.invalidate();
                    stgv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                                adjustHeightOfStaggerdView();
                            stgv.setVerticalScrollbarPosition(0);
                        }
                    }, 1000);
                }
            }
        };
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CAMPUS_LIST)) {
            mHandler.sendEmptyMessage(15);
            footerView1.setVisibility(View.GONE);
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            }
            parseCampusListResult(result);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(ScrollingActivity.this, jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                campusTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    ProductModel productModel = new ProductModel();
                    productModel.id = object.getString(Tags.id);
                    productModel.cat_id = object.getString(Tags.cat_id);
                    productModel.title = object.getString(Tags.title);
                    productModel.description = object.getString(Tags.description);
                    productModel.price = object.getString(Tags.price);
                    productModel.item_condition = object.getString(Tags.item_condition);

                    productModel.image_1 = object.getString(Tags.image_1);
                    productModel.image_2 = object.getString(Tags.image_2);
                    productModel.image_3 = object.getString(Tags.image_3);
                    productModel.image_4 = object.getString(Tags.image_4);

                    productModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                    productModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                    productModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                    productModel.image_4_thumb = object.getString(Tags.image_4_thumb);
                    float floatValue = Float.parseFloat(object.getString(Tags.rating));
                    AppDelegate.LogT("floatValue = " + floatValue);
                    productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
                    AppDelegate.LogT("productModel.rating = " + productModel.rating);


                    productModel.sold_status = object.getString(Tags.sold_status);
                    productModel.status = object.getString(Tags.status);
                    productModel.created = object.getString(Tags.created);
                    productModel.modified = object.getString(Tags.modified);
                    productModel.total_product_views = object.getString(Tags.total_product_views);
                    productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                    if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category).toString())) {
                        JSONObject productObject = object.getJSONObject(Tags.product_category);
                        productModel.pc_id = productObject.getString(Tags.id);
                        productModel.pc_title = productObject.getString(Tags.cat_name);
                        productModel.pc_status = productObject.getString(Tags.status);
                    }

                    productModel.latitude = object.getString(Tags.latitude);
                    productModel.longitude = object.getString(Tags.longitude);

                    JSONObject userObject = object.getJSONObject(Tags.user);
                    productModel.user_id = userObject.getString(Tags.id);
                    productModel.user_first_name = userObject.getString(Tags.first_name);
                    productModel.user_last_name = userObject.getString(Tags.last_name);
                    productModel.user_email = userObject.getString(Tags.email);
                    productModel.user_role = userObject.getString(Tags.role);
                    productModel.user_image = userObject.getString(Tags.image);
                    productModel.user_social_id = userObject.getString(Tags.social_id);
                    productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                    productModel.user_following_count = userObject.getInt(Tags.following_count);
                    productModel.user_followers_count = userObject.getInt(Tags.followers_count);
                    productModel.user_total_product = userObject.getInt(Tags.total_product);
                    productModel.user_follow_status = userObject.getInt(Tags.follow_status);

                    JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                    if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                        productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                        productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                        JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                        if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                            productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                        }
                        if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        } else {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                    } else {
                        productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                    }

                    productArray.add(productModel);
                }
            } else {
                campusTotalPage = 0;
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(2);
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
