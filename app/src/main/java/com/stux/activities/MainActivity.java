package com.stux.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.OptionsListAdapter;
import com.stux.AppDelegate;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.constants.Constants;
import com.stux.constants.Tags;
import com.stux.fragments.DealsMoviesFragment;
import com.stux.fragments.DemoHomeFragment;
import com.stux.fragments.EventDetailFragment;
import com.stux.fragments.FollowingListFragment;
import com.stux.fragments.HelpFragment;
import com.stux.fragments.HomeFragment;
import com.stux.fragments.MyEventListingFragment;
import com.stux.fragments.MyProductListFragment;
import com.stux.fragments.MyProfileFragment;
import com.stux.fragments.ProfileLandingFragment;
import com.stux.fragments.SellItemFragment;
import com.stux.fragments.SellersProfileFragment;
import com.stux.interfaces.OnListItemClickListener;
import com.wunderlist.slidinglayer.SlidingLayer;
import com.wunderlist.slidinglayer.transformer.AlphaTransformer;

import java.util.ArrayList;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class MainActivity extends FragmentActivity implements View.OnClickListener, OnListItemClickListener {

    public static final int COLLAPSE = 0, EXPAND = 1;
    public static Handler mHandler;
    public static SlidingPaneLayout1 mSlidingPaneLayout;
    public static final int PANEL_PROFILE = 20, PANEL_DASHBOARD = 0, PANEL_FOLLOWING = 1, PANEL_MY_LIST = 2, PANEL_LIST_ITEM = 3, PANEL_MY_COUPON = 4, PANEL_DEALS_MOVIES = 5, PANEL_EVENTS = 6, PANEL_NOTIFICATION = 7, PANEL_N_MESSAGE = 71, PANEL_N_DEALS = 72, PANEL_N_COUPONS = 73, PANEL_INTERNSHIP_JOBS = 8, PANEL_LOST_FOUND = 9, PANEL_HELP = 10, PANEL_LOGOUT = 11;

    public static RelativeLayout side_bar, user_name_layout, panel_dashboard_layout, panel_my_listing_layout,
            panel_list_item_layout, panel_my_lists_layout, panel_deals_movies_layout, panel_events_layout, panel_notifications_layout,
            panel_intership_jobs_layout, panel_lost_found_layout, panel_help_layout, panel_logout_layout, panel_my_coupon_layout,
            panel_messages_alert_layout, panel_deals_alert_layout, panel_coupons_alert_layout, panel_following_layout;
    public static TextView txt_c_user_name, txt_c_address;
    public SlideMenuClickListener menuClickListener;
    //  public TextView panel_home_txt, panel_find_ride_txt, panel_booked_ride_txt,
//            panel_completed_ride_txt, panel_see_family_txt, panel_profile_txt,
//            panel_logout_txt, panel_login_txt;
    public ImageView panel_home_img, panel_find_ride_imge,
            panel_booked_ride_img, panel_completed_ride_img,
            panel_see_family_img, panel_profile_img, panel_logout_img,
            panel_login_img, img_loading, panel_notifications_arrow_img;
    public CircleImageView cimg_user;
    public LinearLayout side_panel;
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;

    public SlidingLayer sl_more;
    public ListView list_options;
    public OptionsListAdapter adapter;
    public ArrayList<String> arrayOptions = new ArrayList<>();

    public Prefs prefs;
    private ExpandableRelativeLayout exp_layout;
    private UserDataModel dataModel;

    public static void isMapNotContain(boolean enable) {
        if (mSlidingPaneLayout != null) {
            mSlidingPaneLayout.setEnable(enable);
            mSlidingPaneLayout.setEnabled(enable);
            Constants.isMapScreen = !enable;
        }
    }

    public static void setEnablePanel(boolean enable) {
        if (mSlidingPaneLayout != null) {
            mSlidingPaneLayout.setEnable(enable);
            mSlidingPaneLayout.setEnabled(enable);
            Constants.isMapScreen = enable;
        }
    }

    @SuppressWarnings("deprecation")
    public static void setInitailSideBar(Context mContext, int value) {
        if (mContext == null)
            return;
        panel_dashboard_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_my_listing_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_list_item_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_my_lists_layout.setBackgroundDrawable(mContext
                .getResources().getDrawable(
                        R.drawable.sl_bg_panel));
        panel_deals_movies_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_events_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_notifications_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
        panel_intership_jobs_layout.setBackgroundDrawable(mContext.getResources()
                .getDrawable(R.drawable.sl_bg_panel));
//        switch (value) {
//            case 0:
//                panel_dashboard_layout.setBackgroundDrawable(mContext.getResources()
//                        .getDrawable(R.drawable.selector_slide_background_purple));
//                break;
//            case 1:
//                panel_my_listing_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 2:
//                panel_list_item_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 3:
//                panel_my_lists_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 4:
//                panel_deals_movies_layout.setBackgroundDrawable(mContext
//                        .getResources().getDrawable(
//                                R.drawable.selector_slide_background_purple));
//                break;
//            case 5:
//                panel_events_layout.setBackgroundDrawable(mContext.getResources()
//                        .getDrawable(R.drawable.selector_slide_background_purple));
//                break;
//            default:
//                break;
//        }
    }

    public void expandableView(final ExpandableRelativeLayout exp_layout, final int value) {
        exp_layout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    exp_layout.collapse();
                } else {
                    exp_layout.expand();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Constants.isMapScreen = true;
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        showFragment();
        initView();
        setHandler();
        updateUserDetail();
    }

    private void showFragment() {
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle.getString(Tags.FROM).equalsIgnoreCase(Tags.follow)) {
//                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment());
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment());
                Fragment fragment = new SellersProfileFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
            }
        } else if (dataModel != null && AppDelegate.isValidString(dataModel.image)) {
//            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new HomeFragment());
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new DemoHomeFragment());
        } else {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), new ProfileLandingFragment());
        }
    }

    private void initView() {
        side_panel = (LinearLayout) findViewById(R.id.side_panel);
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.sliding_pane);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());

        side_bar = (RelativeLayout) findViewById(R.id.side_bar);
        side_bar.getLayoutParams().height = (AppDelegate.getDeviceWith(this) / 4) * 3;

        user_name_layout = (RelativeLayout) findViewById(R.id.user_name_layout);
        user_name_layout.setOnClickListener(this);

        panel_dashboard_layout = (RelativeLayout) findViewById(R.id.panel_dashboard_layout);
        panel_dashboard_layout.setOnClickListener(this);
        panel_my_listing_layout = (RelativeLayout) findViewById(R.id.panel_my_listing_layout);
        panel_my_listing_layout.setOnClickListener(this);
        panel_list_item_layout = (RelativeLayout) findViewById(R.id.panel_list_item_layout);
        panel_list_item_layout.setOnClickListener(this);
        panel_my_lists_layout = (RelativeLayout) findViewById(R.id.panel_my_lists_layout);
        panel_my_lists_layout.setOnClickListener(this);
        panel_deals_movies_layout = (RelativeLayout) findViewById(R.id.panel_deals_movies_layout);
        panel_deals_movies_layout.setOnClickListener(this);
        panel_events_layout = (RelativeLayout) findViewById(R.id.panel_events_layout);
        panel_events_layout.setOnClickListener(this);
        panel_notifications_layout = (RelativeLayout) findViewById(R.id.panel_notifications_layout);
        panel_notifications_layout.setOnClickListener(this);
        panel_intership_jobs_layout = (RelativeLayout) findViewById(R.id.panel_intership_jobs_layout);
        panel_intership_jobs_layout.setOnClickListener(this);
        panel_lost_found_layout = (RelativeLayout) findViewById(R.id.panel_lost_found_layout);
        panel_lost_found_layout.setOnClickListener(this);
        panel_help_layout = (RelativeLayout) findViewById(R.id.panel_help_layout);
        panel_help_layout.setOnClickListener(this);
        panel_logout_layout = (RelativeLayout) findViewById(R.id.panel_logout_layout);
        panel_logout_layout.setOnClickListener(this);
        panel_my_coupon_layout = (RelativeLayout) findViewById(R.id.panel_my_coupon_layout);
        panel_my_coupon_layout.setOnClickListener(this);

        panel_messages_alert_layout = (RelativeLayout) findViewById(R.id.panel_messages_alert_layout);
        panel_messages_alert_layout.setOnClickListener(this);
        panel_deals_alert_layout = (RelativeLayout) findViewById(R.id.panel_deals_alert_layout);
        panel_deals_alert_layout.setOnClickListener(this);
        panel_coupons_alert_layout = (RelativeLayout) findViewById(R.id.panel_coupons_alert_layout);
        panel_coupons_alert_layout.setOnClickListener(this);
        panel_following_layout = (RelativeLayout) findViewById(R.id.panel_following_layout);
        panel_following_layout.setOnClickListener(this);

        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);

        txt_c_user_name = (TextView) findViewById(R.id.txt_c_user_name);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
//        panel_home_txt = (TextView) findViewById(R.id.panel_home_txt);
//        panel_find_ride_txt = (TextView) findViewById(R.id.panel_find_ride_txt);
//        panel_booked_ride_txt = (TextView) findViewById(R.id.panel_booked_ride_txt);
//        panel_completed_ride_txt = (TextView) findViewById(R.id.panel_completed_ride_txt);
//        panel_see_family_txt = (TextView) findViewById(R.id.panel_see_family_txt);
//        panel_profile_txt = (TextView) findViewById(R.id.panel_profile_txt);
//        panel_logout_txt = (TextView) findViewById(R.id.panel_logout_txt);
//        panel_login_txt = (TextView) findViewById(R.id.panel_login_txt);

        panel_notifications_arrow_img = (ImageView) findViewById(R.id.panel_notifications_arrow_img);
        panel_notifications_arrow_img.setSelected(true);

        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);

        arrayOptions.add("Notifications");
        arrayOptions.add("Favourites");
        arrayOptions.add("App Guide");

        sl_more = (SlidingLayer) findViewById(R.id.sl_more);
        sl_more.setStickTo(SlidingLayer.STICK_TO_RIGHT);
        sl_more.setLayerTransformer(new AlphaTransformer());
        sl_more.setShadowSize(0);
        sl_more.setShadowDrawable(null);

        sl_more.setOnInteractListener(new SlidingLayer.OnInteractListener() {

            @Override
            public void onOpen() {
//                ((carbon.widget.ImageView) findViewById(R.id.img_options_1)).setImageResource(R.drawable.cross);
            }

            @Override
            public void onShowPreview() {
            }

            @Override
            public void onClose() {
//                ((carbon.widget.ImageView) findViewById(R.id.img_options_1)).setImageResource(R.drawable.menu);
            }

            @Override
            public void onOpened() {
            }

            @Override
            public void onPreviewShowed() {
            }

            @Override
            public void onClosed() {
            }
        });

        adapter = new OptionsListAdapter(this, arrayOptions, this);
        list_options = (ListView) findViewById(R.id.list_options);
        list_options.setAdapter(adapter);

    }

    public void updateUserDetail() {
        if (prefs.getUserdata() != null) {
            prefs.getInstitutionModel();
            txt_c_user_name.setText(prefs.getUserdata().first_name + " " + prefs.getUserdata().last_name);
            txt_c_address.setText(prefs.getInstitutionModel().institution_name + " - " + prefs.getInstitutionModel().department_name);
            if (AppDelegate.isValidString(prefs.getUserdata().image)) {
                img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                frameAnimation.setCallback(img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                Picasso.with(this).load(prefs.getUserdata().image).into(cimg_user, new Callback() {
                    @Override
                    public void onSuccess() {
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
            } else if (prefs.getUserdata().str_Gender.equalsIgnoreCase(Tags.MALE)) {
                cimg_user.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1));
            } else {
                cimg_user.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1));
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private void setHandler() {
        menuClickListener = new SlideMenuClickListener();
        mHandler = new Handler() {

            @Override
            public void dispatchMessage(Message msg) {
                if (msg.what == 0) {
                    toggleSlider();
                } else if (msg.what == 1) {
                    AppDelegate.showProgressDialog(MainActivity.this);
                } else if (msg.what == 2) {
//                    setResultFromGeoCoderApi(msg.getData());
                } else if (msg.what == 3) {
                    AppDelegate.hideProgressDialog(MainActivity.this);
                } else if (msg.what == 5) {
                } else if (msg.what == 6) {
                } else if (msg.what == 7) {
                } else if (msg.what == 8) {
                } else if (msg.what == 9) {
                } else if (msg.what == 10) {
                }
            }
        };
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof EventDetailFragment) {
            AppDelegate.showFragmentAnimationOppose(getSupportFragmentManager(), new MyEventListingFragment());
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment || getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof ProfileLandingFragment || getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyEventListingFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_name_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyProfileFragment))
                    menuClickListener.onItemClick(null, v, PANEL_PROFILE, PANEL_PROFILE);
                break;

            case R.id.panel_dashboard_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof HomeFragment))
                    menuClickListener.onItemClick(null, v, PANEL_DASHBOARD, PANEL_DASHBOARD);
                break;

            case R.id.panel_following_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof FollowingListFragment))
                    menuClickListener.onItemClick(null, v, PANEL_FOLLOWING, PANEL_FOLLOWING);
                break;


            case R.id.panel_my_listing_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyProductListFragment))
                    menuClickListener.onItemClick(null, v, PANEL_MY_LIST, PANEL_MY_LIST);
                break;

//            case R.id.panel_list_item_layout:
//                mHandler.sendEmptyMessage(0);
//                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SellItemFragment)) {
//                    if (AppDelegate.haveNetworkConnection(MainActivity.this, false)) {
//                        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                            buildAlertMessageNoGps();
//                        } else {
//                            menuClickListener.onItemClick(null, v, PANEL_LIST_ITEM, PANEL_LIST_ITEM);
//                        }
//                    } else {
//                        AppDelegate.addFragment(getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
//                    }
//                }
//                break;

            case R.id.panel_deals_movies_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof DealsMoviesFragment))
                    menuClickListener.onItemClick(null, v, PANEL_DEALS_MOVIES, PANEL_DEALS_MOVIES);
                break;

            case R.id.panel_events_layout:
                mHandler.sendEmptyMessage(0);
                if (!(getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof MyEventListingFragment))
                    menuClickListener.onItemClick(null, v, PANEL_EVENTS, PANEL_EVENTS);
                break;

            case R.id.panel_notifications_layout:
//                mHandler.sendEmptyMessage(0);
                expandableView(exp_layout, exp_layout.isExpanded() ? COLLAPSE : EXPAND);
                panel_notifications_arrow_img.setSelected(exp_layout.isExpanded());
                break;

            case R.id.panel_help_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_HELP, PANEL_HELP);
                break;

            case R.id.panel_logout_layout:
                mHandler.sendEmptyMessage(0);
                menuClickListener.onItemClick(null, v, PANEL_LOGOUT, PANEL_LOGOUT);
                break;
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    public void toggleSlider() {
        if (mSlidingPaneLayout != null)
            if (!mSlidingPaneLayout.isOpen()) {
                mSlidingPaneLayout.openPane();
            } else {
                mSlidingPaneLayout.closePane();
            }
    }

    public void whileSlide(int view) {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    /**
     * Displaying fragment view for selected navigation drawer list item
     */
    private void displayView(int position) {
        AppDelegate.hideKeyBoard(MainActivity.this);
        if (position != 6)
            setInitailSideBar(MainActivity.this, position);
        Fragment fragment = null;
        switch (position) {

            case PANEL_PROFILE:
                fragment = new MyProfileFragment();
                break;

            case PANEL_DASHBOARD:
                fragment = new HomeFragment();
                break;

            case PANEL_FOLLOWING:
                fragment = new FollowingListFragment();
                break;

            case PANEL_MY_LIST:
                fragment = new MyProductListFragment();
                break;

            case PANEL_LIST_ITEM:
                fragment = new SellItemFragment();
                break;

            case PANEL_DEALS_MOVIES:
                fragment = new DealsMoviesFragment();
                break;

            case PANEL_EVENTS:
                fragment = new MyEventListingFragment();
                break;

            case PANEL_NOTIFICATION:
                expandableView(exp_layout, exp_layout.isExpanded() ? COLLAPSE : EXPAND);
                break;

            case 8:
                break;


            case PANEL_HELP:
                fragment = new HelpFragment();
                break;

            case PANEL_LOGOUT:
                prefs.clearTempPrefs();
                prefs.clearSharedPreference();
                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.IS_SPLASH, Tags.FALSE);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }

        if (fragment != null) {
            AppDelegate.showFragmentAnimation(getSupportFragmentManager(), fragment);
        } else if (position != 6) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (SellItemFragment.callbackManager != null)
            SellItemFragment.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Slide menu item click listener
     **/
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                final int position, long id) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    displayView(position);
                }
            }, 600);
        }
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener {

        @Override
        public void onPanelOpened(View panel) {
            if (!isSlideOpen) {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(MainActivity.this);
        }

        @Override
        public void onPanelClosed(View panel) {
            if (isSlideOpen) {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset) {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }
}
