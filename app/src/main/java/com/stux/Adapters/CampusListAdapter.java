package com.stux.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.bulletnoid.android.widget.StaggeredGridViewDemo.STGVImageView;
import com.lid.lib.LabelImageView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Models.ProductModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class CampusListAdapter extends ArrayAdapter<String> {

    static class ViewHolder {
        LinearLayout ll_c_main;
        RelativeLayout rl_c_main;
        ImageView img_product, img_star_1, img_star_2, img_star_3, img_star_4, img_star_5, img_loading;
        LabelImageView label_image;
        TextView txt_c_product_name, txt_c_price;
        public STGVImageView img_content;
    }

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<ProductModel> productArray;

    private OnListItemClickListenerWithHeight listenerWithHeight;
    private OnListItemClickListener itemClickListener;

    public CampusListAdapter(final Context context, final int textViewResourceId, ArrayList<ProductModel> productArray, OnListItemClickListener itemClickListener, OnListItemClickListenerWithHeight listenerWithHeight) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.productArray = productArray;
        this.itemClickListener = itemClickListener;
        this.listenerWithHeight = listenerWithHeight;
    }

    @Override
    public int getCount() {
        return productArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.product_list_item, parent, false);
            holder = new ViewHolder();
            holder.ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
            holder.rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);
            holder.img_product = (ImageView) convertView.findViewById(R.id.img_product);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.img_content = (STGVImageView) convertView.findViewById(R.id.img_content);

            holder.img_star_1 = (ImageView) convertView.findViewById(R.id.img_star_1);
            holder.img_star_2 = (ImageView) convertView.findViewById(R.id.img_star_2);
            holder.img_star_3 = (ImageView) convertView.findViewById(R.id.img_star_3);
            holder.img_star_4 = (ImageView) convertView.findViewById(R.id.img_star_4);
            holder.img_star_5 = (ImageView) convertView.findViewById(R.id.img_star_5);

            holder.label_image = (LabelImageView) convertView.findViewById(R.id.label_image);
            holder.txt_c_product_name = (TextView) convertView.findViewById(R.id.txt_c_product_name);
            holder.txt_c_price = (TextView) convertView.findViewById(R.id.txt_c_price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_product_name.setText(productArray.get(position).title);
            holder.txt_c_price.setText("N" + productArray.get(position).price);

            if (productArray.get(position).item_condition.equalsIgnoreCase("1")) {
                holder.label_image.setLabelText("NEW");
            } else if (productArray.get(position).item_condition.equalsIgnoreCase("2")) {
                holder.label_image.setLabelText("ALMOST  NEW");
            } else if (productArray.get(position).item_condition.equalsIgnoreCase("3")) {
                holder.label_image.setLabelText("USED");
            }

            holder.img_loading.setVisibility(View.VISIBLE);
            if (holder.img_loading != null) {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }

            Picasso.with(mContext).load(productArray.get(position).image_1_thumb).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    holder.img_content.mWidth = bitmap.getWidth();
                    holder.img_content.mHeight = bitmap.getHeight();
                    if (listenerWithHeight != null) {
                        listenerWithHeight.setOnListItemClickListener(Tags.HEIGHT, position, holder.img_content.mHeight);
                    }
                    holder.img_content.setImageBitmap(bitmap);
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });

            holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));

            switch (productArray.get(position).rating) {
                case 0:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 1:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 2:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 3:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 4:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 5:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    break;
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.product, position);
                    }
                }
            });
            holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.product, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        return convertView;
    }

}