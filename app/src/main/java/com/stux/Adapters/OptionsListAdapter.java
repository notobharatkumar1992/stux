package com.stux.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.LinearLayout;


public class OptionsListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> arrayOptions;
    private OnListItemClickListener clickListener;

    public OptionsListAdapter(Context mContext,
                              ArrayList<String> arrayOptions, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.arrayOptions = arrayOptions;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return arrayOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.options_list_item, null);
            holder = new Holder();
            holder.txt_c_name = (TextView) convertView
                    .findViewById(R.id.txt_c_name);
            holder.img_icon = (ImageView) convertView
                    .findViewById(R.id.img_icon);
            holder.ll_c_main_item = (LinearLayout) convertView
                    .findViewById(R.id.ll_c_main_item);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.txt_c_name.setText(arrayOptions.get(position));
        switch (position) {
            case 0:
                holder.img_icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.back));
                break;
            case 1:
                holder.img_icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.back));
                break;
            case 2:
                holder.img_icon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.back));
                break;
        }


        holder.ll_c_main_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.Options, position);
            }
        });
        return convertView;
    }

    class Holder {
        public TextView txt_c_name;
        public ImageView img_icon;
        public LinearLayout ll_c_main_item;
    }

}
