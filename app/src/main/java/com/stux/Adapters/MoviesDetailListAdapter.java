package com.stux.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.stux.AppDelegate;
import com.stux.Models.MovieTime;
import com.stux.R;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class MoviesDetailListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<MovieTime> movieArray;
    private OnListItemClickListener itemClickListener;

    public MoviesDetailListAdapter(final Context context, final int textViewResourceId, ArrayList<MovieTime> movieArray, OnListItemClickListener itemClickListener) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.movieArray = movieArray;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getCount() {
        return movieArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.movies_detail_list_item, parent, false);
            holder = new ViewHolder();
            holder.ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_name.setText(movieArray.get(position).cinema_type);
            holder.txt_c_description.setText(movieArray.get(position).cinema_time);

//            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (itemClickListener != null) {
//                        itemClickListener.setOnListItemClickListener(Tags.LIST_ITEM_TRENDING, position);
//                    }
//                }
//            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        return convertView;
    }

    static class ViewHolder {
        LinearLayout ll_c_main;
        TextView txt_c_name, txt_c_description;
    }

}