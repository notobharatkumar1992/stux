package com.stux.Adapters;


import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bulletnoid.android.widget.StaggeredGridViewDemo.STGVImageView;
import com.stux.R;

import carbon.widget.TextView;

public class ProductAdapter extends ArrayAdapter<String> {

    private static final String TAG = "CampusListAdapter";

    static class ViewHolder {
        public ImageView img_product;
        public TextView txt_c_product_name, txt_c_price;
        public RatingBar rb_product;
        public STGVImageView img_content;
    }

    private final LayoutInflater mLayoutInflater;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public ProductAdapter(final Context context, final int textViewResourceId) {
        super(context, textViewResourceId);
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.product_list_item, parent, false);
            vh = new ViewHolder();
            vh.img_product = (ImageView) convertView.findViewById(R.id.img_product);
            vh.txt_c_product_name = (TextView) convertView.findViewById(R.id.txt_c_product_name);
            vh.txt_c_price = (TextView) convertView.findViewById(R.id.txt_c_price);
            vh.rb_product = (RatingBar) convertView.findViewById(R.id.rb_product);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}