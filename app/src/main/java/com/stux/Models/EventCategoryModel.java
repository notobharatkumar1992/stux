package com.stux.Models;

/**
 * Created by Bharat on 07/19/2016.
 */
public class EventCategoryModel {

    public String id, cat_name, status, created, modified;

    public EventCategoryModel(String cat_name) {
        this.cat_name = cat_name;
    }

    public EventCategoryModel() {
    }
}
