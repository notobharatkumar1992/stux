package com.stux.Models;

/**
 * Created by Bharat on 06/08/2016.
 */
public class DepartmentModel {

    public String id, department_name;

    public DepartmentModel(String department_name) {
        this.department_name = department_name;
        this.id = department_name;
    }

    public DepartmentModel() {
    }
}
