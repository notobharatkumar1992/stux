package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 07/25/2016.
 */
public class MoviesModel implements Parcelable {

    public String id, user_id, title, banner_image, banner_image_thumb, synopsis, run_time, starring, trailer_link, genre, cinema_type_with_time,
            status, is_view, total_movie_views, logged_user_view_status, created;

    public MoviesModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        title = in.readString();
        banner_image = in.readString();
        banner_image_thumb = in.readString();
        synopsis = in.readString();
        run_time = in.readString();
        starring = in.readString();
        trailer_link = in.readString();
        genre = in.readString();
        cinema_type_with_time = in.readString();
        status = in.readString();
        is_view = in.readString();
        total_movie_views = in.readString();
        logged_user_view_status = in.readString();
        created = in.readString();
    }

    public static final Creator<MoviesModel> CREATOR = new Creator<MoviesModel>() {
        @Override
        public MoviesModel createFromParcel(Parcel in) {
            return new MoviesModel(in);
        }

        @Override
        public MoviesModel[] newArray(int size) {
            return new MoviesModel[size];
        }
    };

    public MoviesModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(title);
        dest.writeString(banner_image);
        dest.writeString(banner_image_thumb);
        dest.writeString(synopsis);
        dest.writeString(run_time);
        dest.writeString(starring);
        dest.writeString(trailer_link);
        dest.writeString(genre);
        dest.writeString(cinema_type_with_time);
        dest.writeString(status);
        dest.writeString(is_view);
        dest.writeString(total_movie_views);
        dest.writeString(logged_user_view_status);
        dest.writeString(created);
    }
}
