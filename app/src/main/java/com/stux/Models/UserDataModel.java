package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 15/1/16.
 */
public class UserDataModel implements Parcelable {
    public String userId, first_name, last_name, email, dob, image, fbImageUrl, str_Gender, mobile_number, password, facebook_id, role, gcm_token, login_type, fav_cat_id, status, is_verified, created, student_id, city_name, is_view, social_id;
    public String institution_state_id, institution_id, institute_name, department_name;
    public int date, month, year, isGroupOwner = 0, follow_status = 0, total_product = 0, followers_count = 0, following_count = 0;

    public double latitude, longitude;

    public InstitutionModel institutionModel;

    public UserDataModel(Parcel in) {
        userId = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        dob = in.readString();
        image = in.readString();
        fbImageUrl = in.readString();
        str_Gender = in.readString();
        mobile_number = in.readString();
        password = in.readString();
        facebook_id = in.readString();
        role = in.readString();
        gcm_token = in.readString();
        login_type = in.readString();
        fav_cat_id = in.readString();
        status = in.readString();
        is_verified = in.readString();
        created = in.readString();
        student_id = in.readString();
        city_name = in.readString();
        is_view = in.readString();
        social_id = in.readString();

        institution_state_id = in.readString();
        institution_id = in.readString();
        institute_name = in.readString();
        department_name = in.readString();

        date = in.readInt();
        month = in.readInt();
        year = in.readInt();
        isGroupOwner = in.readInt();
        follow_status = in.readInt();
        total_product = in.readInt();
        followers_count = in.readInt();
        following_count = in.readInt();


        latitude = in.readDouble();
        longitude = in.readDouble();

        institutionModel = in.readParcelable(InstitutionModel.class.getClassLoader());
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };

    public UserDataModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(dob);
        dest.writeString(image);
        dest.writeString(fbImageUrl);
        dest.writeString(str_Gender);
        dest.writeString(mobile_number);
        dest.writeString(password);
        dest.writeString(facebook_id);
        dest.writeString(role);
        dest.writeString(gcm_token);
        dest.writeString(login_type);
        dest.writeString(fav_cat_id);
        dest.writeString(status);
        dest.writeString(is_verified);
        dest.writeString(created);
        dest.writeString(student_id);
        dest.writeString(city_name);
        dest.writeString(is_view);
        dest.writeString(social_id);

        dest.writeString(institution_state_id);
        dest.writeString(institution_id);
        dest.writeString(institute_name);
        dest.writeString(department_name);

        dest.writeInt(date);
        dest.writeInt(month);
        dest.writeInt(year);
        dest.writeInt(isGroupOwner);
        dest.writeInt(follow_status);
        dest.writeInt(total_product);
        dest.writeInt(followers_count);
        dest.writeInt(following_count);

        dest.writeDouble(latitude);
        dest.writeDouble(longitude);

        dest.writeParcelable(institutionModel, flags);
    }
}
