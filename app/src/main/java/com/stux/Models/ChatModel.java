package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bharat on 4/1/16.
 */
public class ChatModel implements Parcelable {

    public static final Creator<ChatModel> CREATOR = new Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };
    public String id = "", user_id, user_name, message, user_image, status, create_date, category_name, date, time, activity_id, custom_activity_name;
    public boolean is_friend = true, sent = false, is_date_identifier = false;

    protected ChatModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        user_name = in.readString();
        message = in.readString();
        user_image = in.readString();
        status = in.readString();
        create_date = in.readString();
        category_name = in.readString();
        date = in.readString();
        time = in.readString();
        activity_id = in.readString();
        custom_activity_name = in.readString();

        is_friend = in.readByte() != 0;
        sent = in.readByte() != 0;
        is_date_identifier = in.readByte() != 0;
    }

    public ChatModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(user_name);
        dest.writeString(message);
        dest.writeString(user_image);
        dest.writeString(status);
        dest.writeString(create_date);
        dest.writeString(category_name);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(activity_id);
        dest.writeString(custom_activity_name);

        dest.writeByte((byte) (is_friend ? 1 : 0));
        dest.writeByte((byte) (sent ? 1 : 0));
        dest.writeByte((byte) (is_date_identifier ? 1 : 0));
    }

}
