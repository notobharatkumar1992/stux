package com.stux.constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */

	/*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";


    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";


    public static final String BASE_URL = "http://notosolutions.net/stux/web-services/";
    public static final String GET_STATE_LIST = BASE_URL + "getStateList";
    //        public static final String GET_STATE_LIST = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&name=cruise&key=AIzaSyD_tJNzy9obMNvlpqML8txVVGVqb4HXWWU";
    public static final String GET_INSTITUTION_NAME = BASE_URL + "getInstitutionName";
    public static final String GET_DEPARTMENT = BASE_URL + "getDepartmentName";
    public static final String REGISTRATION = BASE_URL + "registration";

    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static final String LOGIN = BASE_URL + "login";
    public static final String FAV_CATEGORY = BASE_URL + "favCategory";

    public static final String EDIT_PROFILE = BASE_URL + "editProfile";

    public static final String SLIDERS_LIST = BASE_URL + "slidersList";
    public static final String SLIDERS_CLICKS = BASE_URL + "getSliderClicks";


    public static final String CAMPUS_LIST = BASE_URL + "getcampusList";
    public static final String EVENTS_LIST = BASE_URL + "getEvents";
    public static final String DEALS_LIST = BASE_URL + "getTopDeals";
    public static final String MOVIES_LIST = BASE_URL + "getMovies";
    public static final String CHANGE_PASSWORD = BASE_URL + "changePassword";

    public static final String GET_MY_PRODUCT = BASE_URL + "getMyProduct";

    public static final String GET_USER_REVIEW = BASE_URL + "getuserReviews";
    public static final String ADD_REVIEW = BASE_URL + "addReviews";

    public static final String GET_FOLLOWERS = BASE_URL + "getFollowers";
    public static final String GET_FOLLOWING = BASE_URL + "getFollowing";
    public static final String FOLLOW = BASE_URL + "follow";


    public static final String GET_USER = BASE_URL + "getUserData";

    public static final String ITEM_VIEW = BASE_URL + "itemView";
    public static final String ITEM_SOLD = BASE_URL + "itemSold";

    public static final String GET_DEAL_CATEGORY = BASE_URL + "getDealCategories";
    public static final String GET_PRODUCT_CATEGORY = BASE_URL + "getProductCategories";
    public static final String GET_EVENT_CATEGORY = BASE_URL + "getEventType";

    public static final String CREATE_PRODUCT = BASE_URL + "createProduct";
    public static final String CREATE_EVENT = BASE_URL + "createEvent";

    public static final String GET_MY_EVENTS = BASE_URL + "getMyEvents";
    public static final String GRAB_COUPONS = BASE_URL + "grabCoupons";


    public static final String HELP_TERMS_AND_CONDITIONS = "";
    public static final String HELP_PRIVACY_POLICY = "";
    public static final String HELP_SAFETY_GUIDELINE = "";
    public static final String HELP_STUX_RULES = "";


}

