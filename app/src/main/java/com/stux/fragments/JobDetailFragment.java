package com.stux.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/02/2016.
 */
public class JobDetailFragment extends Fragment implements View.OnClickListener {

    private ImageView img_banner;
    private CircleImageView cimg_user;
    private TextView txt_c_name, txt_c_inc, txt_c_address, txt_c_company_name, txt_c_company_desc;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.jobs_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        img_banner = (ImageView) view.findViewById(R.id.img_banner);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);

        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_inc = (TextView) view.findViewById(R.id.txt_c_inc);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_company_name = (TextView) view.findViewById(R.id.txt_c_company_name);
        txt_c_company_desc = (TextView) view.findViewById(R.id.txt_c_company_desc);

        view.findViewById(R.id.txt_c_share).setOnClickListener(this);
        view.findViewById(R.id.txt_c_apply).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_share:
                break;
            case R.id.txt_c_apply:
                break;

        }
    }
}
