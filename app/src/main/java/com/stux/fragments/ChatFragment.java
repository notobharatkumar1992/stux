package com.stux.fragments;

import android.app.Service;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.stux.AppDelegate;
import com.stux.R;
import com.stux.Utils.SoftKeyboard;

/**
 * Created by Bharat on 06/06/2016.
 */
public class ChatFragment extends Fragment implements OnClickListener {

    SoftKeyboard softKeyboard;
    //    public static EmojiconEditText editEmojicon;
    private FrameLayout emojicons;
    private RelativeLayout rl_message;
    private ViewGroup rootLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rl_message = (RelativeLayout) view.findViewById(R.id.rl_message);
//        editEmojicon = (EmojiconEditText) view.findViewById(R.id.editEmojicon);
        rootLayout = (ViewGroup) view.findViewById(R.id.rootLayout);

        emojicons = (FrameLayout) view.findViewById(R.id.emojicons);
        view.findViewById(R.id.img_c_emoji).setOnClickListener(this);
        view.findViewById(R.id.img_c_send).setOnClickListener(this);
        attachKeyboardListeners();
    }

//    @Override
//    public void onEmojiconClicked(Emojicon emojicon) {
//        EmojiconsFragment.input(editEmojicon, emojicon);
//    }
//
//    @Override
//    public void onEmojiconBackspaceClicked(View v) {
//        EmojiconsFragment.backspace(editEmojicon);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_emoji:
                AppDelegate.hideKeyBoard(getActivity());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                AppDelegate.LogT("emoji clicked = " + (emojicons.getVisibility() != View.VISIBLE));
                if (emojicons.getVisibility() != View.VISIBLE) {
                    emojicons.setVisibility(View.VISIBLE);
                    layoutParams.addRule(RelativeLayout.ABOVE, R.id.emojicons);
//                    getActivity().getSupportFragmentManager()
//                            .beginTransaction()
//                            .replace(R.id.emojicons, EmojiconsFragment.newInstance(false))
//                            .commit();
                } else {
                    emojicons.setVisibility(View.GONE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                }
                rl_message.setLayoutParams(layoutParams);
                rl_message.requestLayout();
                break;
        }
    }

    protected void attachKeyboardListeners() {
        InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        softKeyboard = new SoftKeyboard(rootLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

            @Override
            public void onSoftKeyboardHide() {
                AppDelegate.LogT("onSoftKeyboardHide called");
            }

            @Override
            public void onSoftKeyboardShow() {
                AppDelegate.LogT("onSoftKeyboardShow called");
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                emojicons.setVisibility(View.GONE);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                rl_message.setLayoutParams(layoutParams);
                rl_message.requestLayout();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (softKeyboard != null)
            softKeyboard.unRegisterSoftKeyboardCallback();
    }
}
