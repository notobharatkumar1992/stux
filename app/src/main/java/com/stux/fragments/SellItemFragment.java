package com.stux.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.stux.Adapters.SpinnerProductCategoryAdapter;
import com.stux.AppDelegate;
import com.stux.Async.LocationAddress;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductCategoryModel;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by Bharat on 07/18/2016.
 */
public class SellItemFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private LinearLayout ll_c_img_layout;
    private ImageView img_c_pic_0, img_c_pic_1, img_c_pic_2, img_c_pic_3, img_c_check;
    private EditText et_name, et_item_description, et_currency, et_location;

    public ArrayList<String> arrayStringProductCondition = new ArrayList<>();
    private ArrayAdapter<String> adapterProductCondition;

    public ArrayList<ProductCategoryModel> arrayProductCategory = new ArrayList<>();
    private SpinnerProductCategoryAdapter adapterCategory;

    private Spinner spn_category, spn_product_type;

    private int selected_type = 0, selected_product_category = 0;

    private Prefs prefs;
    private UserDataModel dataModel;

    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;

    private Handler mHandler;
    private int selected_image = 0;
    private Bitmap bitmap_pic_0, bitmap_pic_1, bitmap_pic_2, bitmap_pic_3;
    public static File capturedFile_0, capturedFile_1, capturedFile_2, capturedFile_3;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private CountDownTimer countDownTimer;
    private String city_name = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sell_an_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        facebookSDKInitialize();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        shareDialog = new ShareDialog(this);  // intialize facebook shareDialog.
        initView(view);
        setHandler();
        callGetProductTypeAsync();
    }

    // Initialize the facebook sdk and then callback manager will handle the login responses.

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callbackManager = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGoogleApiClient.connect();
    }

    private void requestForLocationUpdate() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("request for Location update = " + mCurrentLocation);
        if (!findAddressCalled && mCurrentLocation != null) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Sell an Item");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) view.findViewById(R.id.txt_c_right)).setText("Publish");
        view.findViewById(R.id.txt_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.txt_c_right).setOnClickListener(this);

        ll_c_img_layout = (LinearLayout) view.findViewById(R.id.ll_c_img_layout);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ll_c_img_layout.getLayoutParams();
        layoutParams.height = (AppDelegate.getDeviceWith(getActivity()) - AppDelegate.dpToPix(getActivity(), 45)) / 4;
        ll_c_img_layout.setLayoutParams(layoutParams);
        ll_c_img_layout.invalidate();

        img_c_pic_0 = (ImageView) view.findViewById(R.id.img_c_pic_0);
        img_c_pic_0.setOnClickListener(this);
        img_c_pic_1 = (ImageView) view.findViewById(R.id.img_c_pic_1);
        img_c_pic_1.setOnClickListener(this);
        img_c_pic_2 = (ImageView) view.findViewById(R.id.img_c_pic_2);
        img_c_pic_2.setOnClickListener(this);
        img_c_pic_3 = (ImageView) view.findViewById(R.id.img_c_pic_3);
        img_c_pic_3.setOnClickListener(this);
        img_c_check = (ImageView) view.findViewById(R.id.img_c_check);
        img_c_check.setOnClickListener(this);
        img_c_check.setSelected(false);

        et_name = (EditText) view.findViewById(R.id.et_name);
        et_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_item_description = (EditText) view.findViewById(R.id.et_item_description);
        et_item_description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_currency = (EditText) view.findViewById(R.id.et_currency);
        et_currency.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_location = (EditText) view.findViewById(R.id.et_location);
        et_location.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_location.setEnabled(false);

        if (arrayStringProductCondition.size() == 0) {
            arrayStringProductCondition.add("Item Condition");
            arrayStringProductCondition.add("New");
            arrayStringProductCondition.add("Almost New");
            arrayStringProductCondition.add("Used");
        }

        adapterProductCondition = new ArrayAdapter<>(getActivity(), R.layout.spinner_dropdown_text, arrayStringProductCondition);
        adapterProductCondition.setDropDownViewResource(R.layout.spinner_dropdown_text);
        spn_product_type = (Spinner) view.findViewById(R.id.spn_product_type);
        spn_product_type.setAdapter(adapterProductCondition);
        spn_product_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_type = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (arrayProductCategory.size() == 0) {
            arrayProductCategory.add(new ProductCategoryModel("Select a Category"));
        }

        spn_category = (Spinner) view.findViewById(R.id.spn_category);
        adapterCategory = new SpinnerProductCategoryAdapter(getActivity(), arrayProductCategory);
        spn_category.setAdapter(adapterCategory);
        spn_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_product_category = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private ProductModel productModel;

    public void openFacebook(final ProductModel productModel) {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(productModel);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
//                        if (AccessToken.getCurrentAccessToken() != null)
//                            LoginManager.getInstance().logOut();
//                        if (!isCalledOnce) {
//                            isCalledOnce = true;
//                            openFacebook(productModel);
//                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(productModel);
                                }
                            }
                        }
                    }
                });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    adapterCategory.notifyDataSetChanged();
                    spn_category.invalidate();
                } else if (msg.what == 2) {
                    setResultFromGeoCoderApi(msg.getData());
                }
            }
        };
    }

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
//            placeDetail = new Place();
//            placeDetail.name = bundle.getString(Tags.PLACE_NAME);
//            placeDetail.add = bundle.getString(Tags.PLACE_ADD);
//            placeDetail.latitude = bundle.getDouble(Tags.LAT);
//            placeDetail.longitude = bundle.getDouble(Tags.LNG);
//            placeDetail.city = bundle.getString(Tags.city_param);
//            placeDetail.country = bundle.getString(Tags.country_param);
//            placeDetail.postal_code = bundle.getString(Tags.postalCode);


            city_name = bundle.getString(Tags.PLACE_ADD);
            et_location.setText(city_name);

//            canSearch = false;
//            if (AppDelegate.isValidString(placeDetail.name))
//                et_search.setText(placeDetail.name);
//            else
//                et_search.setText(placeDetail.vicinity);
//            et_search.setSelection(et_search.length());
//
//            list_map_layout.setVisibility(View.GONE);
//            this.latLng = new LatLng(placeDetail.latitude, placeDetail.longitude);
//            if (canCheckCameraChange) {
//                mHandler.sendEmptyMessage(8);
//                animateCamera(this.latLng);
//                mHandler.sendEmptyMessage(11);
//            } else
//                canCheckCameraChange = true;
//            canSearch = true;

//            callAddLocationAsync();
        } else {
            et_location.setEnabled(true);
//            canCheckCameraChange = true;
//            canSearch = true;
            AppDelegate.showToast(getActivity(), "Location not available or something went wrong with server, Please try again later.");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_pic_0:
                selected_image = 0;
                showImageSelectorList();
                break;

            case R.id.img_c_pic_1:
                if (capturedFile_0 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 1st image.");
                    return;
                }
                selected_image = 1;
                showImageSelectorList();
                break;
            case R.id.img_c_pic_2:
                if (capturedFile_1 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 2nd image.");
                    return;
                }
                selected_image = 2;
                showImageSelectorList();
                break;
            case R.id.img_c_pic_3:
                if (capturedFile_2 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 3rd image.");
                    return;
                }
                selected_image = 3;
                showImageSelectorList();
                break;
            case R.id.img_c_check:
                img_c_check.setSelected(img_c_check.isSelected() ? false : true);
                AppDelegate.LogT("img_c_check => " + img_c_check.isSelected());
                break;
            case R.id.txt_c_right:
                callSellItemAsync();
                break;
        }
    }


    private void callGetProductTypeAsync() {
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_PRODUCT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }


    private void callSellItemAsync() {
        if (capturedFile_0 == null && capturedFile_1 == null && capturedFile_2 == null && capturedFile_3 == null) {
            AppDelegate.showAlert(getActivity(), "Please select single image at least.");
        } else if (et_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter title name.");
        } else if (et_item_description.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter description.");
        } else if (selected_product_category == 0) {
            AppDelegate.showAlert(getActivity(), "Please select product category.");
        } else if (spn_product_type.getSelectedItemPosition() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select product condition.");
        } else if (mCurrentLocation == null) {
            AppDelegate.showAlert(getActivity(), "Please make sure your GPS in working and try again later.");
        } else if (et_location.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter location");
        } else if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.cat_id, arrayProductCategory.get(selected_product_category).id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.title, et_name.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.description, et_item_description.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.price, et_currency.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.item_condition, spn_product_type.getSelectedItemPosition() + "");
            if (capturedFile_0 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_1, capturedFile_0.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_1 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_2, capturedFile_1.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_2 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_3, capturedFile_2.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_3 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_4, capturedFile_3.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, mCurrentLocation.getLatitude() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, mCurrentLocation.getLongitude() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, et_location.getText().toString() + "");

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.CREATE_PRODUCT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    private void shareFacebook(ProductModel productModel) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(et_name.getText().toString())
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(productModel.image_1))
                    .setContentDescription(et_item_description.getText().toString())
                    .build();
            shareDialog.show(linkContent);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CREATE_PRODUCT)) {
            mHandler.sendEmptyMessage(11);
            parseProductItemResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PRODUCT_CATEGORY)) {
            mHandler.sendEmptyMessage(11);
            parseProductCategoryResult(result);
        }
    }

    private void parseProductCategoryResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    productCategoryModel.id = jsonObject.getString(Tags.id);
                    productCategoryModel.cat_name = jsonObject.getString(Tags.cat_name);
                    productCategoryModel.status = jsonObject.getString(Tags.id);
                    arrayProductCategory.add(productCategoryModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseProductItemResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                productModel = new ProductModel();
                productModel.id = object.getString(Tags.id);
                productModel.cat_id = object.getString(Tags.cat_id);
                productModel.title = object.getString(Tags.title);
                productModel.description = object.getString(Tags.description);
                productModel.price = object.getString(Tags.price);
                productModel.item_condition = object.getString(Tags.item_condition);

                productModel.image_1 = object.getString(Tags.image_1);
                productModel.image_2 = object.getString(Tags.image_2);
                productModel.image_3 = object.getString(Tags.image_3);
                productModel.image_4 = object.getString(Tags.image_4);

                productModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                productModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                productModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                productModel.image_4_thumb = object.getString(Tags.image_4_thumb);
//                float floatValue = Float.parseFloat(object.getString(Tags.rating));
//                AppDelegate.LogT("floatValue = " + floatValue);
//                productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
//                AppDelegate.LogT("productModel.rating = " + productModel.rating);

//                productModel.sold_status = object.getString(Tags.sold_status);
                productModel.status = object.getString(Tags.status);
                productModel.created = object.getString(Tags.created);
                productModel.modified = object.getString(Tags.modified);
                productModel.total_product_views = object.getString(Tags.total_product_views);
                productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                productModel.pc_id = productModel.cat_id;

//                if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category).toString())) {
//                    JSONObject productObject = object.getJSONObject(Tags.product_category);
//                    productModel.pc_id = productModel.cat_id;
//                    productModel.pc_title = productObject.getString(Tags.cat_name);
//                    productModel.pc_status = productObject.getString(Tags.status);
//                }

                productModel.latitude = object.getString(Tags.latitude);
                productModel.longitude = object.getString(Tags.longitude);

                JSONObject userObject = object.getJSONObject(Tags.user);
                productModel.user_id = userObject.getString(Tags.id);
                productModel.user_first_name = userObject.getString(Tags.first_name);
                productModel.user_last_name = userObject.getString(Tags.last_name);
                productModel.user_email = userObject.getString(Tags.email);
                productModel.user_role = userObject.getString(Tags.role);
                productModel.user_image = userObject.getString(Tags.image);
                productModel.user_social_id = userObject.getString(Tags.social_id);
                productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                productModel.user_following_count = userObject.getInt(Tags.following_count);
                productModel.user_followers_count = userObject.getInt(Tags.followers_count);
                productModel.user_total_product = userObject.getInt(Tags.total_product);
                productModel.user_follow_status = userObject.getInt(Tags.follow_status);

                JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                    productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                    JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                    if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                        productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                    }
                    if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                    } else {
                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                    }
                } else {
                    productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                }
                if (img_c_check.isSelected())
                    openFacebook(productModel);
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }


    //------------image selector-----------------//

    //    private DisplayImageOptions options;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private Bitmap OriginalPhoto;


    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{/*"  Avatar", */"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
//                    case 0:
//                        dialog.dismiss();
//                        showAvatarAlert();
//                        break;
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void writeImageFile() {
        FileOutputStream fOut = null;
        try {
            switch (selected_image) {
                case 0:
                    if (capturedFile_0 == null)
                        capturedFile_0 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_0);
                    break;
                case 1:
                    if (capturedFile_1 == null)
                        capturedFile_1 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_1);
                    break;
                case 2:
                    if (capturedFile_2 == null)
                        capturedFile_2 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_2);
                    break;
                case 3:
                    if (capturedFile_3 == null)
                        capturedFile_3 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_3);
                    break;
            }
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("SellItemFragment onActivityResult Profile => requestCode = " + requestCode + ", resultCode = " + resultCode);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                case AppDelegate.SELECT_PICTURE:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri uri = data.getData();
                        imageUri = uri;
                        AppDelegate.LogT("uri=" + uri);
                        if (uri != null) {
                            // User had pick an image.
                            Cursor cursor = getActivity().getContentResolver()
                                    .query(uri, new String[]{MediaStore.Images.ImageColumns.DATA},
                                            null, null, null);
                            cursor.moveToFirst();

                            // Link to the image
                            final String imageFilePath = cursor.getString(0);
                            cursor.close();
                            int orientation = 0;
                            if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                                try {
                                    ExifInterface ei = new ExifInterface(imageFilePath);
                                    orientation = ei.getAttributeInt(
                                            TAG_ORIENTATION,
                                            ORIENTATION_NORMAL);
                                } catch (IOException e) {
                                    AppDelegate.LogE(e);
                                }

                                Bitmap OriginalPhoto = null;
                                switch (selected_image) {
                                    case 0:
                                        capturedFile_0 = new File(imageFilePath);
                                        OriginalPhoto = AppDelegate.decodeFile(capturedFile_0);
                                        break;
                                    case 1:
                                        capturedFile_1 = new File(imageFilePath);
                                        OriginalPhoto = AppDelegate.decodeFile(capturedFile_1);
                                        break;
                                    case 2:
                                        capturedFile_2 = new File(imageFilePath);
                                        OriginalPhoto = AppDelegate.decodeFile(capturedFile_2);
                                        break;
                                    case 3:
                                        capturedFile_3 = new File(imageFilePath);
                                        OriginalPhoto = AppDelegate.decodeFile(capturedFile_3);
                                        break;
                                }
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                    default:
                                        break;

                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    AppDelegate.LogT("Gallery => " + android.os.Build.VERSION.SDK_INT + " <= " + Build.VERSION_CODES.LOLLIPOP_MR1);
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        switch (selected_image) {
                                            case 0:
                                                bitmap_pic_0 = OriginalPhoto;
                                                img_c_pic_0.setImageBitmap(bitmap_pic_0);
                                                break;
                                            case 1:
                                                bitmap_pic_1 = OriginalPhoto;
                                                img_c_pic_1.setImageBitmap(bitmap_pic_1);
                                                break;
                                            case 2:
                                                bitmap_pic_2 = OriginalPhoto;
                                                img_c_pic_2.setImageBitmap(bitmap_pic_2);
                                                break;
                                            case 3:
                                                bitmap_pic_3 = OriginalPhoto;
                                                img_c_pic_3.setImageBitmap(bitmap_pic_3);
                                                break;
                                        }
//                                        execute_updateProfileApi();
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                    break;
                case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                    AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);

                    //Get our saved file into a bitmap object:
                    try {
                        if (resultCode == Activity.RESULT_OK) {
                            int orientation = 0;
                            Uri uri = null;
                            BitmapFactory.Options o = new BitmapFactory.Options();
                            o.inJustDecodeBounds = true;

                            Bitmap OriginalPhoto = null;
                            switch (selected_image) {
                                case 0:
                                    uri = Uri.fromFile(new File(capturedFile_0.getAbsolutePath()));
                                    OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile_0.getAbsolutePath(), 1000, 700);
                                    break;
                                case 1:
                                    uri = Uri.fromFile(new File(capturedFile_1.getAbsolutePath()));
                                    OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile_1.getAbsolutePath(), 1000, 700);
                                    break;
                                case 2:
                                    uri = Uri.fromFile(new File(capturedFile_2.getAbsolutePath()));
                                    OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile_2.getAbsolutePath(), 1000, 700);
                                    break;
                                case 3:
                                    uri = Uri.fromFile(new File(capturedFile_3.getAbsolutePath()));
                                    OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile_3.getAbsolutePath(), 1000, 700);
                                    break;
                            }

                            Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                            try {
                                ExifInterface ei = new ExifInterface(
                                        uri.getPath());

                                orientation = ei.getAttributeInt(
                                        ExifInterface.TAG_ORIENTATION,
                                        ExifInterface.ORIENTATION_NORMAL);

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);

                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = OriginalPhoto;
                                        switch (selected_image) {
                                            case 0:
                                                bitmap_pic_0 = OriginalPhoto;
                                                img_c_pic_0.setImageBitmap(bitmap_pic_0);
                                                break;
                                            case 1:
                                                bitmap_pic_1 = OriginalPhoto;
                                                img_c_pic_1.setImageBitmap(bitmap_pic_1);
                                                break;
                                            case 2:
                                                bitmap_pic_2 = OriginalPhoto;
                                                img_c_pic_2.setImageBitmap(bitmap_pic_2);
                                                break;
                                            case 3:
                                                bitmap_pic_3 = OriginalPhoto;
                                                img_c_pic_3.setImageBitmap(bitmap_pic_3);
                                                break;
                                        }
                                    }

                                } else {
                                    AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                                }

                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    break;
                case AppDelegate.PIC_CROP: {
                    AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                    try {
                        if (data.getData() == null) {
                            OriginalPhoto = (Bitmap) data.getExtras().get("data");
                        } else {
                            OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        }

//                    cropimageUri = data.getData();
                        if (resultCode == Activity.RESULT_OK) {
                            if (OriginalPhoto != null) {
                                OriginalPhoto.recycle();
                                OriginalPhoto = null;
                            }
                            OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                            AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                            if (OriginalPhoto == null) {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                return;
                            }
                            OriginalPhoto = Bitmap.createScaledBitmap(
                                    OriginalPhoto, 240, 240, true);
                            AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
//                        cimg_user.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));
                            switch (selected_image) {
                                case 0:
                                    bitmap_pic_0 = OriginalPhoto;
                                    img_c_pic_0.setImageBitmap(bitmap_pic_0);
                                    break;
                                case 1:
                                    bitmap_pic_1 = OriginalPhoto;
                                    img_c_pic_1.setImageBitmap(bitmap_pic_1);
                                    break;
                                case 2:
                                    bitmap_pic_2 = OriginalPhoto;
                                    img_c_pic_2.setImageBitmap(bitmap_pic_2);
                                    break;
                                case 3:
                                    bitmap_pic_3 = OriginalPhoto;
                                    img_c_pic_3.setImageBitmap(bitmap_pic_3);
                                    break;
                            }

                            FileOutputStream fOut = null;
                            try {
                                switch (selected_image) {
                                    case 0:
                                        capturedFile_0 = new File(getNewFile());
                                        fOut = new FileOutputStream(capturedFile_0);
                                        break;
                                    case 1:
                                        capturedFile_1 = new File(getNewFile());
                                        fOut = new FileOutputStream(capturedFile_1);
                                        break;
                                    case 2:
                                        capturedFile_2 = new File(getNewFile());
                                        fOut = new FileOutputStream(capturedFile_2);
                                        break;
                                    case 3:
                                        capturedFile_3 = new File(getNewFile());
                                        fOut = new FileOutputStream(capturedFile_3);
                                        break;
                                }
                            } catch (FileNotFoundException e) {
                                AppDelegate.LogE(e);
                            }

                            OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                            try {
                                fOut.flush();
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            try {
                                fOut.close();
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            // getActivity().getContentResolver().delete(cropimageUri, null, null);
                        } else {
                            AppDelegate.LogE("at onActivicTyResult failed to crop");
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
            AppDelegate.LogE(e);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    boolean findAddressCalled = false;

    @Override
    public void onLocationChanged(Location location) {
        if (location != null)
            mCurrentLocation = location;
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if (!findAddressCalled) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AppDelegate.LogT("onConnected called");
        requestForLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppDelegate.LogT("onConnectionSuspended called");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppDelegate.LogT("onConnectionFailed called");
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        AppDelegate.LogT("setLatLngAndFindAddress called");
        findAddressCalled = true;
        countDownTimer = new CountDownTimer(countDownTime, countDownTime) {

            @Override
            public void onTick(long millisUntilFinished) {
                AppDelegate.LogT("timer = " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(
                        arg0.latitude, arg0.longitude,
                        getActivity(), mHandler);
            }
        };
        countDownTimer.start();
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            switch (selected_image) {
                case 0:
                    imageURI = Uri.fromFile(capturedFile_0);
                    break;

                case 1:
                    imageURI = Uri.fromFile(capturedFile_1);
                    break;

                case 2:
                    imageURI = Uri.fromFile(capturedFile_2);
                    break;

                case 3:
                    imageURI = Uri.fromFile(capturedFile_3);
                    break;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            try {
                switch (selected_image) {
                    case 0:
                        capturedFile_0 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_0.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_0.getAbsolutePath());
                            return capturedFile_0.getAbsolutePath();
                        }
                        break;

                    case 1:
                        capturedFile_1 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_1.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_1.getAbsolutePath());
                            return capturedFile_1.getAbsolutePath();
                        }
                        break;

                    case 2:
                        capturedFile_2 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_2.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_2.getAbsolutePath());
                            return capturedFile_2.getAbsolutePath();
                        }
                        break;
                    case 3:
                        capturedFile_3 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_3.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_3.getAbsolutePath());
                            return capturedFile_3.getAbsolutePath();
                        }
                        break;

                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }
}
