package com.stux.fragments;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.AppDelegate;
import com.stux.R;
import com.stux.constants.Tags;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/25/2016.
 */
public class HelpFragment extends Fragment implements View.OnClickListener {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.help, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Help");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        view.findViewById(R.id.txt_c_terms_condition).setOnClickListener(this);
        view.findViewById(R.id.txt_c_privacy_policy).setOnClickListener(this);
        view.findViewById(R.id.txt_c_safety_guide).setOnClickListener(this);
        view.findViewById(R.id.txt_c_rules).setOnClickListener(this);

        view.findViewById(R.id.txt_c_like_fb).setOnClickListener(this);
        view.findViewById(R.id.txt_c_like_twitter).setOnClickListener(this);

        view.findViewById(R.id.txt_c_faq).setOnClickListener(this);
        view.findViewById(R.id.txt_c_contact_us).setOnClickListener(this);
        view.findViewById(R.id.txt_c_software).setOnClickListener(this);
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            ((TextView) view.findViewById(R.id.txt_c_version)).setText(pInfo.versionCode + "");
        } catch (PackageManager.NameNotFoundException e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.txt_c_terms_condition:
                Bundle bundle = new Bundle();
                bundle.putString(Tags.FROM, "Terms & conditions");
                Fragment fragment = new WebViewDetailFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

        }
    }
}
