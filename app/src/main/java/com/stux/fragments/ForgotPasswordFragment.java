package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by NOTO on 5/24/2016.
 */
public class ForgotPasswordFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private EditText et_email;

    public Prefs prefs;
    public Handler mHandler;
    public AppDelegate mInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInstance = AppDelegate.getInstance(getActivity());
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void initView(View view) {
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));

        view.findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                if (AppDelegate.haveNetworkConnection(getActivity(), false))
                    validateData();
                else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                break;
        }
    }

    private void validateData() {
        if (et_email.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please Enter Your Email Address.");
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString()))/* || !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(getActivity(), getString(R.string.please_fill_email_address_in_format));
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {

            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            mInstance.setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ForgotPasswordFragment.this, ServerRequestConstants.FORGOT_PASSWORD,
                    mPostArrayList, ForgotPasswordFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FORGOT_PASSWORD)) {
            parseForgotPassword(result);
        }
    }

    private void parseForgotPassword(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new ResetPasswordFragment());
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
    }
}
