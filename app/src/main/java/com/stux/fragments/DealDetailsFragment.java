package com.stux.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.google.android.gms.maps.MapsInitializer;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.LargeImageActivity;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class DealDetailsFragment extends Fragment implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse {

    private TextView txt_c_detail_name, txt_c_category, txt_c_viewer, txt_c_area, txt_c_new_price, txt_c_old_price, txt_c_percent, txt_c_address_1, txt_c_address_2, txt_c_contact, txt_c_coupon, txt_c_address;
    private carbon.widget.ImageView img_c_deal_banner;
    private ImageView img_loading;
    private ScrollView scrollView;

    private DealModel dealModel;
    private Prefs prefs;
    private UserDataModel dataModel;

    public ArrayList<String> arrayString = new ArrayList<>();
    private android.widget.LinearLayout pager_indicator;
    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;

    private Handler mHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deals_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        dealModel = getArguments().getParcelable(Tags.deal);
        initView(view);
        setValues();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                }
            }
        };
    }

    private void setValues() {
        if (dealModel != null) {

            txt_c_detail_name.setText(dealModel.title);
            txt_c_viewer.setText(dealModel.view_count);
            txt_c_category.setText("CATEGORY: " + dealModel.product_category);
            txt_c_old_price.setText(" N" + dealModel.price + " ");
            txt_c_old_price.setPaintFlags(txt_c_old_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txt_c_new_price.setText("N" + (Integer.parseInt(dealModel.price) - Integer.parseInt(dealModel.discount_price)));
            txt_c_percent.setText(dealModel.discount + "%");

            txt_c_address_1.setText(dealModel.venue);
            txt_c_address_2.setText(dealModel.venue);

            txt_c_contact.setText("Contact: " + dealModel.emailid);
            txt_c_address.setText("Address: " + dealModel.venue);

//            1=for all students , 2 = for particular institutes
            txt_c_area.setText("AREA: " + (dealModel.deal_area.equalsIgnoreCase("1") ? "ALL STUDENTS" : "PARTICULAR INSTITUTES"));

            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
//            Picasso.with(getActivity()).load(dealModel.image_1_thumb).into(img_c_deal_banner, new Callback() {
//                @Override
//                public void onSuccess() {
//                    img_loading.setVisibility(View.GONE);
//                }
//
//                @Override
//                public void onError() {
//                    img_loading.setVisibility(View.GONE);
//                }
//            });
            img_c_deal_banner.setVisibility(View.GONE);
        }
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Deal Detail");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        txt_c_detail_name = (TextView) view.findViewById(R.id.txt_c_detail_name);
        txt_c_viewer = (TextView) view.findViewById(R.id.txt_c_viewer);
        txt_c_category = (TextView) view.findViewById(R.id.txt_c_category);
        txt_c_area = (TextView) view.findViewById(R.id.txt_c_area);
        txt_c_new_price = (TextView) view.findViewById(R.id.txt_c_new_price);
        txt_c_old_price = (TextView) view.findViewById(R.id.txt_c_old_price);
        txt_c_percent = (TextView) view.findViewById(R.id.txt_c_percent);
        txt_c_address_1 = (TextView) view.findViewById(R.id.txt_c_address_1);
        txt_c_address_2 = (TextView) view.findViewById(R.id.txt_c_address_2);
        txt_c_contact = (TextView) view.findViewById(R.id.txt_c_contact);
        txt_c_coupon = (TextView) view.findViewById(R.id.txt_c_coupon);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);

        img_c_deal_banner = (carbon.widget.ImageView) view.findViewById(R.id.img_c_deal_banner);
        img_c_deal_banner.setOnClickListener(this);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        view.findViewById(R.id.txt_c_grab).setOnClickListener(this);

        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);
        if (arrayString.size() == 0)
            if (AppDelegate.isValidString(dealModel.image_4_thumb)) {
                arrayString.add(dealModel.image_1_thumb);
                arrayString.add(dealModel.image_2_thumb);
                arrayString.add(dealModel.image_3_thumb);
                arrayString.add(dealModel.image_4_thumb);
            } else if (AppDelegate.isValidString(dealModel.image_3_thumb)) {
                arrayString.add(dealModel.image_1_thumb);
                arrayString.add(dealModel.image_2_thumb);
                arrayString.add(dealModel.image_3_thumb);
            } else if (AppDelegate.isValidString(dealModel.image_2_thumb)) {
                arrayString.add(dealModel.image_1_thumb);
                arrayString.add(dealModel.image_2_thumb);
            } else if (AppDelegate.isValidString(dealModel.image_1_thumb)) {
                arrayString.add(dealModel.image_1_thumb);
            } else {
//                arrayString.add("");
            }
        setUiPageViewController();
        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.orange_radius_square);
        }
    }

    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = arrayString.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(15, 15);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;

            case R.id.txt_c_grab:
                showDialogGrab();
                break;

            case R.id.img_c_deal_banner:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Tags.image, dealModel.banner_image);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                break;
        }
    }


    private void showDialogGrab() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_deal_coupen);
        carbon.widget.ImageView img_c_coupon = (carbon.widget.ImageView) dialog.findViewById(R.id.img_c_coupon);
        Picasso.with(getActivity()).load(dealModel.image_1_thumb).into(img_c_coupon, new Callback() {
            @Override
            public void onSuccess() {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                img_loading.setVisibility(View.GONE);
            }
        });

        TextView txt_c_coupon = (TextView) dialog.findViewById(R.id.txt_c_coupon);
        txt_c_coupon.setText(dealModel.coupon_code);

        TextView txt_coupon_time = (TextView) dialog.findViewById(R.id.txt_coupon_time);
        if (AppDelegate.isValidString(dealModel.expiry_date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(Long.parseLong(dealModel.expiry_date));
            AppDelegate.LogT("Expiry => " + calendar.getTime());
            txt_coupon_time.setText("Coupon will expire in " + AppDelegate.getTimeForchat(calendar.getTime()));
            txt_coupon_time.setVisibility(View.VISIBLE);
        } else {
            AppDelegate.LogT("dealModel.expiry_date = " + dealModel.expiry_date);
            txt_coupon_time.setVisibility(View.GONE);
        }

        dialog.findViewById(R.id.txt_c_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callGrabCouponAsync();
            }
        });
        dialog.show();
    }

    private void callGrabCouponAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.deal_id, dealModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.coupon_code, dealModel.coupon_code);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GRAB_COUPONS,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                Bundle bundle = new Bundle();
                if (position == 0) {
                    bundle.putString(Tags.image, dealModel.image_1);
                } else if (position == 1) {
                    bundle.putString(Tags.image, dealModel.image_2);
                } else if (position == 2) {
                    bundle.putString(Tags.image, dealModel.image_3);
                } else if (position == 3) {
                    bundle.putString(Tags.image, dealModel.image_4);
                }
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GRAB_COUPONS)) {
            mHandler.sendEmptyMessage(11);
            parseGrabResult(result);
        }
    }

    private void parseGrabResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
