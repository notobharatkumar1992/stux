package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bulletnoid.android.widget.StaggeredGridView.StaggeredGridView;
import com.stux.Adapters.CampusListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.ScrollViewExt;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.interfaces.ScrollViewListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/27/2016.
 */
public class CampusListFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener, OnListItemClickListenerWithHeight {

    private Handler mHandler;
    private ArrayList<ProductModel> productArray = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    // Campus list
    private CampusListAdapter mCampusListAdapter;
    private StaggeredGridView stgv;
    private int campusCounter = 1, campusTotalPage = -1;
    private TextView txt_c_no_list;

    private boolean campusAsyncExcecuting = false;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.campus_list_staggerd_grid_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        if (productArray.size() == 0)
            callCampusListAsync();
        else
            mHandler.sendEmptyMessage(2);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "5");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.CAMPUS_LIST,
                    mPostArrayList, null);
            if (!campusAsyncExcecuting)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified ");
                    txt_c_no_list.setVisibility(productArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No product available");
                    mCampusListAdapter.notifyDataSetChanged();
                    stgv.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        mCampusListAdapter = new CampusListAdapter(getActivity(), R.id.txt_line1, productArray, this, this);

        stgv = (StaggeredGridView) view.findViewById(R.id.stgv);
        stgv.setItemMargin(15);
//        stgv.setPadding(8, 0, 8, 0);
        stgv.setAdapter(mCampusListAdapter);

        stgv.setOnLoadmoreListener(new StaggeredGridView.OnLoadmoreListener() {
            @Override
            public void onLoadmore() {
                if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                    mHandler.sendEmptyMessage(2);
                    callCampusListAsync();
                    campusAsyncExcecuting = true;
                }
            }
        });

        ((ScrollViewExt) view.findViewById(R.id.scroll_view))
                .setScrollViewListener(
                        new ScrollViewListener() {
                            @Override
                            public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
                                // We take the last son in the scrollview
                                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
                                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
//                AppDelegate.LogT("scrollview setScrollViewListener = " + diff);
                                // if diff is zero, then the bottom has been reached
                                if (diff == 0) {
                                    // do stuff
                                    AppDelegate.LogT("onScrollChanged Called, campusAsyncExcecuting = " + campusAsyncExcecuting + ", campusTotalPage = " + campusTotalPage);
                                    if (campusTotalPage != 0 && !campusAsyncExcecuting) {
//                                        footerView1.setVisibility(View.VISIBLE);
                                        mHandler.sendEmptyMessage(2);
                                        callCampusListAsync();
                                        campusAsyncExcecuting = true;
//                                        stgv.smoothScrollToPosition(gridViewCampusList.getAdapter().getCount());
//                                        stgv.setSelection(gridViewCampusList.getAdapter().getCount());
                                    }
                                }
                            }
                        }

                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CAMPUS_LIST)) {
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            }
            parseCampusListResult(result);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                campusTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    ProductModel productModel = new ProductModel();
                    productModel.id = object.getString(Tags.id);
                    productModel.cat_id = object.getString(Tags.cat_id);
                    productModel.title = object.getString(Tags.title);
                    productModel.description = object.getString(Tags.description);
                    productModel.price = object.getString(Tags.price);
                    productModel.item_condition = object.getString(Tags.item_condition);

                    productModel.image_1 = object.getString(Tags.image_1);
                    productModel.image_2 = object.getString(Tags.image_2);
                    productModel.image_3 = object.getString(Tags.image_3);
                    productModel.image_4 = object.getString(Tags.image_4);

                    productModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                    productModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                    productModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                    productModel.image_4_thumb = object.getString(Tags.image_4_thumb);
                    float floatValue = Float.parseFloat(object.getString(Tags.rating));
                    AppDelegate.LogT("floatValue = " + floatValue);
                    productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
                    AppDelegate.LogT("productModel.rating = " + productModel.rating);

                    productModel.sold_status = object.getString(Tags.sold_status);
                    productModel.status = object.getString(Tags.status);
                    productModel.created = object.getString(Tags.created);
                    productModel.modified = object.getString(Tags.modified);
                    productModel.total_product_views = object.getString(Tags.total_product_views);
                    productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                    JSONObject productObject = object.getJSONObject(Tags.product_category);
                    productModel.pc_id = productObject.getString(Tags.id);
                    productModel.pc_title = productObject.getString(Tags.title);
                    productModel.pc_description = productObject.getString(Tags.description);
                    productModel.pc_meta_tags = productObject.getString(Tags.meta_tags);
                    productModel.pc_meta_keywords = productObject.getString(Tags.meta_keywords);
                    productModel.pc_status = productObject.getString(Tags.status);
                    productModel.pc_lft = productObject.getString(Tags.lft);
                    productModel.pc_rght = productObject.getString(Tags.rght);
                    productModel.pc_parent_id = productObject.getString(Tags.parent_id);

                    productModel.latitude = object.getString(Tags.latitude);
                    productModel.longitude = object.getString(Tags.longitude);

                    JSONObject userObject = object.getJSONObject(Tags.user);
                    productModel.user_id = userObject.getString(Tags.id);
                    productModel.user_first_name = userObject.getString(Tags.first_name);
                    productModel.user_last_name = userObject.getString(Tags.last_name);
                    productModel.user_email = userObject.getString(Tags.email);
                    productModel.user_role = userObject.getString(Tags.role);
                    productModel.user_image = userObject.getString(Tags.image);
                    productModel.user_social_id = userObject.getString(Tags.social_id);
                    productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                    productModel.user_following_count = userObject.getInt(Tags.following_count);
                    productModel.user_followers_count = userObject.getInt(Tags.followers_count);
                    productModel.user_total_product = userObject.getInt(Tags.total_product);
                    productModel.user_follow_status = userObject.getInt(Tags.follow_status);

                    JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                    productModel.user_dob = studentObject.getString(Tags.dob);
                    if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                        productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                        productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                        JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                        if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                            productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                        }
                        if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        } else {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                    } else {
                        productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                    }
                    productArray.add(productModel);
                }
            } else {
                campusTotalPage = 0;
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.product)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productArray.get(position));
            Fragment fragment = new ProductDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int height) {
        if (name.equalsIgnoreCase(Tags.HEIGHT)) {
            productArray.get(position).height = height;

            int heightValue1 = 0, heightValue2 = 0;
            ViewGroup.LayoutParams params = stgv.getLayoutParams();

            for (int i = 0; i < productArray.size(); i++) {
                AppDelegate.LogT(" productArray.get(i).height = " + productArray.get(i).height);
                if (i % 2 == 0)
                    heightValue1 += productArray.get(i).height + AppDelegate.dpToPix(getActivity(), 140);
                else
                    heightValue2 += productArray.get(i).height + AppDelegate.dpToPix(getActivity(), 140);
            }
            AppDelegate.LogT("Campus list => heightValue1 = " + heightValue1 + ", heightValue2 = " + heightValue2);
            if (heightValue1 > heightValue2) {
                params.height = heightValue1;
            } else {
                params.height = heightValue2;
            }

            stgv.setLayoutParams(params);
            stgv.requestLayout();
//            stgv.setSelectionToTop();
            stgv.setVerticalScrollbarPosition(0);
        }
    }
}
