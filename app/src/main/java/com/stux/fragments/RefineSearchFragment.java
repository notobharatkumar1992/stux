package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.appyvet.rangebar.RangeBar;
import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.R;
import com.stux.activities.MainActivity;
import com.stux.interfaces.OnReciveServerResponse;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/13/2016.
 */
public class RefineSearchFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private ArrayList<String> arrayCondition = new ArrayList<>();
    private SpinnerArrayStringAdapter adapterCondition;
    private EditText et_search;
    private Spinner spn_campus_list, spn_condition;
    private ImageView img_c_checkbox, img_c_checkbox_upload;

    private RangeBar rangebar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.refine_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Refine Search");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        et_search = (EditText) view.findViewById(R.id.et_search);
        et_search.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));

        spn_campus_list = (Spinner) view.findViewById(R.id.spn_campus_list);

        arrayCondition.add("Select Condition");
        arrayCondition.add("New");
        arrayCondition.add("Almost New");
        arrayCondition.add("Used");
        spn_condition = (Spinner) view.findViewById(R.id.spn_condition);
        adapterCondition = new SpinnerArrayStringAdapter(getActivity(), arrayCondition);
        spn_condition.setAdapter(adapterCondition);

        img_c_checkbox = (ImageView) view.findViewById(R.id.img_c_checkbox);
        img_c_checkbox.setOnClickListener(this);
        img_c_checkbox_upload = (ImageView) view.findViewById(R.id.img_c_checkbox_upload);
        img_c_checkbox_upload.setOnClickListener(this);

        rangebar = (RangeBar) view.findViewById(R.id.rangebar);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;

            case R.id.img_c_checkbox:
                break;

            case R.id.img_c_checkbox_upload:
                break;

        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {

    }
}
