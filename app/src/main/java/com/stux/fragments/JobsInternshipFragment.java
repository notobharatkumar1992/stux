package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.Adapters.PagerAdapter;
import com.stux.R;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/02/2016.
 */
public class JobsInternshipFragment extends Fragment implements View.OnClickListener {

    private TextView txt_c_internship, txt_c_jobs;
    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.jobs_internship_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        txt_c_internship = (TextView) view.findViewById(R.id.txt_c_internship);
        txt_c_internship.setOnClickListener(this);
        txt_c_jobs = (TextView) view.findViewById(R.id.txt_c_jobs);
        txt_c_jobs.setOnClickListener(this);

        fragments.add(new CampusListFragment());
        fragments.add(new CampusListFragment());

        pager = (ViewPager) view.findViewById(R.id.pager);
        pagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
        pager.setAdapter(pagerAdapter);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        pager.setCurrentItem(0);
        switchPage(0);
    }

    private void switchPage(int value) {
        txt_c_internship.setSelected(false);
        txt_c_jobs.setSelected(false);
        txt_c_internship.setTextColor(getResources().getColor(R.color.profile_desc));
        txt_c_jobs.setTextColor(getResources().getColor(R.color.profile_desc));
        switch (value) {
            case 0:
                txt_c_internship.setSelected(true);
                txt_c_internship.setTextColor(Color.WHITE);
                break;
            case 1:
                txt_c_jobs.setSelected(true);
                txt_c_jobs.setTextColor(Color.WHITE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_internship:
                pager.setCurrentItem(0);
                break;
            case R.id.txt_c_jobs:
                pager.setCurrentItem(1);
                break;
        }
    }
}
