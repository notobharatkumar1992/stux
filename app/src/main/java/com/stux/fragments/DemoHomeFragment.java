package com.stux.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.PagerAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.MoviesModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.DealRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.MoviesRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.ProductRecyclerViewAdapter;

/**
 * Created by NOTO on 5/27/2016.
 */
public class DemoHomeFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnListItemClickListener, OnListItemClickListenerWithHeight {

    public static ArrayList<ProductModel> productArray = new ArrayList<>();
    private TextView txt_c_header;
    private ViewPager /*view_pager,*/ view_pager_banner;
    private PagerAdapter /*pagerAdapter,*/ bannerPagerAdapter;
    //    private HomeBannerAdapter bannerAdapter;
    private LinearLayout ll_c_campus_list, ll_c_top_deals, ll_c_events;
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private ArrayList<Fragment> bannerFragment = new ArrayList<>();
    private ArrayList<SliderModel> sliderArray = new ArrayList<>();
    private RelativeLayout rl_c_banner;
    private Handler mHandler;
    private ProgressBar progressbar/*, progressbar_1*/;
    private android.widget.LinearLayout pager_indicator;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;
    private TextView txt_c_shop_now/*, txt_c_no_list*/;
    private Prefs prefs;
    //    private ScrollViewExt scrollview;
    private UserDataModel dataModel;
//    private RelativeLayout rl_c_main;

    // Campus list
//    private CampusListAdapter mCampusListAdapter;
//    private StaggeredGridView stgv;
    private RecyclerView recyclerView;
    private ProductRecyclerViewAdapter rcAdapter;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private int campusCounter = 1, campusTotalPage = -1;

    private SpacesItemDecoration campusItemDecoration;
    private SpacesItemDecoration dealItemDecoration;
    private SpacesItemDecoration moviesItemDecoration;

    // Deal list
    private ArrayList<DealModel> dealArray = new ArrayList<>();
    //    private ListView mDealList;
//    private DealListAdapter mDealAdapter;
    private DealRecyclerViewAdapter dealAdapter;
    private LinearLayoutManager linearLayoutManager;

    private int dealCounter = 1, dealTotalPage = -1;

    // Movies list
//    private ListView event_list_view;
//    private EventListAdapter mEventAdapter;
    private MoviesRecyclerViewAdapter moviesAdapter;
    private ArrayList<MoviesModel> moviesArray = new ArrayList<>();
    private int moviesCounter = 1, moviesTotalPage = -1;

    private PostAsync sliderAsync;

    private boolean campusAsyncExcecuting = false, dealAsyncExcecuting = false, moviesAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppDelegate.LogT("HomeFragment onCreateView called");
        return inflater.inflate(R.layout.demo_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppDelegate.LogT("HomeFragment onViewCreated called");
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        sliderArray.clear();
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_shop_now.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 14) {
//                    progressbar_1.setVisibility(View.VISIBLE);
//                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 15) {
//                    progressbar_1.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    AppDelegate.LogT("sliderArray = " + sliderArray.size());
//                    bannerAdapter = new HomeBannerAdapter(getActivity(), sliderArray, HomeFragment.this);
//                    view_pager_banner.setAdapter(bannerAdapter);
                    bannerFragment.clear();
                    for (int i = 0; i < sliderArray.size(); i++) {
                        Fragment fragment = new BannerHomeFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.slider_id, sliderArray.get(i));
                        fragment.setArguments(bundle);
                        bannerFragment.add(fragment);
                    }

                    bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
                    view_pager_banner.setAdapter(bannerPagerAdapter);

                    setUiPageViewController();
                    if (sliderArray.size() == 0) {
                        txt_c_shop_now.setVisibility(View.VISIBLE);
                        if (selected_tab == 0) {
                            txt_c_shop_now.setText("Top Campus banner are not available.");
                        } else if (selected_tab == 1) {
                            txt_c_shop_now.setText("Top Deals banner are not available.");
                        } else if (selected_tab == 2) {
                            txt_c_shop_now.setText("Top Events banner are not available.");
                        }
                    } else {
                        txt_c_shop_now.setVisibility(View.GONE);
                        rl_c_banner.setVisibility(View.VISIBLE);
                        view_pager_banner.setVisibility(View.VISIBLE);
                    }
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified => selected_tab = " + selected_tab);
                    if (selected_tab == 0) {
//                        txt_c_no_list.setVisibility(productArray.size() > 0 ? View.GONE : View.VISIBLE);
//                        txt_c_no_list.setText("No product available");
                        rcAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }
                } else if (msg.what == 3) {
                    if (selected_tab == 1) {
                        AppDelegate.LogT("mDealAdapter notified = " + dealArray.size() + ",  => selected_tab = " + selected_tab);
//                        txt_c_no_list.setVisibility(dealArray.size() > 0 ? View.GONE : View.VISIBLE);
//                        txt_c_no_list.setText("No deal available");
//                        mDealAdapter.notifyDataSetChanged();
//                        mDealList.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                mDealList.invalidate();
//                                AppDelegate.setListViewHeight(getActivity(), mDealList, mDealAdapter, footerView2);
//                                mDealList.smoothScrollToPosition(0);
//                                mDealList.setSelection(0);
//                                mDealList.setVerticalScrollbarPosition(0);
//                            }
//                        }, 1000);
                        dealAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }
                } else if (msg.what == 4) {
                    if (selected_tab == 2) {
                        AppDelegate.LogT("mMoviesAdapter notified = " + moviesArray.size() + ",  => selected_tab = " + selected_tab);
//                        txt_c_no_list.setVisibility(eventArray.size() > 0 ? View.GONE : View.VISIBLE);
//                        txt_c_no_list.setText("No event available");
//                        mEventAdapter.notifyDataSetChanged();
//                        event_list_view.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                event_list_view.invalidate();
//                                AppDelegate.setListViewHeight(getActivity(), event_list_view, mEventAdapter, footerView3);
//                                event_list_view.smoothScrollToPosition(0);
//                                event_list_view.setSelection(0);
//                                event_list_view.setVerticalScrollbarPosition(0);
//                            }
//                        }, 1000);
                        moviesAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                    }

                }
            }
        };
    }

    private void initView(View view) {
//        scrollview = (ScrollViewExt) view.findViewById(R.id.scrollview);
//        scrollview.setScrollViewListener(new ScrollViewListener() {
//            @Override
//            public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
//                // We take the last son in the scrollview
//                View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
//                int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
////                AppDelegate.LogT("scrollview setScrollViewListener = " + diff);
//                // if diff is zero, then the bottom has been reached
//                if (diff == 0) {
//                    // do stuff
//                    switch (selected_tab) {
//                        case 0:
//                            AppDelegate.LogT("onScrollChanged Called, campusAsyncExcecuting = " + campusAsyncExcecuting + ", campusTotalPage = " + campusTotalPage);
//                            if (campusTotalPage != 0 && !campusAsyncExcecuting) {
//                                footerView1.setVisibility(View.VISIBLE);
//                                mHandler.sendEmptyMessage(2);
//                                callCampusListAsync();
//                                campusAsyncExcecuting = true;
////                                stgv.setVerticalScrollbarPosition(stgv.getAdapter().getCount());
//                            }
//                            break;
//                        case 1:
//                            AppDelegate.LogT("onScrollChanged Called, dealAsyncExcecuting = " + dealAsyncExcecuting + ", dealTotalPage = " + dealTotalPage);
//                            if (dealTotalPage != 0 && !dealAsyncExcecuting) {
//                                footerView2.setVisibility(View.VISIBLE);
////                                mDealList.addFooterView(footerView2);
//                                mHandler.sendEmptyMessage(3);
//                                callDealsListAsync();
//                                dealAsyncExcecuting = true;
////                                mDealList.smoothScrollToPosition(0);
////                                mDealList.setSelection(0);
////                                mDealList.setVerticalScrollbarPosition(0);
//                            }
//                            break;
//                        case 2:
//                            AppDelegate.LogT("onScrollChanged Called, eventAsyncExcecuting = " + eventAsyncExcecuting + ", eventTotalPage = " + eventTotalPage);
//                            if (eventTotalPage != 0 && !eventAsyncExcecuting) {
//                                footerView3.setVisibility(View.VISIBLE);
////                                event_list_view.addFooterView(footerView3);
//                                mHandler.sendEmptyMessage(4);
//                                callEventListAsync();
//                                eventAsyncExcecuting = true;
////                                event_list_view.smoothScrollToPosition(0);
////                                event_list_view.setSelection(0);
//                            }
//                            break;
//                    }
//                }
//            }
//        });

        rl_c_banner = (RelativeLayout) view.findViewById(R.id.rl_c_banner);
//        rl_c_main = (RelativeLayout) view.findViewById(R.id.rl_c_main);

        android.widget.LinearLayout.LayoutParams layoutParams = (android.widget.LinearLayout.LayoutParams) rl_c_banner.getLayoutParams();
        layoutParams.height = AppDelegate.getDeviceWith(getActivity()) / 2 + AppDelegate.dpToPix(getActivity(), 60);
        rl_c_banner.setLayoutParams(layoutParams);

        int height = AppDelegate.getDeviceWith(getActivity()) / 2 + AppDelegate.dpToPix(getActivity(), 29) + AppDelegate.dpToPix(getActivity(), (int) getResources().getDimension(R.dimen.header_height));
        AppBarLayout appbar = (AppBarLayout) view.findViewById(R.id.app_bar);
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        lp.height = height;
        appbar.setLayoutParams(lp);
        appbar.invalidate();

        view.findViewById(R.id.txt_c_header).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_header).setVisibility(View.VISIBLE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        ll_c_campus_list = (LinearLayout) view.findViewById(R.id.ll_c_campus_list);
        ll_c_campus_list.setOnClickListener(this);
        ll_c_top_deals = (LinearLayout) view.findViewById(R.id.ll_c_top_deals);
        ll_c_top_deals.setOnClickListener(this);
        ll_c_events = (LinearLayout) view.findViewById(R.id.ll_c_events);
        ll_c_events.setOnClickListener(this);

        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

//        progressbar_1 = (ProgressBar) view.findViewById(R.id.progressbar_1);
//        progressbar_1.setVisibility(View.GONE);

        txt_c_shop_now = (TextView) view.findViewById(R.id.txt_c_shop_now);
//        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);

        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);

        if (fragments.size() == 0) {
            fragments.add(new CampusListFragment());
            fragments.add(new DealsListFragment());
            fragments.add(new EventListFragment());
        }

//        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
//        pagerAdapter = new PagerAdapter(getChildFragmentManager(), fragments);
//        view_pager.setAdapter(pagerAdapter);
//        view_pager.setVisibility(View.GONE);
//        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                switchPage(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//            }
//        });
//        view_pager.setCurrentItem(0);


//        bannerAdapter = new HomeBannerAdapter(getActivity(), sliderArray, this);
        view_pager_banner = (ViewPager) view.findViewById(R.id.view_pager_banner);
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        view_pager_banner.setAdapter(bannerPagerAdapter);
//        view_pager_banner.setAdapter(bannerAdapter);
        view_pager_banner.setVisibility(View.VISIBLE);
        setUiPageViewController();
        view_pager_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);

        // Campus list Data init
//        mCampusListAdapter = new CampusListAdapter(getActivity(), R.id.txt_line1, productArray, this, this);
//        stgv = (StaggeredGridView) view.findViewById(R.id.stgv);
//        stgv.setItemMargin(25);
////        stgv.setPadding(22, 0, 22, 0);
//        stgv.setAdapter(mCampusListAdapter);
//        stgv.setOnLoadmoreListener(new StaggeredGridView.OnLoadmoreListener() {
//            @Override
//            public void onLoadmore() {
//                if (campusTotalPage != 0 && !campusAsyncExcecuting) {
//                    footerView1.setVisibility(View.VISIBLE);
//                    mHandler.sendEmptyMessage(2);
//                    callCampusListAsync();
//                    campusAsyncExcecuting = true;
//                }
//            }
//        });
//        stgv.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });

        campusItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));
        dealItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 15), true);
        moviesItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(campusItemDecoration);
        rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
        recyclerView.setAdapter(rcAdapter);


        // Deal list Data init
//        mDealAdapter = new DealListAdapter(getActivity(), R.id.txt_line1, dealArray);
//        mDealList = (ListView) view.findViewById(R.id.deal_list_view);
////        mDealList.addFooterView(footerView2);
//        mDealList.setAdapter(mDealAdapter);
//        mDealList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                AppDelegate.LogT("event_list_view onItemClick");
//                Bundle bundle = new Bundle();
//                bundle.putParcelable(Tags.deal, dealArray.get(position));
//                Fragment fragment = new DealDetailsFragment();
//                fragment.setArguments(bundle);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//            }
//        });
//        mDealList.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(dealAdapter);

        // Event list Data init
//        mEventAdapter = new EventListAdapter(getActivity(), R.id.txt_line1, eventArray);
//        event_list_view = (ListView) view.findViewById(R.id.event_list_view);
//        // event_list_view.addFooterView(footerView3);
//        event_list_view.setAdapter(mEventAdapter);
//        event_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                AppDelegate.LogT("event_list_view onItemClick");
//                Bundle bundle = new Bundle();
//                bundle.putParcelable(Tags.EVENT, eventArray.get(position));
//                Fragment fragment = new EventDetailFragment();
//                fragment.setArguments(bundle);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//            }
//        });
//        event_list_view.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return false;
//            }
//        });
        moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(moviesAdapter);


        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (selected_tab == 0) {
                                 if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callCampusListAsync();
                                     campusAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                                 }
                             } else if (selected_tab == 1) {
                                 if (dealTotalPage != 0 && !dealAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callDealsListAsync();
                                     dealAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 1, " + dealTotalPage + ", " + dealAsyncExcecuting);
                                 }
                             } else {
                                 if (moviesTotalPage != 0 && !moviesAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(4);
                                     callEventListAsync();
                                     moviesAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 2, " + moviesTotalPage + ", " + moviesAsyncExcecuting);
                                 }
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        switchPage(selected_tab);
    }


    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.CAMPUS_LIST,
                    mPostArrayList, null);
            if (campusCounter == 1) {
                mHandler.sendEmptyMessage(10);
            }
            AppDelegate.LogT("callCampusListAsync called");
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callDealsListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.DEALS_LIST,
                    mPostArrayList, null);
            if (dealCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callEventListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, moviesCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.MOVIES_LIST,
                    mPostArrayList, null);
            if (moviesCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.orange_radius_square);
        }
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(15, 15);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, this);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callSliderAsync(String value) {
        try {
            if (sliderAsync != null) {
                sliderAsync.cancelAsync(false);
                sliderAsync.cancel(true);
            }
            sliderArray.clear();
            bannerFragment.clear();
            bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
            view_pager_banner.setAdapter(bannerPagerAdapter);
            mHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_type, value);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            sliderAsync = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_LIST,
                    mPostArrayList, this);
            txt_c_shop_now.setVisibility(View.GONE);
            mHandler.sendEmptyMessage(12);
            sliderAsync.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_LIST)) {
            mHandler.sendEmptyMessage(13);
            parseSliderAsync(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            mHandler.sendEmptyMessage(13);
            parseSlidersClick(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CAMPUS_LIST)) {
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            } else if (campusCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 0)
                swipyrefreshlayout.setRefreshing(false);
            swipyrefreshlayout.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
            parseCampusListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DEALS_LIST)) {
            if (dealCounter > 1) {
                dealAsyncExcecuting = false;
            } else if (dealCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 1)
                swipyrefreshlayout.setRefreshing(false);
            parseDealListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENTS_LIST)) {
            if (moviesCounter > 1) {
                moviesAsyncExcecuting = false;
            } else if (moviesCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 2)
                swipyrefreshlayout.setRefreshing(false);
//            parseEventListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.MOVIES_LIST)) {
            if (moviesCounter > 1) {
                moviesAsyncExcecuting = false;
            } else if (moviesCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 2)
                swipyrefreshlayout.setRefreshing(false);
            parseMoviesListResult(result);
        }
    }

    private void parseDealListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                dealTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    DealModel dealModel = new DealModel();
                    dealModel.id = object.getString(Tags.id);
                    dealModel.user_id = object.getString(Tags.user_id);
                    dealModel.deal_area = object.getString(Tags.deal_area);
                    dealModel.deal_catid = object.getString(Tags.deal_catid);
                    dealModel.institute_id = object.getString(Tags.institute_id);
                    dealModel.title = object.getString(Tags.title);
                    dealModel.details = object.getString(Tags.details);
                    dealModel.venue = object.getString(Tags.venue);
                    dealModel.price = object.getString(Tags.price);
                    dealModel.discount = object.getString(Tags.discount);
                    dealModel.discount_price = object.getString(Tags.discount_price);
                    dealModel.coupon_code = object.getString(Tags.coupon_code);
                    dealModel.expiry_date = object.getString(Tags.expiry_date);

                    JSONObject dealObject = object.getJSONObject(Tags.deal_category);
                    dealModel.product_category = dealObject.getString(Tags.title);

                    dealModel.image_1 = object.getString(Tags.image_1);
                    dealModel.image_2 = object.getString(Tags.image_2);
                    dealModel.image_3 = object.getString(Tags.image_3);
                    dealModel.image_4 = object.getString(Tags.image_4);

                    dealModel.image_1_thumb = object.getString(Tags.image_1_thumb);
                    dealModel.image_2_thumb = object.getString(Tags.image_2_thumb);
                    dealModel.image_3_thumb = object.getString(Tags.image_3_thumb);
                    dealModel.image_4_thumb = object.getString(Tags.image_4_thumb);

//                    dealModel.sold_status = object.getString(Tags.sold_status);
//                    dealModel.status = object.getString(Tags.status);
                    dealModel.created = object.getString(Tags.created);
//                    dealModel.modified = object.getString(Tags.modified);

                    dealArray.add(dealModel);
                }
            } else {
                dealTotalPage = 0;
            }
            dealCounter++;

            mHandler.sendEmptyMessage(3);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(3);
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }


    private void parseMoviesListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                moviesTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    MoviesModel eventModel = new MoviesModel();
                    eventModel.id = object.getString(Tags.id);
                    eventModel.user_id = object.getString(Tags.user_id);
                    eventModel.title = object.getString(Tags.title);
                    eventModel.banner_image = object.getString(Tags.banner_image);
                    eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                    eventModel.synopsis = object.getString(Tags.synopsis);
                    eventModel.run_time = object.getString(Tags.run_time);
                    eventModel.starring = object.getString(Tags.starring);
                    eventModel.trailer_link = object.getString(Tags.trailer_link);
                    eventModel.genre = object.getString(Tags.genre);

                    eventModel.cinema_type_with_time = object.getString(Tags.cinema_type_with_time);
                    eventModel.status = object.getString(Tags.status);
                    eventModel.is_view = object.getString(Tags.is_view);
                    eventModel.total_movie_views = object.getString(Tags.total_movie_views);

                    eventModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
                    eventModel.created = object.getString(Tags.created);

                    moviesArray.add(eventModel);
                }
            } else {
                moviesTotalPage = 0;
            }
            moviesCounter++;

            mHandler.sendEmptyMessage(4);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(4);
                }
            }, 2000);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                campusTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    ProductModel productModel = new ProductModel();
                    productModel.id = object.getString(Tags.id);
                    productModel.cat_id = object.getString(Tags.cat_id);
                    productModel.title = object.getString(Tags.title);
                    productModel.description = object.getString(Tags.description);
                    productModel.price = object.getString(Tags.price);
                    productModel.item_condition = object.getString(Tags.item_condition);

                    productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                    productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                    productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                    productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                    productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                    productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                    productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                    productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                    float floatValue = Float.parseFloat(object.getString(Tags.rating));
                    AppDelegate.LogT("floatValue = " + floatValue);
                    productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
                    AppDelegate.LogT("productModel.rating = " + productModel.rating);


                    productModel.sold_status = object.getString(Tags.sold_status);
                    productModel.status = object.getString(Tags.status);
                    productModel.created = object.getString(Tags.created);
                    productModel.modified = object.getString(Tags.modified);
                    productModel.total_product_views = object.getString(Tags.total_product_views);
                    productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

//                    productModel.pc_id = productModel.cat_id;
//                    productModel.pc_title =

                    if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category) + "")) {
                        JSONObject productObject = object.getJSONObject(Tags.product_category);
                        productModel.pc_id = productObject.getString(Tags.id);
                        productModel.pc_title = productObject.getString(Tags.cat_name);
                        productModel.pc_status = productObject.getString(Tags.status);
                    }

                    productModel.latitude = object.getString(Tags.latitude);
                    productModel.longitude = object.getString(Tags.longitude);

                    JSONObject userObject = object.getJSONObject(Tags.user);
                    productModel.user_id = userObject.getString(Tags.id);
                    productModel.user_first_name = userObject.getString(Tags.first_name);
                    productModel.user_last_name = userObject.getString(Tags.last_name);
                    productModel.user_email = userObject.getString(Tags.email);
                    productModel.user_role = userObject.getString(Tags.role);
                    productModel.user_image = userObject.getString(Tags.image);
                    productModel.user_social_id = userObject.getString(Tags.social_id);
                    productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                    productModel.user_following_count = userObject.getInt(Tags.following_count);
                    productModel.user_followers_count = userObject.getInt(Tags.followers_count);
                    productModel.user_total_product = userObject.getInt(Tags.total_product);
                    productModel.user_follow_status = userObject.getInt(Tags.follow_status);

                    JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                    if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                        productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                        productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                        JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                        if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                            productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                        }
                        if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        } else {
                            productModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                    } else {
                        productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                        productModel.user_department_name = studentObject.getString(Tags.department_name);
                    }

                    productArray.add(productModel);
                }
            } else {
                campusTotalPage = 0;
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(2);
                }
            }, 2000);
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            } else {
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSliderAsync(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            sliderArray.clear();

            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    SliderModel sliderModel = new SliderModel();
                    sliderModel.id = object.getString(Tags.id);
                    sliderModel.banner_image = object.getString(Tags.img);
//                    sliderModel.banner_thumb_image = object.getString(Tags.thumb);
                    sliderModel.banner_thumb_image = sliderModel.banner_image;
                    sliderModel.expiry_date = object.getString(Tags.expiry_date);
                    sliderModel.slider_category = object.getString(Tags.slider_category);
                    sliderModel.status = object.getString(Tags.status);
                    sliderModel.created = object.getString(Tags.created);
                    sliderModel.title = object.getString(Tags.title);
                    sliderModel.descriptions = object.getString(Tags.description);
                    sliderModel.url = object.getString(Tags.url);
                    sliderModel.type_of_slider = selected_tab;

                    if (object.has(Tags.click_status) && object.optJSONObject(Tags.click_status) != null) {
                        sliderModel.single_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.click_status));
                        sliderModel.thump_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.thumb_status));
                    } else {
                        sliderModel.single_clicked = 0;
                        sliderModel.thump_clicked = 0;
                    }
                    sliderArray.add(sliderModel);
                }
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).toggleSlider();
                    }
                }, 300);
                break;

            case R.id.img_c_right:
                break;

            case R.id.ll_c_campus_list:
                selected_tab = 0;
//                view_pager.setCurrentItem(0);
                switchPage(0);
                break;

            case R.id.ll_c_top_deals:
                selected_tab = 1;
//                view_pager.setCurrentItem(1);
                switchPage(1);
                break;

            case R.id.ll_c_events:
                selected_tab = 2;
                switchPage(2);
//                view_pager.setCurrentItem(2);
                break;

            case R.id.txt_c_shop_now:
                break;

            case R.id.view_background_banner:
                if (sliderArray.size() > 0) {
                    this.item_position = view_pager_banner.getCurrentItem();
                    if (sliderArray.get(item_position).single_clicked == 0) {
                        callClicksAsync(sliderArray.get(item_position), 0);
                    }
                    showBannerDialog(sliderArray.get(item_position));
                }
                break;
        }
    }

    private void switchPage(int i) {
        swipyrefreshlayout.setRefreshing(false);

        ll_c_campus_list.setSelected(false);
        ll_c_top_deals.setSelected(false);
        ll_c_events.setSelected(false);

        recyclerView.removeItemDecoration(campusItemDecoration);
        recyclerView.removeItemDecoration(dealItemDecoration);
        recyclerView.removeItemDecoration(moviesItemDecoration);

//        stgv.setVisibility(View.GONE);
//        mDealList.setVisibility(View.GONE);
//        event_list_view.setVisibility(View.GONE);
        switch (i) {
            case 0:
                AppDelegate.LogT("switchPage => 0");
                ll_c_campus_list.setSelected(true);
                callSliderAsync("5");
//                stgv.setVisibility(View.VISIBLE);
                rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(campusItemDecoration);
                recyclerView.setAdapter(rcAdapter);
                if (productArray.size() == 0) {
                    callCampusListAsync();
                } else {

                    mHandler.sendEmptyMessage(2);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mHandler.sendEmptyMessage(2);
                        }
                    }, 2000);
                }
//                rl_c_main.setBackgroundColor(getResources().getColor(R.color.grey_font));
                break;

            case 1:
//                mDealList.setVisibility(View.VISIBLE);
                ll_c_top_deals.setSelected(true);
                callSliderAsync("3");
                dealAdapter = new DealRecyclerViewAdapter(getActivity(), dealArray, this);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(dealItemDecoration);
                recyclerView.setAdapter(dealAdapter);
                if (dealArray.size() == 0) {
                    callDealsListAsync();
                } else {
                    mHandler.sendEmptyMessage(3);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mHandler.sendEmptyMessage(3);
                        }
                    }, 2000);
                }
//                rl_c_main.setBackgroundColor(Color.WHITE);
                break;

            case 2:
//                event_list_view.setVisibility(View.VISIBLE);
                ll_c_events.setSelected(true);
                moviesAdapter = new MoviesRecyclerViewAdapter(getActivity(), moviesArray, this);
                recyclerView.setBackgroundColor(getResources().getColor(R.color.grey_font));
                recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
                recyclerView.setLayoutManager(gaggeredGridLayoutManager);
                recyclerView.addItemDecoration(moviesItemDecoration);
                recyclerView.setAdapter(moviesAdapter);
                callSliderAsync("2");
                if (moviesArray.size() == 0) {
                    callEventListAsync();
                } else {
                    mHandler.sendEmptyMessage(4);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mHandler.sendEmptyMessage(4);
                        }
                    }, 2000);
                }
//                rl_c_main.setBackgroundColor(Color.WHITE);
                break;
        }
    }

    private void showBannerDialog(final SliderModel sliderModel) {
        Dialog bannerDialog = new Dialog(getActivity(), android.R.style.Theme_Light);
        bannerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bannerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        bannerDialog.setContentView(R.layout.dialog_deal_coupen);
        ImageView img_c_coupon = (ImageView) bannerDialog.findViewById(R.id.img_c_coupon);
        Picasso.with(getActivity()).load(sliderModel.banner_image).into(img_c_coupon);
        TextView txt_c_coupon = (TextView) bannerDialog.findViewById(R.id.txt_c_coupon);
        TextView txt_coupon_time = (TextView) bannerDialog.findViewById(R.id.txt_coupon_time);
        TextView txt_c_save = (TextView) bannerDialog.findViewById(R.id.txt_c_save);
        txt_c_save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliderModel.single_clicked == 0) {
                    callClicksAsync(sliderModel, 1);
                }
            }
        });
        bannerDialog.show();
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.product)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productArray.get(position));
            Fragment fragment = new ProductDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//        } else {
//            AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
//            this.item_position = position;
//            this.click_type = 0;
//            if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
//                if (AppDelegate.haveNetworkConnection(getActivity())) {
//                    if (sliderArray.get(position).single_clicked == 0) {
//                        callClicksAsync(sliderArray.get(position), 0);
//                    }
//
//                    Intent intent = new Intent(getActivity(), BannerDetailActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Tags.image, sliderArray.get(position).banner_image);
//                    bundle.putParcelable(Tags.slider_id, sliderArray.get(position));
//                    intent.putExtras(bundle);
//                    startActivity(intent);
//
////                showBannerDialog(sliderArray.get(position));
//                }
//            }
        } else if (name.equalsIgnoreCase(Tags.deal)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.deal, dealArray.get(position));
            Fragment fragment = new DealDetailsFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.movies)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.movies, moviesArray.get(position));
            Fragment fragment = new MoviesDetailsFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int height) {
        if (name.equalsIgnoreCase(Tags.HEIGHT)) {
            productArray.get(position).height = height;
//            adjustHeightOfStaggerdView();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    adjustHeightOfStaggerdView();
//                }
//            }, 1000);
        }
    }

//    private void adjustHeightOfStaggerdView() {
//        try {
//            int heightValue1 = 0, heightValue2 = 0;
//            ViewGroup.LayoutParams params = stgv.getLayoutParams();
//
//            for (int i = 0; i < productArray.size(); i++) {
//                AppDelegate.LogT("H productArray.get(i).height = " + productArray.get(i).height);
//                if (i % 2 == 0)
//                    heightValue1 += productArray.get(i).height + AppDelegate.dpToPix(getActivity(), 85);
//                else
//                    heightValue2 += productArray.get(i).height + AppDelegate.dpToPix(getActivity(), 85);
//            }
//            AppDelegate.LogT("H Campus list => heightValue1 = " + heightValue1 + ", heightValue2 = " + heightValue2);
//            if (heightValue1 > heightValue2) {
//                params.height = heightValue1;
//            } else {
//                params.height = heightValue2;
//            }
//
//            stgv.setLayoutParams(params);
//            stgv.requestLayout();
////            stgv.setSelectionToTop();
//            stgv.setVerticalScrollbarPosition(0);
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//    }

}
