package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.stux.AppDelegate;
import com.stux.R;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/03/2016.
 */
public class LostFoundDetailFragment extends Fragment {

    private ImageView img_product;
    private TextView txt_c_condition, txt_c_image_count, txt_c_address, txt_c_description;

    private FrameLayout map_frame;
    private ScrollView scrollView;

    private GoogleMap map_business;
    private SupportMapFragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lost_found_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        img_product = (ImageView) view.findViewById(R.id.img_product);

        txt_c_condition = (TextView) view.findViewById(R.id.txt_c_condition);
        txt_c_image_count = (TextView) view.findViewById(R.id.txt_c_image_count);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);


        map_frame = (FrameLayout) view.findViewById(R.id.map_frame);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        map_frame.getLayoutParams().height = AppDelegate.getDeviceWith(getActivity()) / 2;
        map_frame.requestLayout();

        map_frame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return map_frame.onTouchEvent(event);
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.map_frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
    }

    private void showMap() {
        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }

        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        AppDelegate.setMyLocationBottomRightButton(getActivity(), fragment);
        map_business.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

            }
        });

    }
}
