package com.stux.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.activities.LargeImageActivity;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class ProductDetailFragment extends Fragment implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse {

    public ArrayList<String> arrayString = new ArrayList<>();
    // private WorkaroundMapFragment fragment;
    // private SupportMapFragment fragment;
    private MapView mapview;
    private TextView txt_c_description, txt_c_image, txt_c_viewer, txt_c_condition, txt_c_detail, txt_c_address, txt_c_price, txt_c_name, txt_c_price_my_product, txt_c_item_sold, txt_c_chat, txt_c_make_offer;
    private RelativeLayout rl_profile_img;
    private CircleImageView cimg_user;
    private ImageView img_product, img_loading;
    //    private FrameLayout map_Frame;
    private ScrollView scrollView;
    private GoogleMap map_business;
    private ProductModel productModel;
    private String str_from = "";
    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;

    private Handler mHandler;

    private Prefs prefs;
    private UserDataModel dataModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppDelegate.LogT("ProductDetailFragment onCreate called");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AppDelegate.LogT("ProductDetailFragment onCreateView called");
        MapsInitializer.initialize(getActivity());
        return inflater.inflate(R.layout.product_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppDelegate.LogT("ProductDetailFragment onViewCreated called");
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        productModel = getArguments().getParcelable(Tags.product);
        if (getArguments().getString(Tags.from) != null)
            str_from = getArguments().getString(Tags.from);
        try {
            mapview = (MapView) view.findViewById(R.id.mapview);
            mapview.onCreate(savedInstanceState);
            map_business = mapview.getMap();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        initView(view);
        setHandler();
        setValues();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void executeItemViewApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProductDetailFragment.this, ServerRequestConstants.ITEM_VIEW,
                    mPostArrayList, ProductDetailFragment.this);
//            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void executeItemSoldApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.sold_status, "1");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProductDetailFragment.this, ServerRequestConstants.ITEM_SOLD,
                    mPostArrayList, ProductDetailFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setValues() {
        if (productModel != null) {

            txt_c_description.setText(Html.fromHtml("<b>Description : </b>" + productModel.description + ""));
            txt_c_detail.setText(productModel.title);
            txt_c_viewer.setText(productModel.total_product_views + "");

            if (productModel.item_condition.equalsIgnoreCase("1")) {
                txt_c_condition.setText("CONDITION : NEW");
            } else if (productModel.item_condition.equalsIgnoreCase("2")) {
                txt_c_condition.setText("CONDITION : ALMOST NEW");
            } else if (productModel.item_condition.equalsIgnoreCase("3")) {
                txt_c_condition.setText("CONDITION : USED");
            }

            if (arrayString.size() == 0)
                if (AppDelegate.isValidString(productModel.image_4_thumb)) {
                    txt_c_image.setText("4");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                    arrayString.add(productModel.image_3_thumb);
                    arrayString.add(productModel.image_4_thumb);
                } else if (AppDelegate.isValidString(productModel.image_3_thumb)) {
                    txt_c_image.setText("3");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                    arrayString.add(productModel.image_3_thumb);
                } else if (AppDelegate.isValidString(productModel.image_2_thumb)) {
                    txt_c_image.setText("2");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                } else if (AppDelegate.isValidString(productModel.image_1_thumb)) {
                    txt_c_image.setText("1");
                    arrayString.add(productModel.image_1_thumb);
                } else {
                    txt_c_image.setText("0");
//                arrayString.add("");
                }

            mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
            view_pager.setAdapter(mPagerAdapter);
            view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });


            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            Picasso.with(getActivity()).load(productModel.user_image).into(cimg_user, new Callback() {
                @Override
                public void onSuccess() {
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    img_loading.setVisibility(View.GONE);
                }
            });
            txt_c_name.setText(productModel.user_first_name);
            txt_c_address.setText(productModel.user_institute_name + " - " + productModel.user_department_name);
            txt_c_price.setText("N" + productModel.price);
            txt_c_price_my_product.setText("N" + productModel.price);

            if (productModel.user_id.equalsIgnoreCase(dataModel.userId)) {
                rl_profile_img.setVisibility(View.GONE);
                txt_c_price_my_product.setVisibility(View.VISIBLE);
                mapview.setVisibility(View.GONE);
                txt_c_item_sold.setVisibility(View.VISIBLE);
                txt_c_chat.setVisibility(View.GONE);
                txt_c_make_offer.setVisibility(View.GONE);
            } else {
                rl_profile_img.setVisibility(View.VISIBLE);
                txt_c_price_my_product.setVisibility(View.GONE);
                mapview.setVisibility(View.VISIBLE);
                txt_c_item_sold.setVisibility(View.GONE);
                txt_c_chat.setVisibility(View.VISIBLE);
                txt_c_make_offer.setVisibility(View.VISIBLE);
            }
            if (productModel.logged_user_view_status.equalsIgnoreCase("0")) {
                executeItemViewApi();
            }
        }
    }

    private void showDialogDelete() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_delete_item);
        dialog.findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        dialog.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                executeItemSoldApi();
            }
        });
        dialog.show();
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("List Details");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        txt_c_image = (TextView) view.findViewById(R.id.txt_c_image);
        txt_c_viewer = (TextView) view.findViewById(R.id.txt_c_viewer);
        txt_c_condition = (TextView) view.findViewById(R.id.txt_c_condition);
        txt_c_detail = (TextView) view.findViewById(R.id.txt_c_detail);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_price = (TextView) view.findViewById(R.id.txt_c_price);
        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_price_my_product = (TextView) view.findViewById(R.id.txt_c_price_my_product);
        txt_c_item_sold = (TextView) view.findViewById(R.id.txt_c_item_sold);
        txt_c_chat = (TextView) view.findViewById(R.id.txt_c_chat);
        txt_c_make_offer = (TextView) view.findViewById(R.id.txt_c_make_offer);

        rl_profile_img = (RelativeLayout) view.findViewById(R.id.rl_profile_img);

        img_product = (ImageView) view.findViewById(R.id.img_product);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);

//        map_Frame = (FrameLayout) view.findViewById(R.id.map_Frame);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        mapview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return true;
            }
        });
//        map_business = ((WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map)).getMap();
//        ((WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
//            @Override
//            public void onTouch() {
//                scrollView.requestDisallowInterceptTouchEvent(true);
//            }
//        });

//        map_frame.getLayoutParams().height = AppDelegate.getDeviceWith(getActivity()) / 2;
//        map_frame.requestLayout();
//
//        map_Frame.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                scrollView.requestDisallowInterceptTouchEvent(true);
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_MOVE:
//                        AppDelegate.LogT("onTouch => ACTION_MOVE");
//                        scrollView.requestDisallowInterceptTouchEvent(true);
//                        return true;
////                        break;
//                    case MotionEvent.ACTION_UP:
//                        AppDelegate.LogT("onTouch => ACTION_UP");
//                        scrollView.requestDisallowInterceptTouchEvent(false);
//                        break;
//                    case MotionEvent.ACTION_CANCEL:
//                        AppDelegate.LogT("onTouch => ACTION_CANCEL");
//                        scrollView.requestDisallowInterceptTouchEvent(false);
//                        break;
//                }
//                return map_Frame.dispatchTouchEvent(event);
//            }
//        });

        view_pager = (ViewPager) view.findViewById(R.id.view_pager);

        view.findViewById(R.id.txt_c_chat).setOnClickListener(this);
        view.findViewById(R.id.txt_c_make_offer).setOnClickListener(this);
        view.findViewById(R.id.txt_c_item_sold).setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        fragment = WorkaroundMapFragment.newInstance();
//
//        getChildFragmentManager().beginTransaction()
//                .replace(R.id.map_Frame, fragment, "MAP1").addToBackStack(null)
//                .commit();
//        new Handler().postDelayed(new Runnable() {
//                                      @Override
//                                      public void run() {
        showMap();
//                                      }
//                                  }
//                , 1500);
    }

    private void showMap() {
//        map_business = fragment.getMap();
        if (map_business == null) {
            AppDelegate.LogE("map_business null at showMap");
            return;
        }

        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//      AppDelegate.setMyLocationBottomRightButton(getActivity(), wor);

        if (productModel != null && AppDelegate.isValidString(productModel.latitude) && AppDelegate.isValidString(productModel.longitude)) {
            AppDelegate.LogT("location showing => " + productModel.latitude + "," + productModel.longitude);
            map_business.addMarker(AppDelegate.getMarkerOptions(getActivity(), new LatLng(Double.parseDouble(productModel.latitude), Double.parseDouble(productModel.longitude))));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(productModel.latitude), Double.parseDouble(productModel.longitude)))
                    .zoom(14).build();
            //Zoom in and animate the camera.
            map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;
            case R.id.txt_c_chat:
                break;
            case R.id.txt_c_item_sold:
                showAlertDialog();
                break;

            case R.id.txt_c_make_offer:

                break;

            case R.id.cimg_user:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    UserDataModel sellersDataModel = new UserDataModel();
                    sellersDataModel.userId = productModel.user_id;
                    sellersDataModel.first_name = productModel.user_first_name;
                    sellersDataModel.last_name = productModel.user_last_name;
                    sellersDataModel.image = productModel.user_image;
                    sellersDataModel.is_view = productModel.user_is_view;
                    sellersDataModel.role = productModel.user_role;
                    sellersDataModel.social_id = productModel.user_social_id;
                    sellersDataModel.status = productModel.user_status;
                    sellersDataModel.login_type = productModel.user_login_type;
                    sellersDataModel.dob = productModel.user_dob;

                    sellersDataModel.institution_state_id = productModel.user_institution_state_id;
                    sellersDataModel.institution_id = productModel.user_institution_id;
                    sellersDataModel.institute_name = productModel.user_institute_name;
                    sellersDataModel.department_name = productModel.user_department_name;

                    sellersDataModel.following_count = productModel.user_following_count;
                    sellersDataModel.followers_count = productModel.user_followers_count;
                    sellersDataModel.total_product = productModel.user_total_product;
                    sellersDataModel.follow_status = productModel.user_follow_status;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Tags.user, sellersDataModel);
                    Fragment fragment = new SellersProfileFragment();
                    if (sellersDataModel.userId.equalsIgnoreCase(dataModel.userId)) {
                        fragment = new MyProfileFragment();
                    } else {
                        fragment = new SellersProfileFragment();
                    }
                    fragment.setArguments(bundle);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//                Bundle bundle1 = new Bundle();
//                bundle1.putParcelable(Tags.user, sellersDataModel);
//                Fragment fragment1 = new ReviewsFragment();
//                fragment1.setArguments(bundle1);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment1);
                } else {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                }
                break;
        }
    }

    private void showAlertDialog() {
        try {
            AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(false);
            mAlert.setTitle("STUX");
            mAlert.setMessage("Are you sure you want to mark this product as a sold out.");
            mAlert.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            executeItemSoldApi();
                            dialog.dismiss();
                        }
                    });

            mAlert.setPositiveButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private int item_position = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                Bundle bundle = new Bundle();
                if (position == 0) {
                    bundle.putString(Tags.image, productModel.image_1);
                } else if (position == 1) {
                    bundle.putString(Tags.image, productModel.image_2);
                } else if (position == 2) {
                    bundle.putString(Tags.image, productModel.image_3);
                } else if (position == 3) {
                    bundle.putString(Tags.image, productModel.image_4);
                }
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.ITEM_VIEW)) {
//            mHandler.sendEmptyMessage(11);
            parseItemViewResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ITEM_SOLD)) {
            mHandler.sendEmptyMessage(11);
            parseItemSoldResult(result);
        }
    }

    private void parseItemSoldResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                getFragmentManager().popBackStack();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void parseItemViewResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                productModel.total_product_views += 1;
                setValues();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);

        }
    }
}
