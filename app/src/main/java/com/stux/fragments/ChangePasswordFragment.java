package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/04/2016.
 */
public class ChangePasswordFragment extends Fragment implements OnReciveServerResponse, View.OnClickListener {

    private EditText et_c_old_password, et_c_new_password, et_c_confirm_password;
    private Prefs prefs;
    private UserDataModel dataModel;

    private Handler mHandler;
    private android.widget.ImageView img_old_password, img_new_password, img_confirm_new_password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.change_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Change Password");
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        ((ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        ((ImageView) view.findViewById(R.id.img_c_left)).setVisibility(View.VISIBLE);

        et_c_old_password = (EditText) view.findViewById(R.id.et_c_old_password);
        et_c_old_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_bold)));
        et_c_new_password = (EditText) view.findViewById(R.id.et_c_new_password);
        et_c_new_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_bold)));
        et_c_confirm_password = (EditText) view.findViewById(R.id.et_c_confirm_password);
        et_c_confirm_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_bold)));

        img_old_password = (android.widget.ImageView) view.findViewById(R.id.img_old_password);
        img_old_password.setOnClickListener(this);
        img_new_password = (android.widget.ImageView) view.findViewById(R.id.img_new_password);
        img_new_password.setOnClickListener(this);
        img_confirm_new_password = (android.widget.ImageView) view.findViewById(R.id.img_confirm_new_password);
        img_confirm_new_password.setOnClickListener(this);

        view.findViewById(R.id.txt_c_continue).setOnClickListener(this);
    }

    private void callChangePasswordAsync() {
        if (et_c_old_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter your Old Password.");
        } else if (et_c_new_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter your New Password.");
        } else if (et_c_confirm_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter your Confirm New Password.");
        } else if (!et_c_new_password.getText().toString().equals(et_c_confirm_password.getText().toString())) {
            AppDelegate.showAlert(getActivity(), "New Password and Confirm New Passwrord must be same, Please enter valid New and Confirm Password.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.old_password, et_c_old_password.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.new_password, et_c_new_password.getText().toString());

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ChangePasswordFragment.this, ServerRequestConstants.CHANGE_PASSWORD,
                    mPostArrayList, ChangePasswordFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CHANGE_PASSWORD)) {
            parseFavCategoryResult(result);
        }
    }

    private void parseFavCategoryResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), "Password changed successfully!");
                getFragmentManager().popBackStack();
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);

            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_old_password:
                img_old_password.setSelected(!img_old_password.isSelected());
                if (img_old_password.isSelected()) {
                    et_c_old_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_old_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_old_password.setSelection(et_c_old_password.length());
                break;

            case R.id.img_new_password:
                img_new_password.setSelected(!img_new_password.isSelected());
                if (img_new_password.isSelected()) {
                    et_c_new_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_new_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_new_password.setSelection(et_c_new_password.length());
                break;

            case R.id.img_confirm_new_password:
                img_confirm_new_password.setSelected(!img_confirm_new_password.isSelected());
                if (img_confirm_new_password.isSelected()) {
                    et_c_confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    et_c_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                et_c_confirm_password.setSelection(et_c_confirm_password.length());
                break;

            case R.id.txt_c_continue:
                callChangePasswordAsync();
                break;
        }
    }
}
