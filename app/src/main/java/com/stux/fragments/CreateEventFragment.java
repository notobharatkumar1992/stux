package com.stux.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.maps.model.LatLng;
import com.stux.Adapters.LocationMapAdapter;
import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.AppDelegate;
import com.stux.Async.GetLatLongFromPlaceIdAsync;
import com.stux.Async.PlacesService;
import com.stux.Async.PostAsync;
import com.stux.Models.EventCategoryModel;
import com.stux.Models.EventModel;
import com.stux.Models.Place;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DateUtils;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.GetLocationResult;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by Bharat on 06/02/2016.
 */
public class CreateEventFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, GetLocationResult, OnListItemClickListener {

    private Prefs prefs;
    private UserDataModel dataModel;

    public static List<Place> mapPlacesArray = new ArrayList<>();

    private Spinner spn_type_event, spn_location, spn_theme, spn_gate_fee, spn_ticket;
    private SpinnerArrayStringAdapter adapterEventType, adapterLocationType, adapterTheme, adapterGateFee, adapterTicket;
    public ArrayList<EventCategoryModel> eventTypeArray = new ArrayList<>();
    public ArrayList<String> arrayStringTypeEvent = new ArrayList<>();
    public ArrayList<String> arrayStringLoaction = new ArrayList<>();
    public ArrayList<String> arrayStringTheme = new ArrayList<>();
    public ArrayList<String> arrayStringGateFee = new ArrayList<>();
    public ArrayList<String> arrayStringTicket = new ArrayList<>();

    private EditText et_event_name, et_location, et_venue, et_theme, et_contact_number, et_email, et_fees, et_description;
    private LinearLayout ll_c_upload_pic, ll_c_img_layout;
    private RelativeLayout rl_c_pic_0, rl_c_pic_1, rl_c_pic_2, rl_c_pic_3;
    private ImageView img_c_pic_0, img_c_pic_1, img_c_pic_2, img_c_pic_3, img_c_close_0, img_c_close_1, img_c_close_2, img_c_close_3;
    private TextView txt_c_from_date, txt_c_to_date, txt_c_from_time, txt_c_to_time, no_location_text;

    private ListView search_list;
    private ProgressBar pb_location_progressbar;
    public Thread mThreadOnSearch;
    public LocationMapAdapter adapter;

    private int selected_event_type = 0, selected_location = 0, selected_fees = 0, selected_ticket = 0;

    private ArrayList<File> arrayImageFile = new ArrayList<>();

    private ExpandableRelativeLayout exp_layout;
    private boolean canSearch = true;
    private String placeKeyword = "";
    private Place placeDetail;
    private int selected_item_position = 0;
    private LatLng latLng;

    private EventModel eventModel;
    private Handler mHandler;

    private Bitmap OriginalPhoto;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_event, container, false);
    }

    public void expandableView(final ExpandableRelativeLayout exp_layout, final int value) {
        exp_layout.post(new Runnable() {
            @Override
            public void run() {
                if (value == MainActivity.COLLAPSE) {
                    exp_layout.collapse();
                } else {
                    exp_layout.expand();
                }
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        eventModel = new EventModel();
        initView(view);
        setHandler();
        executeEventApi();
    }

    private void executeEventApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    CreateEventFragment.this, ServerRequestConstants.GET_EVENT_CATEGORY,
                    mPostArrayList, CreateEventFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;

                    case 1:
                        adapterEventType.notifyDataSetChanged();
                        spn_type_event.invalidate();
                        break;

                    case 2:
                        if (mapPlacesArray != null && mapPlacesArray.size() > 0) {
                            AppDelegate.LogT("handler 1");
                            adapter.notifyDataSetChanged();
                            search_list.invalidate();
                            search_list.setVisibility(View.VISIBLE);
                        } else {
                            search_list.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult) {
                                no_location_text
                                        .setText(getResources()
                                                .getString(
                                                        R.string.no_location_available_for_this_address));
                            } else {
                                no_location_text.setText(getResources().getString(
                                        R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text.setVisibility(View.VISIBLE);
                            pb_location_progressbar.setVisibility(View.GONE);
                        }
                        setListViewHeight(getActivity(), exp_layout, adapter);
                        break;
                    case 6:
                        no_location_text.setText(getResources().getString(
                                R.string.no_internet_connection));
                        pb_location_progressbar.setVisibility(View.INVISIBLE);
                        no_location_text.setVisibility(View.VISIBLE);
                        search_list.setVisibility(View.GONE);
                        expandableView(exp_layout, MainActivity.EXPAND);
                        setListViewHeight(getActivity(), exp_layout, adapter);
                        break;
                    case 7:
                        pb_location_progressbar.setVisibility(View.VISIBLE);
                        no_location_text.setVisibility(View.GONE);
                        break;
                    case 8:
                        pb_location_progressbar.setVisibility(View.INVISIBLE);
                        break;
                }
            }
        };
    }

    public void setListViewHeight(Context mContext, ExpandableRelativeLayout listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = AppDelegate.dpToPix(mContext, 180);
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            if (itemCount == 0) {
                totalHeight = AppDelegate.dpToPix(mContext, 180);
            } else {
                for (int i = 0; i < itemCount; i++) {
                    View listItem = gridAdapter.getView(i, null, listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Create Event");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) view.findViewById(R.id.txt_c_right)).setText("Create");
        view.findViewById(R.id.txt_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.txt_c_right).setOnClickListener(this);

        txt_c_from_date = (TextView) view.findViewById(R.id.txt_c_from_date);
        txt_c_to_date = (TextView) view.findViewById(R.id.txt_c_to_date);
        txt_c_from_time = (TextView) view.findViewById(R.id.txt_c_from_time);
        txt_c_to_time = (TextView) view.findViewById(R.id.txt_c_to_time);

        et_event_name = (EditText) view.findViewById(R.id.et_event_name);
        et_event_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_location = (EditText) view.findViewById(R.id.et_location);
        et_location.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_venue = (EditText) view.findViewById(R.id.et_venue);
        et_venue.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_venue.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                placeKeyword = et_venue.getText().toString();
                if (canSearch) {
                    if (et_venue.length() != 0) {
                        expandableView(exp_layout, MainActivity.EXPAND);
                        stopStartThread(mThreadOnSearch);
                        no_location_text.setVisibility(View.GONE);
                    } else {
                        expandableView(exp_layout, MainActivity.COLLAPSE);
                    }
                } else {
                    expandableView(exp_layout, MainActivity.COLLAPSE);
                }
            }

        });

        et_theme = (EditText) view.findViewById(R.id.et_theme);
        et_theme.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_contact_number = (EditText) view.findViewById(R.id.et_contact_number);
        et_contact_number.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_fees = (EditText) view.findViewById(R.id.et_fees);
        et_fees.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_description = (EditText) view.findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));

        arrayStringTypeEvent.add("Type of Event");
        eventTypeArray.add(new EventCategoryModel("Type of Event"));

        arrayStringLoaction.add("Location Type");
        arrayStringLoaction.add("On Campus");
        arrayStringLoaction.add("Off Campus");

        arrayStringTheme.add("Theme");
        arrayStringTheme.add("Theme");
        arrayStringTheme.add("Theme");

        arrayStringGateFee.add("Gate Fees Type");
        arrayStringGateFee.add("Free");
        arrayStringGateFee.add("Paid");

        arrayStringTicket.add("Ticket Type");
        arrayStringTicket.add("All Ticket Sold Out");
        arrayStringTicket.add("Ticket Available");

        spn_type_event = (Spinner) view.findViewById(R.id.spn_type_event);
        adapterEventType = new SpinnerArrayStringAdapter(getActivity(), arrayStringTypeEvent);
        spn_type_event.setAdapter(adapterEventType);
        spn_type_event.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_event_type = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn_location = (Spinner) view.findViewById(R.id.spn_location);
        adapterLocationType = new SpinnerArrayStringAdapter(getActivity(), arrayStringLoaction);
        spn_location.setAdapter(adapterLocationType);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_location = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn_theme = (Spinner) view.findViewById(R.id.spn_theme);
        adapterTheme = new SpinnerArrayStringAdapter(getActivity(), arrayStringTheme);
        spn_theme.setAdapter(adapterTheme);

        spn_gate_fee = (Spinner) view.findViewById(R.id.spn_gate_fee);
        adapterGateFee = new SpinnerArrayStringAdapter(getActivity(), arrayStringGateFee);
        spn_gate_fee.setAdapter(adapterGateFee);
        spn_gate_fee.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_fees = position;
                switch (position) {
                    case 0:
                        et_fees.setVisibility(View.GONE);
                        break;
                    case 1:
                        et_fees.setVisibility(View.GONE);
                        break;
                    case 2:
                        et_fees.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spn_ticket = (Spinner) view.findViewById(R.id.spn_ticket);
        adapterTicket = new SpinnerArrayStringAdapter(getActivity(), arrayStringTicket);
        spn_ticket.setAdapter(adapterTicket);
        spn_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_ticket = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ll_c_img_layout = (LinearLayout) view.findViewById(R.id.ll_c_img_layout);
        ll_c_img_layout.setVisibility(View.GONE);
        rl_c_pic_0 = (RelativeLayout) view.findViewById(R.id.rl_c_pic_0);
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1 = (RelativeLayout) view.findViewById(R.id.rl_c_pic_1);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2 = (RelativeLayout) view.findViewById(R.id.rl_c_pic_2);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3 = (RelativeLayout) view.findViewById(R.id.rl_c_pic_3);
        rl_c_pic_3.setVisibility(View.INVISIBLE);

        img_c_pic_0 = (ImageView) view.findViewById(R.id.img_c_pic_0);
        img_c_pic_1 = (ImageView) view.findViewById(R.id.img_c_pic_1);
        img_c_pic_2 = (ImageView) view.findViewById(R.id.img_c_pic_2);
        img_c_pic_3 = (ImageView) view.findViewById(R.id.img_c_pic_3);

        img_c_close_0 = (ImageView) view.findViewById(R.id.img_c_close_0);
        img_c_close_0.setOnClickListener(this);
        img_c_close_1 = (ImageView) view.findViewById(R.id.img_c_close_1);
        img_c_close_1.setOnClickListener(this);
        img_c_close_2 = (ImageView) view.findViewById(R.id.img_c_close_2);
        img_c_close_2.setOnClickListener(this);
        img_c_close_3 = (ImageView) view.findViewById(R.id.img_c_close_3);
        img_c_close_3.setOnClickListener(this);

        view.findViewById(R.id.ll_c_upload_pic).setOnClickListener(this);
        view.findViewById(R.id.rl_c_from_date).setOnClickListener(this);
        view.findViewById(R.id.rl_c_to_date).setOnClickListener(this);
        view.findViewById(R.id.rl_c_from_time).setOnClickListener(this);
        view.findViewById(R.id.rl_c_to_time).setOnClickListener(this);
        view.findViewById(R.id.txt_c_submit).setOnClickListener(this);

        exp_layout = (ExpandableRelativeLayout) view.findViewById(R.id.exp_layout);
        expandableView(exp_layout, MainActivity.COLLAPSE);

        no_location_text = (TextView) view.findViewById(R.id.no_location_text);
        search_list = (ListView) view.findViewById(R.id.search_list);
        adapter = new LocationMapAdapter(getActivity(), mapPlacesArray, this);
        search_list.setAdapter(adapter);

        pb_location_progressbar = (ProgressBar) view.findViewById(R.id.pb_location_progressbar);
        pb_location_progressbar.setVisibility(View.GONE);

    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    private void stopStartThread(Thread theThread) {
        if (theThread != null)
            onlyStopThread(theThread);
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            mHandler.sendEmptyMessage(6);
        } else {
            theThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mHandler.sendEmptyMessage(7);
                    mapPlacesArray.clear();
                    mapPlacesArray.addAll(PlacesService.autocompleteForMap(placeKeyword));
                    mHandler.sendEmptyMessage(8);
                    mHandler.sendEmptyMessage(2);
                    canSearch = true;
                }
            });
            theThread.start();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_upload_pic:
                if (arrayImageFile.size() < 1) {
                    showImageSelectorList();
                } else {
                    AppDelegate.showAlert(getActivity(), "You can't upload more than 1 image.");
                }
                break;
            case R.id.rl_c_from_date:
                showDateDialog(0);
                break;
            case R.id.rl_c_to_date:
                showDateDialog(1);
                break;
            case R.id.rl_c_from_time:
                showTimeDialog(0);
                break;
            case R.id.rl_c_to_time:
                showTimeDialog(1);
                break;
            case R.id.txt_c_right:
                callCreateEventAsync();
                break;

            case R.id.img_c_close_0:
                removePicUpdate(0);
                break;
            case R.id.img_c_close_1:
                removePicUpdate(1);
                break;
            case R.id.img_c_close_2:
                removePicUpdate(2);
                break;
            case R.id.img_c_close_3:
                removePicUpdate(3);
                break;
        }
    }

    private void callCreateEventAsync() {
        if (selected_event_type == 0) {
            AppDelegate.showAlert(getActivity(), "Please select event type.");
        } else if (et_event_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter event name.");
        } else if (selected_location == 0) {
            AppDelegate.showAlert(getActivity(), "Please select location type.");
//        } else if (et_location.length() == 0) {
//            AppDelegate.showAlert(getActivity(), "Please enter event location.");
        } else if (et_venue.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter event venue.");
        } else if (et_theme.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter event theme.");
        } else if (et_contact_number.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter contact number.");
        } else if (et_email.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter email address.");
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(getActivity(), getString(R.string.please_fill_email_address_in_format));
        } else if (txt_c_from_date.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select from date of event.");
        } else if (txt_c_to_date.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select to date of event.");
        } else if (txt_c_from_time.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select from time of event.");
        } else if (txt_c_to_time.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select to time of event.");
        } else if (selected_fees == 0) {
            AppDelegate.showAlert(getActivity(), "Please select fees type.");
        } else if (selected_fees == 2 && et_fees.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter event fees.");
        } else if (selected_ticket == 0) {
            AppDelegate.showAlert(getActivity(), "Please select ticket type.");
        } else if (et_description.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter description.");
        } else if (capturedFile == null) {
            AppDelegate.showAlert(getActivity(), "Please select image file.");
        } else if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {

            /**
             *  user_id,location_type(1=oncampus , 2= off campus),event_type,event_name, banner_image,theme,details, venue,location, gate_fees_type(0= free , 1= paid),
             *  gate_fees,tickets(0 = sold out , 1 = available),contact_no,event_start_time, event_end_time, latitude, longitude,status=1
             *
             *  2016-06-13 10:38:00
             ***/

            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_type, eventTypeArray.get(selected_event_type).id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_name, et_event_name.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.location_type, selected_location + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.venue, et_venue.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.theme, et_theme.getText().toString() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.contact_no, et_contact_number.getText().toString() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString() + "");
            String start_time = "";
            try {
                start_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("dd-MM-yyyy hh:mm aa").parse(txt_c_from_date.getText().toString() + " " + txt_c_from_time.getText().toString()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_start_time, start_time + "");
            String end_time = "";
            try {
                end_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new SimpleDateFormat("dd-MM-yyyy hh:mm aa").parse(txt_c_to_date.getText().toString() + " " + txt_c_to_time.getText().toString()));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_end_time, end_time + "");

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gate_fees_type, selected_fees == 1 ? 0 : 1 + "");
            if (selected_fees == 2)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gate_fees, et_fees.getText().toString() + "");

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.details, et_description.getText().toString() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "0");


            if (capturedFile != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.banner_image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
//            if (capturedFile_1 != null) {
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_2, capturedFile_1.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
//            }
//            if (capturedFile_2 != null) {
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_3, capturedFile_2.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
//            }
//            if (capturedFile_3 != null) {
//                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_4, capturedFile_3.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
//            }

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, placeDetail.getLatitude() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, placeDetail.getLongitude() + "");

            PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.CREATE_EVENT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }


    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.hideKeyBoard(getActivity());
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(getActivity())) {
            if (mapPlacesArray.get(position).postal_code != null) {
                placeDetail = mapPlacesArray.get(position);
                canSearch = false;
                if (AppDelegate.isValidString(mapPlacesArray.get(position).name))
                    et_venue.setText(mapPlacesArray.get(position).name);
                else
                    et_venue.setText(mapPlacesArray.get(position).vicinity);
                et_venue.setSelection(et_venue.length());
                canSearch = true;
                selected_item_position = position;
                expandableView(exp_layout, MainActivity.COLLAPSE);
                this.latLng = new LatLng(mapPlacesArray.get(position).latitude, mapPlacesArray.get(position).longitude);
                mHandler.sendEmptyMessage(8);
                mHandler.sendEmptyMessage(11);
            } else {
                placeDetail = mapPlacesArray.get(position);
                canSearch = false;
                if (AppDelegate.isValidString(mapPlacesArray.get(position).name))
                    et_venue.setText(mapPlacesArray.get(position).name);
                else
                    et_venue.setText(mapPlacesArray.get(position).vicinity);
                et_venue.setSelection(et_venue.length());
                selected_item_position = position;
                expandableView(exp_layout, MainActivity.COLLAPSE);
                canSearch = true;
                mHandler.sendEmptyMessage(10);
                new GetLatLongFromPlaceIdAsync(getActivity(),
                        this, Tags.LAT_LNG_COLLECT,
                        placeDetail.getId()).execute();
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_EVENT_CATEGORY)) {
            mHandler.sendEmptyMessage(11);
            parseEventResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CREATE_EVENT)) {
            mHandler.sendEmptyMessage(11);
            parseCreateEventResult(result);
        }
    }

    private void parseCreateEventResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
//                EventModel eventModel = new EventModel();
//                eventModel.id = object.getString(Tags.id);
//                eventModel.location_type = object.getString(Tags.location_type);
//                eventModel.event_type = object.getString(Tags.event_type);
//                eventModel.event_name = object.getString(Tags.event_name);
//                eventModel.banner_image = object.getString(Tags.banner_image);
//
//                eventModel.theme = object.getString(Tags.theme);
//                eventModel.details = object.getString(Tags.details);
//                eventModel.venue = object.getString(Tags.venue);
//                eventModel.location = JSONParser.getString(object, Tags.location);
//                eventModel.tickets = object.getString(Tags.tickets);
//
//                eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
//                eventModel.gate_fees = object.getString(Tags.gate_fees);
//                eventModel.contact_no = object.getString(Tags.contact_no);
//                eventModel.event_start_time = object.getString(Tags.event_start_time);
//
//                eventModel.event_end_time = object.getString(Tags.event_end_time);
//                eventModel.created = object.getString(Tags.created);
////                    eventModel.modified = object.getString(Tags.modified);
//                eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);
//
//                eventModel.latitude = object.getString(Tags.latitude);
//                eventModel.longitude = object.getString(Tags.longitude);
//
//
//                JSONObject userObject = object.getJSONObject(Tags.user);
//                eventModel.user_first_name = userObject.getString(Tags.first_name);
//                eventModel.user_last_name = userObject.getString(Tags.last_name);
//                eventModel.user_image = userObject.getString(Tags.image);
//
//                JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
//                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
//                    eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
//                    eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
//                    if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
//                        eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
//                    }
//                    if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
//                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                    } else {
//                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                    }
//                } else {
//                    eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
//                    eventModel.user_department_name = studentObject.getString(Tags.department_name);
//                }
                getFragmentManager().popBackStack();
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    EventCategoryModel model = new EventCategoryModel();
                    model.id = jsonObject.getString(Tags.id);
                    model.cat_name = jsonObject.getString(Tags.cat_name);
                    model.status = jsonObject.getString(Tags.status);
                    model.created = jsonObject.getString(Tags.created);
                    model.modified = jsonObject.getString(Tags.modified);
                    eventTypeArray.add(model);
                    arrayStringTypeEvent.add(jsonObject.getString(Tags.cat_name));
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void removePicUpdate(int i) {
        switch (i) {
            case 0:
                if (arrayImageFile.size() > 0) {
                    arrayImageFile.remove(0);
                }
                break;
            case 1:
                if (arrayImageFile.size() > 1) {
                    arrayImageFile.remove(1);
                }
                break;
            case 2:
                if (arrayImageFile.size() > 2) {
                    arrayImageFile.remove(2);
                }
                break;
            case 3:
                if (arrayImageFile.size() > 3) {
                    arrayImageFile.remove(3);
                }
                break;
        }
        updateImageView();
    }

    private int mYear, mMonth, mDay, mHour, mMinute;

    private void showDateDialog(final int value) {
        final Calendar c = Calendar.getInstance();
        if (value == 0) {
            if (AppDelegate.isValidString(eventModel.from_date)) {
                c.setTime(getDateObject(eventModel.from_date, DATE_FORMAT));
            }
        } else {
            if (AppDelegate.isValidString(eventModel.to_date)) {
                c.setTime(getDateObject(eventModel.to_date, DATE_FORMAT));
            }
        }
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        if (value == 0) {
                            txt_c_from_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                            eventModel.from_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            eventModel.to_date = "";
                            txt_c_to_date.setText("To");
                        } else {
                            if (AppDelegate.isValidString(eventModel.from_date)) {
                                String selected_to_date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                if (DateUtils.isAfterDay(getDateObject(selected_to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT)) || DateUtils.isSameDay(getDateObject(selected_to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT))) {
                                    txt_c_to_date.setText(selected_to_date);
                                    eventModel.to_date = selected_to_date;
                                } else {
                                    AppDelegate.showAlert(getActivity(), "To date must be greater than from date.");
                                }
                            } else {
                                AppDelegate.showAlert(getActivity(), "Please select from date.");
                            }

                        }
                    }
                }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);
        dpd.show();
    }

    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String TIME_FORMAT_12_HOUR = "hh:mm aa";
    public static final String TIME_FORMAT_24_HOUR = "HH:mm";

    public static Date getDateObject(String date, String date_format) {
        try {
            return new SimpleDateFormat(date_format).parse(date);
        } catch (ParseException e) {
            AppDelegate.LogE(e);
        }
        return Calendar.getInstance().getTime();
    }


    public void showTimeDialog(final int value) {
        final Calendar c = Calendar.getInstance();
        if (value == 0) {
            if (AppDelegate.isValidString(eventModel.from_date)) {
                c.setTime(getDateObject(eventModel.from_date, DATE_FORMAT));
            }
        } else {
            if (AppDelegate.isValidString(eventModel.to_date)) {
                c.setTime(getDateObject(eventModel.to_date, DATE_FORMAT));
            }
        }
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog tpd = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        // Display Selected time in text box
                        // txtTime.setText(hourOfDay + ":" + minute);
                        if (AppDelegate.isValidString(eventModel.to_date)) {
                            try {
                                String selected_time = new SimpleDateFormat(TIME_FORMAT_12_HOUR).format(new SimpleDateFormat(TIME_FORMAT_24_HOUR).parse(hourOfDay + ":" + minute));
                                if (value == 0) {
                                    txt_c_from_time.setText(selected_time);
                                    eventModel.from_time = selected_time;
                                    txt_c_to_time.setText("");
                                    eventModel.to_time = "";
                                } else {

                                    if (AppDelegate.isValidString(eventModel.from_time)) {
                                        Date fromdate = new SimpleDateFormat(TIME_FORMAT_12_HOUR).parse(eventModel.from_time);
                                        Date todate = new SimpleDateFormat(TIME_FORMAT_12_HOUR).parse(selected_time);
                                        if (!DateUtils.isSameDay(getDateObject(eventModel.to_date, DATE_FORMAT), getDateObject(eventModel.from_date, DATE_FORMAT))) {
                                            txt_c_to_time.setText(selected_time);
                                            eventModel.to_time = selected_time;
                                        } else {
                                            long diff_time = todate.getTime() - fromdate.getTime();
                                            if (fromdate.before(todate)) {
                                                if (diff_time >= 1800000) {
                                                    AppDelegate.LogT("in diff " + diff_time);
                                                    txt_c_to_time.setText(selected_time);
                                                    eventModel.to_time = selected_time;
                                                } else {
                                                    AppDelegate.showAlert(getActivity(), "Difference between Start Time and End Time should be minimum of 30 minutes.");
                                                }
                                            } else {
                                                AppDelegate.showAlert(getActivity(), "Please to time must be greater than from time.");
                                            }
                                        }
                                    } else {
                                        AppDelegate.showAlert(getActivity(), "Please select from time.");
                                    }
                                }
                            } catch (ParseException e) {
                                AppDelegate.LogE(e);
                            }
                        } else

                        {
                            AppDelegate.showAlert(getActivity(), "Please select from and to date.");
                        }
                    }
                }

                , mHour, mMinute, false);
        tpd.show();
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }


    @Override
    public void onReciveApiResult(String ApiName, String ActionName, LatLng latLng) {
        if (!isAdded()) {
            AppDelegate
                    .LogE("LocationMapFrag is deattached but still called at onReciveApiResult for ApiName = "
                            + ApiName + ", ActionName = " + ActionName);
        } else if (ApiName.equalsIgnoreCase(Tags.LAT_LNG_COLLECT)
                && ActionName.equalsIgnoreCase(Tags.SUCCESS) && latLng != null) {
            this.latLng = latLng;
            mHandler.sendEmptyMessage(8);
            mHandler.sendEmptyMessage(11);
        }
    }

    @Override
    public void onReciveApiResult(String ApiName, String ActionName, Place placeDetails) {
        if (!isAdded()) {
            AppDelegate
                    .LogE("LocationMapFrag is deattached but still called at onReciveApiResult for ApiName = "
                            + ApiName + ", ActionName = " + ActionName);
        } else if (ApiName.equalsIgnoreCase(Tags.LAT_LNG_COLLECT)
                && ActionName.equalsIgnoreCase(Tags.SUCCESS) && placeDetails != null) {
            mHandler.sendEmptyMessage(11);
            this.latLng = new LatLng(placeDetails.latitude, placeDetails.longitude);
            mHandler.sendEmptyMessage(8);

//            this.placeDetail.name = placeDetails.name;
//            this.placeDetail.vicinity = placeDetails.vicinity;
            this.placeDetail.icon = placeDetails.icon;
            this.placeDetail.city = placeDetails.city;
            this.placeDetail.country = placeDetails.country;
            this.placeDetail.latitude = placeDetails.latitude;
            this.placeDetail.longitude = placeDetails.longitude;
            this.placeDetail.postal_code = placeDetails.postal_code;
            this.placeDetail.formatted_phone_number = placeDetails.formatted_phone_number;
//            canSearch = false;
//            et_venue.setText(placeDetail.name + ", " + placeDetail.vicinity);
//            canSearch = true;
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppDelegate.SELECT_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    imageUri = uri;
                    AppDelegate.LogT("uri=" + uri);
                    if (uri != null) {
                        // User had pick an image.
                        Cursor cursor = getActivity()
                                .getContentResolver()
                                .query(uri,
                                        new String[]{MediaStore.Images.ImageColumns.DATA},
                                        null, null, null);
                        cursor.moveToFirst();

                        // Link to the image
                        final String imageFilePath = cursor.getString(0);
                        cursor.close();
                        int orientation = 0;
                        if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                            try {
                                ExifInterface ei = new ExifInterface(imageFilePath);
                                orientation = ei.getAttributeInt(
                                        TAG_ORIENTATION,
                                        ORIENTATION_NORMAL);
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            capturedFile = new File(imageFilePath);
                            OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);
                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                AppDelegate.LogT("Gallery => " + android.os.Build.VERSION.SDK_INT + " <= " + Build.VERSION_CODES.LOLLIPOP_MR1);
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                    capturedFile = new File(getNewFile());
                                    FileOutputStream fOut = null;
                                    try {
                                        fOut = new FileOutputStream(capturedFile);
                                    } catch (FileNotFoundException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                                    try {
                                        fOut.flush();
                                    } catch (IOException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    try {
                                        fOut.close();
                                    } catch (IOException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    arrayImageFile.add(capturedFile);
                                    updateImageView();
                                }
                            } else {
                                if (imageUri != null)
                                    performCrop(imageUri);
                                else
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                }
                break;
            case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);

                //Get our saved file into a bitmap object:
                try {
                    if (resultCode == Activity.RESULT_OK) {
                        int orientation = 0;
                        Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                        BitmapFactory.Options o = new BitmapFactory.Options();
                        o.inJustDecodeBounds = true;
                        OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                        Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                        try {
                            ExifInterface ei = new ExifInterface(
                                    uri.getPath());

                            orientation = ei.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION,
                                    ExifInterface.ORIENTATION_NORMAL);

                            Log.d("image", "orientation is " + orientation);
                            Matrix matrix = new Matrix();

                            switch (orientation) {
                                case ExifInterface.ORIENTATION_NORMAL:
                                    System.out.println("ORIENTATION_NORMAL" + orientation);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                    matrix.setScale(-1, 1);
                                    System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_180:
                                    matrix.setRotate(180);
                                    break;
                                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                    matrix.setRotate(180);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSPOSE:
                                    matrix.setRotate(90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_90:
                                    matrix.setRotate(90);
                                    break;
                                case ExifInterface.ORIENTATION_TRANSVERSE:
                                    matrix.setRotate(-90);
                                    matrix.postScale(-1, 1);
                                    break;
                                case ExifInterface.ORIENTATION_ROTATE_270:
                                    matrix.setRotate(-90);
                                    break;
                                default:
                                    break;

                            }
                            Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                            if (OriginalPhoto == null) {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                return;
                            }

                            OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                    OriginalPhoto.getHeight(), matrix, true);

                            Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                            if (rotateduri != null) {
                                if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                    performCrop(rotateduri);
                                else {
                                    this.OriginalPhoto = OriginalPhoto;
//                                    cimg_user.setImageBitmap(OriginalPhoto);
                                    capturedFile = new File(getNewFile());
                                    FileOutputStream fOut = null;
                                    try {
                                        fOut = new FileOutputStream(capturedFile);
                                    } catch (FileNotFoundException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                                    try {
                                        fOut.flush();
                                    } catch (IOException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    try {
                                        fOut.close();
                                    } catch (IOException e) {
                                        AppDelegate.LogE(e);
                                    }
                                    arrayImageFile.add(capturedFile);
                                    updateImageView();
                                }
                            } else {
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }

                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                            AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                break;
            case AppDelegate.PIC_CROP: {
                AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                try {
                    cropimageUri = data.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        if (OriginalPhoto != null) {
                            OriginalPhoto.recycle();
                            OriginalPhoto = null;
                        }
                        OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                        AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                        OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                        AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
//                        cimg_user.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));

                        capturedFile = new File(getNewFile());
                        FileOutputStream fOut = null;
                        try {
                            fOut = new FileOutputStream(capturedFile);
                        } catch (FileNotFoundException e) {
                            AppDelegate.LogE(e);
                        }
                        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        try {
                            fOut.flush();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            fOut.close();
                        } catch (IOException e) {
                            AppDelegate.LogE(e);
                        }
                        arrayImageFile.add(capturedFile);
                        updateImageView();


                        // getActivity().getContentResolver().delete(cropimageUri, null, null);
                    } else {
                        AppDelegate.LogE("at onActivicTyResult failed to crop");
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }
    }

    private void setVisiblePic(int value) {
        rl_c_pic_0.setVisibility(View.INVISIBLE);
        rl_c_pic_1.setVisibility(View.INVISIBLE);
        rl_c_pic_2.setVisibility(View.INVISIBLE);
        rl_c_pic_3.setVisibility(View.INVISIBLE);
        switch (value) {
            case 0:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                break;
            case 1:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                break;
            case 2:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                break;
            case 3:
                rl_c_pic_0.setVisibility(View.VISIBLE);
                rl_c_pic_1.setVisibility(View.VISIBLE);
                rl_c_pic_2.setVisibility(View.VISIBLE);
                rl_c_pic_3.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void updateImageView() {
        ll_c_img_layout.setVisibility(View.VISIBLE);
        if (arrayImageFile.size() == 1) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            setVisiblePic(0);
        } else if (arrayImageFile.size() == 2) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            setVisiblePic(1);
        } else if (arrayImageFile.size() == 3) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(1).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            setVisiblePic(2);
        } else if (arrayImageFile.size() == 4) {
            img_c_pic_0.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(0).getAbsolutePath()));
            img_c_pic_1.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(01).getAbsolutePath()));
            img_c_pic_2.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(02).getAbsolutePath()));
            img_c_pic_3.setImageBitmap(BitmapFactory.decodeFile(arrayImageFile.get(03).getAbsolutePath()));
            setVisiblePic(3);
        } else {
            ll_c_img_layout.setVisibility(View.GONE);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

}
