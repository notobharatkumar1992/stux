package com.stux.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.AppDelegate;
import com.stux.R;
import com.stux.activities.MainActivity;
import com.stux.constants.Tags;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 06/28/2016.
 */
public class NoInternetConnectionFragment extends Fragment implements View.OnClickListener {

    private TextView txt_c_wake_up, txt_c_internet, txt_c_try_again;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.no_internet_connected, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        view.findViewById(R.id.img_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.txt_c_header).setVisibility(View.GONE);
        if (getArguments().getString(Tags.FROM).equalsIgnoreCase(Tags.LOGIN)) {
            view.findViewById(R.id.img_c_left).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.img_c_left).setVisibility(View.VISIBLE);
            ((ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
            ((ImageView) view.findViewById(R.id.img_c_left)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).toggleSlider();
                }
            });
        }

        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);

        txt_c_wake_up = (TextView) view.findViewById(R.id.txt_c_wake_up);
        txt_c_internet = (TextView) view.findViewById(R.id.txt_c_internet);
        txt_c_try_again = (TextView) view.findViewById(R.id.txt_c_try_again);
        txt_c_try_again.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_try_again:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                break;
        }
    }
}
