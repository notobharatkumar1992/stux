package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.FollowersRecyclerViewAdapter;

/**
 * Created by Bharat on 07/13/2016.
 */
public class FollowingListFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    private Handler mHandler;
    private ArrayList<UserDataModel> userArray = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;
    private TextView txt_c_no_list;
    private int followCounter = 1, followTotalPage = -1;
    private int preLast, selected_position = -1;
    private boolean followAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private FollowersRecyclerViewAdapter rcAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.follower_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        initView(view);
        setHandler();
        if (userArray.size() == 0) {
            callFollowersAsync();
        } else {
            mHandler.sendEmptyMessage(1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    txt_c_no_list.setVisibility(userArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No record found.");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Following");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);

        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new FollowersRecyclerViewAdapter(getActivity(), userArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(getActivity(), 15), 0, AppDelegate.dpToPix(getActivity(), 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (followTotalPage != 0 && !followAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callFollowersAsync();
                                 followAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + followTotalPage + ", " + followAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    private void callFollowAsync(String status, String seller_id) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.following_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.follower_id, seller_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.FOLLOW,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callFollowersAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, followCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_FOLLOWING,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.follower_id)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.user, userArray.get(position));
            Fragment fragment = new SellersProfileFragment();
            if (userArray.get(position).userId.equalsIgnoreCase(userData.userId)) {
                fragment = new MyProfileFragment();
            } else {
                fragment = new SellersProfileFragment();
            }
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.follow_status)) {
            this.selected_position = position;
            callFollowAsync(userArray.get(position).follow_status == 1 ? "0" : "1", userArray.get(position).userId);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FOLLOW)) {
            mHandler.sendEmptyMessage(11);
            parseFollowStatusResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_FOLLOWING)) {
            if (followCounter == 1)
                mHandler.sendEmptyMessage(13);
            swipyrefreshlayout.setRefreshing(false);
            parseFollowersListResult(result);
        }
    }

    private void parseFollowersListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    followTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        UserDataModel userDataModel = new UserDataModel();
                        userDataModel.role = object.getString(Tags.role);
                        userDataModel.image = object.getString(Tags.image);
                        userDataModel.status = object.getString(Tags.status);
                        userDataModel.is_verified = object.getString(Tags.is_verified);
                        userDataModel.first_name = object.getString(Tags.first_name);
                        userDataModel.last_name = object.getString(Tags.last_name);
                        userDataModel.email = object.getString(Tags.email);
                        userDataModel.created = object.getString(Tags.created);

                        userDataModel.facebook_id = object.getString(Tags.social_id);
                        userDataModel.following_count = JSONParser.getInt(object, Tags.following_count);
                        userDataModel.followers_count = JSONParser.getInt(object, Tags.followers_count);
                        userDataModel.total_product = JSONParser.getInt(object, Tags.total_product);
                        userDataModel.follow_status = JSONParser.getInt(object, Tags.follow_status);

                        JSONObject studentObject = object.getJSONObject(Tags.student_detail);
                        userDataModel.str_Gender = studentObject.getString(Tags.gender);
                        userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                        userDataModel.userId = studentObject.getString(Tags.user_id);
                        userDataModel.dob = studentObject.getString(Tags.dob);
                        userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                        userDataModel.institutionModel = new InstitutionModel();
                        userDataModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            userDataModel.institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                            userDataModel.institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                        } else {
                            userDataModel.institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                        }
                        userDataModel.institutionModel.department_name = studentObject.getString(Tags.department_name);
                        userArray.add(userDataModel);
                    }
                } else {
                    followTotalPage = 0;
                }
                followCounter++;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
        mHandler.sendEmptyMessage(1);
    }

    private void parseFollowStatusResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (!object.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            } else {
//                userArray.get(selected_position).follow_status = userArray.get(selected_position).follow_status == 1 ? 0 : 1;
                userArray.remove(selected_position);
                mHandler.sendEmptyMessage(1);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;

            case R.id.img_c_right:
                break;

        }
    }
}
