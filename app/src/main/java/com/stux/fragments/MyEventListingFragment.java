package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.EventModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.EventRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class MyEventListingFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener, OnListItemClickListenerWithHeight {

    private ArrayList<EventModel> eventArray = new ArrayList<>();

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    private int eventCounter = 1, eventTotalPage = -1;
    private boolean eventAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private EventRecyclerViewAdapter rcAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_event_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        eventArray.clear();
        eventCounter = 1;
        eventTotalPage = -1;
        if (eventArray.size() == 0)
            callCampusListAsync();
        else
            mHandler.sendEmptyMessage(2);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.sold_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, eventCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_MY_EVENTS,
                    mPostArrayList, null);
            if (eventCounter == 1)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + eventArray.size());
                    txt_c_no_list.setVisibility(eventArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No event available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Event Lists");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
        view.findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new CreateEventFragment());
            }
        });


        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new EventRecyclerViewAdapter(getActivity(), eventArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(getActivity(), 15), 0, AppDelegate.dpToPix(getActivity(), 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (eventTotalPage != 0 && !eventAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callCampusListAsync();
                                 eventAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + eventTotalPage + ", " + eventAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MY_EVENTS)) {
            if (eventCounter > 1) {
                eventAsyncExcecuting = false;
            }
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                eventTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    EventModel eventModel = new EventModel();
                    eventModel.id = object.getString(Tags.id);
                    eventModel.location_type = object.getString(Tags.location_type);
                    eventModel.event_type = object.getString(Tags.event_type);
                    eventModel.event_name = object.getString(Tags.event_name);
                    eventModel.banner_image = object.getString(Tags.banner_image);

                    eventModel.theme = object.getString(Tags.theme);
                    eventModel.details = object.getString(Tags.details);
                    eventModel.venue = object.getString(Tags.venue);
                    eventModel.location = object.getString(Tags.location);
                    eventModel.tickets = object.getString(Tags.tickets);

                    eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
                    eventModel.gate_fees = object.getString(Tags.gate_fees);
                    eventModel.contact_no = object.getString(Tags.contact_no);
                    eventModel.event_start_time = object.getString(Tags.event_start_time);

                    eventModel.event_end_time = object.getString(Tags.event_end_time);
                    eventModel.created = object.getString(Tags.created);
//                    eventModel.modified = object.getString(Tags.modified);
                    eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                    eventModel.latitude = object.getString(Tags.latitude);
                    eventModel.longitude = object.getString(Tags.longitude);

                    JSONObject userObject = object.getJSONObject(Tags.user);
                    eventModel.user_first_name = userObject.getString(Tags.first_name);
                    eventModel.user_last_name = userObject.getString(Tags.last_name);
                    eventModel.user_image = userObject.getString(Tags.image);

                    JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                    if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                        eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                        eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
                        if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                            eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
                        }
                        if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        } else {
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                    } else {
                        eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
                    }

                    eventArray.add(eventModel);
                }
            } else {
                eventTotalPage = 0;
            }
            eventCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.event_name)) {
            AppDelegate.LogT("event_list_view onItemClick");
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.EVENT, eventArray.get(position));
            Fragment fragment = new EventDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int height) {

    }
}
