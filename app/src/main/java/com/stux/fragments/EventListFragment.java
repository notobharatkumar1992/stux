package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.stux.Adapters.EventListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.EventModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/27/2016.
 */
public class EventListFragment extends Fragment implements OnReciveServerResponse {

    private ArrayList<EventModel> eventArray = new ArrayList<>();

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    private ListView list_view;
    private EventListAdapter mEventAdapter;
    private int eventCounter = 1, eventTotalPage = -1;
    private boolean eventAsyncExcecuting = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.campus_list_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        if (eventArray.size() == 0)
            callCampusListAsync();
        else mHandler.sendEmptyMessage(3);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, eventCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.EVENTS_LIST,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 3) {
                    AppDelegate.LogT("mEventAdapter notified = " + eventArray.size());
                    txt_c_no_list.setVisibility(eventArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No event available");
                    mEventAdapter.notifyDataSetChanged();
                    list_view.invalidate();
                }
            }
        };
    }

    private int preLast;


    private void initView(View view) {
        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        mEventAdapter = new EventListAdapter(getActivity(), R.id.txt_line1, eventArray);
        list_view = (ListView) view.findViewById(R.id.list_view);
        list_view.setAdapter(mEventAdapter);
        list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                AppDelegate.LogT("onScroll => counter = " + eventCounter + ", totalPage = " + eventTotalPage);
                if (eventCounter <= eventTotalPage)
                    if (lastItem == totalItemCount) {
                        if (preLast != lastItem) { //to avoid multiple calls for last item
                            preLast = lastItem;
                            callCampusListAsync();
                        }
                    }
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppDelegate.LogT("event_list_view onItemClick");
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.EVENT, eventArray.get(position));
                Fragment fragment = new EventDetailFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
            }
        });
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
//        mPullRefreshGridView.setRefreshing(true);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENTS_LIST)) {
            if (eventCounter > 1) {
                eventAsyncExcecuting = false;
            }
            parseEventListResult(result);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                eventTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    EventModel eventModel = new EventModel();
                    eventModel.id = object.getString(Tags.id);
                    eventModel.location_type = object.getString(Tags.location_type);
                    eventModel.event_type = object.getString(Tags.event_type);
                    eventModel.event_name = object.getString(Tags.event_name);
                    eventModel.banner_image = object.getString(Tags.banner_image);

                    eventModel.theme = object.getString(Tags.theme);
                    eventModel.details = object.getString(Tags.details);
                    eventModel.venue = object.getString(Tags.venue);
                    eventModel.location = object.getString(Tags.location);
                    eventModel.tickets = object.getString(Tags.tickets);

                    eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
                    eventModel.gate_fees = object.getString(Tags.gate_fees);
                    eventModel.contact_no = object.getString(Tags.contact_no);
                    eventModel.event_start_time = object.getString(Tags.event_start_time);

                    eventModel.event_end_time = object.getString(Tags.event_end_time);
                    eventModel.created = object.getString(Tags.created);
//                    eventModel.modified = object.getString(Tags.modified);
                    eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                    eventModel.latitude = object.getString(Tags.latitude);
                    eventModel.longitude = object.getString(Tags.longitude);


                    JSONObject userObject = object.getJSONObject(Tags.user);
                    eventModel.user_first_name = userObject.getString(Tags.first_name);
                    eventModel.user_last_name = userObject.getString(Tags.last_name);
                    eventModel.user_image = userObject.getString(Tags.image);

                    JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                    if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                        eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                        eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
                        if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                            eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
                        }
                        if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        } else {
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        }
                    } else {
                        eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
                        eventModel.user_department_name = studentObject.getString(Tags.department_name);
                    }

                    eventArray.add(eventModel);
                }
            } else {
                eventTotalPage = 0;
            }
            eventCounter++;

            mHandler.sendEmptyMessage(3);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

}
