package com.stux.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.Adapters.SpinnerFavCategoryAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.FavCategoryModel;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DateUtils;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by Bharat on 06/17/2016.
 */
public class EditProfileFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private EditText et_first_name, et_last_name;
    private TextView txt_c_place, txt_c_dob, txt_c_male, txt_c_female, txt_c_change_email;

    private Spinner spn_fav_category;

    private SpinnerFavCategoryAdapter adapterFavCategory;

    public ArrayList<FavCategoryModel> arrayFavCategoryModels = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel dataModel;
    private Handler mHandler;

    private ImageView img_c_profile;

    private Bitmap OriginalPhoto;
    //    private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        initView(view);
        setHandler();
        setValues();
    }

    private void setValues() {
        if (dataModel != null) {
            et_first_name.setText(dataModel.first_name);
            et_last_name.setText(dataModel.last_name);
            if (AppDelegate.isValidString(prefs.getUserdata().image)) {
                Picasso.with(getActivity()).load(prefs.getUserdata().image).into(new Target() {
                    @Override
                    public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (bitmap != null) {
                            img_c_profile.setImageBitmap(bitmap);
                            OriginalPhoto = bitmap;
                        }
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }
                });
            }
            if (dataModel.str_Gender.equals("1") || dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                selected_gender = Tags.Male;
                txt_c_male.setSelected(true);
                txt_c_female.setSelected(false);
                txt_c_male.setTextColor(getResources().getColor(R.color.profile_desc));
                txt_c_female.setTextColor(getResources().getColor(R.color.hint_color));
            } else {
                selected_gender = Tags.Female;
                txt_c_male.setSelected(false);
                txt_c_female.setSelected(true);
                txt_c_male.setTextColor(getResources().getColor(R.color.hint_color));
                txt_c_female.setTextColor(getResources().getColor(R.color.profile_desc));
            }

            try {
                if (AppDelegate.isValidString(dataModel.dob)) {
//                    txt_c_dob.setText(AppDelegate.isValidString(dataModel.dob) ? dataModel.dob : "");
                    txt_c_dob.setText(new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(dataModel.dob)));
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
                txt_c_dob.setText("");
            }

            if (arrayFavCategoryModels.size() > 0 && AppDelegate.isValidString(dataModel.fav_cat_id)) {
                for (int i = 0; i < arrayFavCategoryModels.size(); i++) {
                    FavCategoryModel favCategoryModel = arrayFavCategoryModels.get(i);
                    if (favCategoryModel.id.equalsIgnoreCase(dataModel.fav_cat_id)) {
                        spn_fav_category.setSelection(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_getFavCategoryApi();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    adapterFavCategory.notifyDataSetChanged();
                    spn_fav_category.invalidate();

                    if (arrayFavCategoryModels.size() > 0 && AppDelegate.isValidString(dataModel.fav_cat_id)) {
                        for (int i = 0; i < arrayFavCategoryModels.size(); i++) {
                            FavCategoryModel favCategoryModel = arrayFavCategoryModels.get(i);
                            if (favCategoryModel.id.equalsIgnoreCase(dataModel.fav_cat_id)) {
                                spn_fav_category.setSelection(i);
                                break;
                            }
                        }
                    }
                }
            }
        };
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText(getString(R.string.Edit_Profile));
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        ((ImageView) view.findViewById(R.id.img_c_left)).setVisibility(View.VISIBLE);

        et_first_name = (EditText) view.findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_bold)));
        et_last_name = (EditText) view.findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_bold)));

        txt_c_place = (TextView) view.findViewById(R.id.txt_c_place);
        txt_c_dob = (TextView) view.findViewById(R.id.txt_c_dob);
        txt_c_dob.setOnClickListener(this);
        txt_c_male = (TextView) view.findViewById(R.id.txt_c_male);
        txt_c_male.setOnClickListener(this);
        txt_c_female = (TextView) view.findViewById(R.id.txt_c_female);
        txt_c_female.setOnClickListener(this);
        view.findViewById(R.id.txt_c_change_password).setOnClickListener(this);
        txt_c_change_email = (TextView) view.findViewById(R.id.txt_c_change_email);

        spn_fav_category = (Spinner) view.findViewById(R.id.spn_fav_category);
        adapterFavCategory = new SpinnerFavCategoryAdapter(getActivity(), arrayFavCategoryModels);
        spn_fav_category.setAdapter(adapterFavCategory);
        spn_fav_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_fav_category_position = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        img_c_profile = (ImageView) view.findViewById(R.id.img_c_profile);
        img_c_profile.setOnClickListener(this);

        view.findViewById(R.id.txt_c_continue).setOnClickListener(this);
    }

    int selected_fav_category_position = 0;

    private void execute_updateProfileApi() {
        if (et_first_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_First_Name));
        } else if (et_last_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_Last_name));
        } else if (OriginalPhoto == null) {
            AppDelegate.showAlert(getActivity(), "Please select image.");
        } else if (txt_c_dob.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select your Date of Birth.");
        } else if (selected_fav_category_position == 0) {
            AppDelegate.showAlert(getActivity(), "Please select Favourite Category.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.first_name, et_first_name.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, et_last_name.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, dataModel.email);

            if (txt_c_dob.length() > 0) {
                String time = "";
                try {
                    time = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(txt_c_dob.getText().toString()));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, time);
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gender, selected_gender);

//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, "");

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.fav_cat_id, arrayFavCategoryModels.get(selected_fav_category_position).id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    EditProfileFragment.this, ServerRequestConstants.EDIT_PROFILE,
                    mPostArrayList, EditProfileFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void execute_getFavCategoryApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    EditProfileFragment.this, ServerRequestConstants.FAV_CATEGORY,
                    mPostArrayList, EditProfileFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;

            case R.id.img_c_profile:
                showImageSelectorList();
                break;

            case R.id.txt_c_dob:
                showDatePickerDialog();
                break;

            case R.id.txt_c_continue:
                execute_updateProfileApi();
                break;

            case R.id.txt_c_male:
                selected_gender = Tags.Male;
                txt_c_male.setSelected(true);
                txt_c_female.setSelected(false);
                txt_c_male.setTextColor(getResources().getColor(R.color.profile_desc));
                txt_c_female.setTextColor(getResources().getColor(R.color.hint_color));
                break;

            case R.id.txt_c_female:
                selected_gender = Tags.Female;
                txt_c_male.setSelected(false);
                txt_c_female.setSelected(true);
                txt_c_male.setTextColor(getResources().getColor(R.color.hint_color));
                txt_c_female.setTextColor(getResources().getColor(R.color.profile_desc));
                break;

            case R.id.txt_c_change_password:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new ChangePasswordFragment());
                break;


        }
    }

    private String selected_gender = Tags.Male;

    private DatePickerDialog datePickerDialog;
    private int year = -1, month = -1, day = -1;
    private Calendar calendar;

    private void showDatePickerDialog() {
        calendar = Calendar.getInstance();
        if (AppDelegate.isValidString(txt_c_dob.getText().toString())) {
            try {
                calendar.setTime(new SimpleDateFormat("dd MMM yyyy").parse(txt_c_dob.getText().toString()));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
        }
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if (DateUtils.isAfterDay(Calendar.getInstance(), calendar)) {
                    EditProfileFragment.this.year = year;
                    EditProfileFragment.this.month = monthOfYear;
                    EditProfileFragment.this.day = dayOfMonth;
                    txt_c_dob.setText(new SimpleDateFormat("dd MMM yyyy").format(calendar.getTime()));
                } else {
                    AppDelegate.showAlert(getActivity(), "Date of birth can't be equal to or greater than Today.");
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Avatar", "  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        showAvatarAlert();
                        break;
                    case 1:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 2:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void showAvatarAlert() {
        try {
            final AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(true);
            mAlert.setTitle("Use image");
            LinearLayout ll_c_img_01, ll_c_img_02;
            ImageView img_01, img_02;
            View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_default_user_image, null, false);

            img_01 = (ImageView) view.findViewById(R.id.img_01);
            img_02 = (ImageView) view.findViewById(R.id.img_02);

            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                img_01.setImageResource(R.drawable.user_male_1);
                img_02.setImageResource(R.drawable.user_male_2);
            } else {
                img_01.setImageResource(R.drawable.user_female_1);
                img_02.setImageResource(R.drawable.user_female_2);
            }

            ll_c_img_01 = (LinearLayout) view.findViewById(R.id.ll_c_img_01);
            ll_c_img_02 = (LinearLayout) view.findViewById(R.id.ll_c_img_02);
            mAlert.setView(view);
            mAlert.setNegativeButton(
                    Tags.CANCEL,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = mAlert.show();

            ll_c_img_01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    AppDelegate.LogT("ll_c_img_01 dataModel.str_Gender = " + dataModel.str_Gender);

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
                    }

                    img_c_profile.setImageBitmap(OriginalPhoto);
                }
            });
            ll_c_img_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    AppDelegate.LogT("ll_c_img_02 dataModel.str_Gender = " + dataModel.str_Gender);
                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_2);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_2);
                    }

                    img_c_profile.setImageBitmap(OriginalPhoto);
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void writeImageFile() {
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case AppDelegate.SELECT_PICTURE:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri uri = data.getData();
                        imageUri = uri;
                        AppDelegate.LogT("uri=" + uri);
                        if (uri != null) {
                            // User had pick an image.
                            Cursor cursor = getActivity()
                                    .getContentResolver()
                                    .query(uri,
                                            new String[]{MediaStore.Images.ImageColumns.DATA},
                                            null, null, null);
                            cursor.moveToFirst();

                            // Link to the image
                            final String imageFilePath = cursor.getString(0);
                            cursor.close();
                            int orientation = 0;
                            if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                                try {
                                    ExifInterface ei = new ExifInterface(imageFilePath);
                                    orientation = ei.getAttributeInt(
                                            TAG_ORIENTATION,
                                            ORIENTATION_NORMAL);
                                } catch (IOException e) {
                                    AppDelegate.LogE(e);
                                }
                                capturedFile = new File(imageFilePath);
                                Bitmap OriginalPhoto = AppDelegate.decodeFile(capturedFile);

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);

                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                    default:
                                        break;

                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = OriginalPhoto;
                                        img_c_profile.setImageBitmap(OriginalPhoto);
                                    }
//                                performCrop(rotateduri);
                                } else {
                                    if (imageUri != null) {
                                        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                            performCrop(imageUri);
                                        else {
                                            this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                            img_c_profile.setImageBitmap(OriginalPhoto);
                                        }
//                                    performCrop(imageUri);
                                    } else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                    break;
                case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                    AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);

                    //Get our saved file into a bitmap object:
                    try {
                        if (resultCode == Activity.RESULT_OK) {
                            int orientation = 0;
                            Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                            BitmapFactory.Options o = new BitmapFactory.Options();
                            o.inJustDecodeBounds = true;
                            Bitmap OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                            Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                            try {
                                ExifInterface ei = new ExifInterface(
                                        uri.getPath());

                                orientation = ei.getAttributeInt(
                                        ExifInterface.TAG_ORIENTATION,
                                        ExifInterface.ORIENTATION_NORMAL);

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);

                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        img_c_profile.setImageBitmap(OriginalPhoto);
                                    }
                                } else {
                                    AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                                }

                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    break;
                case AppDelegate.PIC_CROP: {
                    AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                    try {
                        if (data.getData() != null)
                            cropimageUri = data.getData();
                        if (resultCode == Activity.RESULT_OK) {
                            if (OriginalPhoto != null) {
                                OriginalPhoto.recycle();
                                OriginalPhoto = null;
                            }
                            OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                            AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                            if (OriginalPhoto == null) {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                return;
                            }
                            OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                            AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                            img_c_profile.setImageBitmap(OriginalPhoto);

                            // getActivity().getContentResolver().delete(cropimageUri, null, null);
                        } else {
                            AppDelegate.LogE("at onActivicTyResult failed to crop");
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
            AppDelegate.LogE(e);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FAV_CATEGORY)) {
            parseFavCategoryResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            parseUpdateProfileResult(result);
        }
    }

    private void parseUpdateProfileResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);
                ((MainActivity) getActivity()).updateUserDetail();

                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new HomeFragment());
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseFavCategoryResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayFavCategoryModels.clear();
                arrayFavCategoryModels.add(new FavCategoryModel("Select Category"));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    FavCategoryModel model = new FavCategoryModel();
                    model.id = object.getString(Tags.id);
                    model.name = object.getString(Tags.title);
                    arrayFavCategoryModels.add(model);
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }
}
