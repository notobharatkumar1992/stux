package com.stux.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.stux.Adapters.SpinnerDepartmentAdapter;
import com.stux.Adapters.SpinnerInstitutionDetailAdapter;
import com.stux.Adapters.SpinnerInstitutionStateAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DepartmentModel;
import com.stux.Models.InstitutionDetailModel;
import com.stux.Models.InstitutionModel;
import com.stux.Models.InstitutionStateModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class SignUpInstitutionFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private TextView txt_c_header;
    private Spinner spn_institution_state, spn_institution_name, spn_department;
    private EditText et_institution_name, et_department_name;

    private SpinnerInstitutionStateAdapter adapterInstitutionState;
    private SpinnerInstitutionDetailAdapter adapterInstitutionName;
    private SpinnerDepartmentAdapter adapterDepartment;

    public ArrayList<InstitutionStateModel> arrayStateModels = new ArrayList<>();
    public ArrayList<InstitutionDetailModel> arrayInstitutionDetailModels = new ArrayList<>();
    public ArrayList<DepartmentModel> arrayDepartmentModels = new ArrayList<>();

    private ImageView img_c_left;

    public InstitutionModel institutionModel;

    public Prefs prefs;
    public Handler mHandler;
    public AppDelegate mInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sign_up_institution, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AppDelegate.LogT("SignUpInst onAttach called");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInstance = AppDelegate.getInstance(getActivity());
        prefs = new Prefs(getActivity());
        if (prefs.getTempInstitutionModel() != null)
            institutionModel = prefs.getTempInstitutionModel();
        else
            institutionModel = new InstitutionModel();
        initView(view);
        setHandler();
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            if (arrayStateModels.size() < 2)
                execute_getInstitutionStateApi();
        } else
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1 || msg.what == 2 || msg.what == 3) {
                    adapterInstitutionState.notifyDataSetChanged();
                    spn_institution_state.invalidate();
                    adapterInstitutionName.notifyDataSetChanged();
                    spn_institution_name.invalidate();
//                    adapterDepartment.notifyDataSetChanged();
//                    spn_department.invalidate();
                } else if (msg.what == 2) {
                    adapterInstitutionName.notifyDataSetChanged();
                    spn_institution_name.invalidate();
                } else if (msg.what == 3) {
//                    adapterDepartment.notifyDataSetChanged();
//                    spn_department.invalidate();
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
        if (institutionModel.selected_institute_name > 0 && arrayInstitutionDetailModels.size() > 1) {
            spn_institution_name.setSelection(institutionModel.selected_institute_name);
            if (institutionModel.selected_institute_name == arrayInstitutionDetailModels.size() - 1) {
                et_institution_name.setVisibility(View.VISIBLE);
//                et_department_name.setVisibility(View.VISIBLE);
//                spn_department.setVisibility(View.GONE);
            } else {
                et_institution_name.setVisibility(View.GONE);
//                et_department_name.setVisibility(View.GONE);
//                spn_department.setVisibility(View.VISIBLE);
            }
        }
//        if (institutionModel.selected_department > 0 && arrayDepartmentModels.size() > 1) {
////            spn_department.setSelection(institutionModel.selected_department);
//            if (institutionModel.selected_department == arrayDepartmentModels.size() - 1) {
//                et_department_name.setVisibility(View.VISIBLE);
//            } else {
//                et_department_name.setVisibility(View.GONE);
//            }
//        }
    }

    private void initView(View view) {
        txt_c_header = (TextView) view.findViewById(R.id.txt_c_header);
        txt_c_header.setText(getString(R.string.Sign_Up));
        img_c_left = (ImageView) view.findViewById(R.id.img_c_left);
        img_c_left.setImageDrawable(getResources().getDrawable(R.drawable.back));
        img_c_left.setOnClickListener(this);

        et_institution_name = (EditText) view.findViewById(R.id.et_institution_name);
        et_institution_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_institution_name.setVisibility(View.GONE);
        et_department_name = (EditText) view.findViewById(R.id.et_department_name);
        et_department_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_HN_medium)));
        et_department_name.setVisibility(View.VISIBLE);

        if (arrayStateModels.size() == 0) {
            arrayStateModels.add(new InstitutionStateModel("Select Institution State"));
        }
        if (arrayInstitutionDetailModels.size() == 0) {
            arrayInstitutionDetailModels.add(new InstitutionDetailModel("Select Institution Name"));
        }
        if (arrayDepartmentModels.size() == 0) {
            arrayDepartmentModels.add(new DepartmentModel("Select Department"));
        }

        spn_institution_state = (Spinner) view.findViewById(R.id.spn_institution_state);
        adapterInstitutionState = new SpinnerInstitutionStateAdapter(getActivity(), arrayStateModels);
        spn_institution_state.setAdapter(adapterInstitutionState);

        spn_institution_name = (Spinner) view.findViewById(R.id.spn_institution_name);
        adapterInstitutionName = new SpinnerInstitutionDetailAdapter(getActivity(), arrayInstitutionDetailModels);
        spn_institution_name.setAdapter(adapterInstitutionName);

        spn_department = (Spinner) view.findViewById(R.id.spn_department);
        adapterDepartment = new SpinnerDepartmentAdapter(getActivity(), arrayDepartmentModels);
        spn_department.setAdapter(adapterDepartment);
        spn_department.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setSpinnerItemListerner();
            }
        }, 1000);
        view.findViewById(R.id.txt_c_continue).setOnClickListener(this);
    }

    private void setSpinnerItemListerner() {
        spn_institution_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                institutionModel.selected_institute_state = position;
                initSpinnerSelection(1);
                if (position == 0) {
                    return;
                }
                institutionModel.institution_state_id = arrayStateModels.get(position).id;
                institutionModel.institution_state_name = arrayStateModels.get(position).region_name;

                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    execute_getInstitutionNameApi(arrayStateModels.get(position).id);
                } else {
                    spn_institution_state.setSelection(0);
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn_institution_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                institutionModel.selected_institute_name = position;
                initSpinnerSelection(2);
                if (position == 0) {
                    return;
                }

                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    institutionModel.institution_name_id = arrayInstitutionDetailModels.get(position).id;
                    institutionModel.institution_name = arrayInstitutionDetailModels.get(position).institute_name;
                    if (position == arrayInstitutionDetailModels.size() - 1) {
                        et_institution_name.setVisibility(View.VISIBLE);
//                        et_department_name.setVisibility(View.VISIBLE);
//                        spn_department.setVisibility(View.GONE);
                    } else {
                        et_institution_name.setVisibility(View.GONE);
//                        et_department_name.setVisibility(View.GONE);
//                        spn_department.setVisibility(View.VISIBLE);
//                        execute_getInstitutionDepartmentApi(arrayInstitutionDetailModels.get(position).id);
                    }
                } else {
                    spn_institution_name.setSelection(0);
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spn_department.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                institutionModel.selected_department = position;
                if (position == 0) {
                    return;
                }

                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    if (position == arrayDepartmentModels.size() - 1) {
                        et_department_name.setVisibility(View.VISIBLE);
                    } else {
                        et_department_name.setVisibility(View.GONE);
                    }
                } else {
                    spn_department.setSelection(0);
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initSpinnerSelection(int value) {
        switch (value) {
            case 1:
                arrayInstitutionDetailModels.clear();
                arrayInstitutionDetailModels.add(new InstitutionDetailModel("Select Institution Name"));
                institutionModel.selected_institute_name = -1;
                spn_institution_name.setSelection(0);

                arrayDepartmentModels.clear();
                arrayDepartmentModels.add(new DepartmentModel("Select Department"));
//                spn_department.setSelection(0);
                institutionModel.selected_department = -1;
                break;
            case 2:
                arrayDepartmentModels.clear();
                arrayDepartmentModels.add(new DepartmentModel("Select Department"));
//                spn_department.setSelection(0);
                institutionModel.selected_department = -1;
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
            case R.id.txt_c_continue:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    validateData();
                } else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                break;
        }
    }

    private void validateData() {
        if (institutionModel.selected_institute_state < 1) {
            AppDelegate.showAlert(getActivity(), "Please select your institution state.");
        } else if (institutionModel.selected_institute_name < 1) {
            AppDelegate.showAlert(getActivity(), "Please select your institution name.");
        } else if (institutionModel.selected_institute_name == arrayInstitutionDetailModels.size() - 1 && et_institution_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please write institution name.");
//        } else if (institutionModel.selected_institute_name != arrayInstitutionDetailModels.size() - 1 && institutionModel.selected_department < 1) {
//            AppDelegate.showAlert(getActivity(), "Please select department.");
        } else if (/*(institutionModel.selected_institute_name == arrayInstitutionDetailModels.size() - 1 || institutionModel.selected_department == arrayDepartmentModels.size() - 1) && */et_department_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please write department name.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {

            institutionModel.institution_state_id = arrayStateModels.get(institutionModel.selected_institute_state).id;
            institutionModel.institution_state_name = arrayStateModels.get(institutionModel.selected_institute_state).region_name;

            if (arrayInstitutionDetailModels.get(institutionModel.selected_institute_name).id.equalsIgnoreCase("other")) {
                institutionModel.institution_name = et_institution_name.getText().toString();
                institutionModel.institution_name_id = "other";

//                institutionModel.department_name_id = "other";
//                institutionModel.department_name = et_department_name.getText().toString();

            } else {
                institutionModel.institution_name = arrayInstitutionDetailModels.get(institutionModel.selected_institute_name).institute_name;
                institutionModel.institution_name_id = arrayInstitutionDetailModels.get(institutionModel.selected_institute_name).id;
                institutionModel.detailModel = arrayInstitutionDetailModels.get(institutionModel.selected_institute_name);

//                if (institutionModel.selected_department == arrayDepartmentModels.size() - 1) {
//                    institutionModel.department_name_id = "other";
//                    institutionModel.department_name = et_department_name.getText().toString();
//                } else {
//                    institutionModel.department_name_id = arrayDepartmentModels.get(institutionModel.selected_department).id;
//                    institutionModel.department_name = arrayDepartmentModels.get(institutionModel.selected_department).department_name;
//                }
            }

//            institutionModel.department_name_id = "other";
            institutionModel.department_name = et_department_name.getText().toString();

            prefs.setTempInstitutionModel(institutionModel);
            Fragment fragment = new SignUpPersonalFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.TEMP_INSTITUTION_MODEL, institutionModel);
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    private void execute_getInstitutionStateApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            mInstance.setPostParamsSecond(mPostArrayList, Tags.country_id, "1");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    SignUpInstitutionFragment.this, ServerRequestConstants.GET_STATE_LIST,
                    mPostArrayList, SignUpInstitutionFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    private void execute_getInstitutionNameApi(String value) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            mInstance.setPostParamsSecond(mPostArrayList, Tags.country_id, "1");
            mInstance.setPostParamsSecond(mPostArrayList, Tags.state_id, value + "");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    SignUpInstitutionFragment.this, ServerRequestConstants.GET_INSTITUTION_NAME,
                    mPostArrayList, SignUpInstitutionFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    private void execute_getInstitutionDepartmentApi(String value) {
        arrayDepartmentModels.clear();
        arrayDepartmentModels.add(new DepartmentModel("Select Department"));
//        spn_department.setSelection(0);
        institutionModel.selected_department = -1;

        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            mInstance.setPostParamsSecond(mPostArrayList, Tags.institution_id, value + "");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    SignUpInstitutionFragment.this, ServerRequestConstants.GET_DEPARTMENT,
                    mPostArrayList, SignUpInstitutionFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_STATE_LIST)) {
            parseStateListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_INSTITUTION_NAME)) {
            parseInstitutionName(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_DEPARTMENT)) {
            parseDepartmentName(result);
        }
    }

    private void parseDepartmentName(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayDepartmentModels.clear();
                arrayDepartmentModels.add(new DepartmentModel("Select Department"));

                institutionModel.selected_department = -1;

                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject object = jsonArray.getString(i);
                        DepartmentModel institutionNameModel = new DepartmentModel();
                        institutionNameModel.id = jsonArray.getString(i);
                        institutionNameModel.department_name = jsonArray.getString(i);

                        arrayDepartmentModels.add(institutionNameModel);
                    }
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
            arrayDepartmentModels.add(new DepartmentModel("Other"));
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(3);
    }

    private void parseInstitutionName(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayInstitutionDetailModels.clear();
                arrayInstitutionDetailModels.add(new InstitutionDetailModel("Select Institution Name"));


                arrayDepartmentModels.clear();
                arrayDepartmentModels.add(new DepartmentModel("Select Department"));

                institutionModel.selected_institute_name = -1;
                institutionModel.selected_department = -1;

                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        InstitutionDetailModel institutionNameModel = new InstitutionDetailModel();
                        institutionNameModel.id = object.getString(Tags.ID);
                        institutionNameModel.institute_name = object.getString(Tags.institute_name);
//                        institutionNameModel.country_id = object.getString(Tags.country_id);
                        institutionNameModel.state_id = object.getString(Tags.state_id);
//                        institutionNameModel.city_id = object.getString(Tags.city_id);
//                        institutionNameModel.address = object.getString(Tags.address);
//                        institutionNameModel.postal_code = object.getString(Tags.postal_code);
//                        institutionNameModel.email = object.getString(Tags.email);
//                        institutionNameModel.contact_no = object.getString(Tags.contact_no);
//                        institutionNameModel.ins_head_name = object.getString(Tags.ins_head_name);
//                        institutionNameModel.ins_head_contact = object.getString(Tags.ins_head_contact);
//                        institutionNameModel.department_name = object.getString(Tags.department_name);
                        institutionNameModel.created = object.getString(Tags.created);
                        institutionNameModel.status = object.getString(Tags.status);

                        arrayInstitutionDetailModels.add(institutionNameModel);
                    }
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
            arrayInstitutionDetailModels.add(new InstitutionDetailModel("Other"));
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(2);
    }

    private void parseStateListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                arrayStateModels.clear();
                arrayStateModels.add(new InstitutionStateModel("Select Institution State"));

                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    InstitutionStateModel stateModel = new InstitutionStateModel();
                    stateModel.id = object.getString(Tags.ID);
                    stateModel.country_id = object.getString(Tags.country_id);
                    stateModel.region_name = object.getString(Tags.region_name);
                    stateModel.slug = object.getString(Tags.slug);
                    stateModel.status = object.getString(Tags.status);
                    arrayStateModels.add(stateModel);
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }
}
