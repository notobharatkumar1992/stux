package com.stux.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.activities.SplashActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by NOTO on 5/24/2016.
 */
public class ProfileLandingFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    //    private DisplayImageOptions options;
    public static File capturedFile;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private CircleImageView cimg_user;
    private android.widget.ImageView img_loading;
    private TextView txt_c_user_name, txt_c_header, txt_c_right, txt_c_description;
    private ImageView img_c_left, img_c_right;
    private Handler mHandler;
    private Bitmap OriginalPhoto;
    private Prefs prefs;
    private UserDataModel dataModel;
    private InstitutionModel institutionModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_landing_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        institutionModel = prefs.getInstitutionModel();
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    private void initView(View view) {
        txt_c_header = (TextView) view.findViewById(R.id.txt_c_header);
        txt_c_header.setText(getString(R.string.Profile));

        img_c_left = (ImageView) view.findViewById(R.id.img_c_left);
        img_c_left.setImageResource(R.drawable.menu);
        img_c_left.setVisibility(View.VISIBLE);
        img_c_left.setOnClickListener(this);

        img_c_right = (ImageView) view.findViewById(R.id.img_c_right);
        img_c_right.setImageResource(R.drawable.filter);
        img_c_right.setVisibility(View.GONE);
        img_c_right.setOnClickListener(this);

        txt_c_right = (TextView) view.findViewById(R.id.txt_c_right);
        txt_c_right.setVisibility(View.VISIBLE);
        txt_c_right.setOnClickListener(this);

        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        img_loading = (android.widget.ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        txt_c_user_name = (TextView) view.findViewById(R.id.txt_c_user_name);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        view.findViewById(R.id.txt_c_continue).setOnClickListener(this);
        view.findViewById(R.id.ll_c_upload).setOnClickListener(this);

        txt_c_user_name.setText("Hello " + dataModel.first_name + ",");

        txt_c_description.setText("\"Welcome to " + institutionModel.institution_name + " Stux Market Place\"\nCongrats! Lets get started by postings things to sell\nand make money !");
        if (AppDelegate.isValidString(dataModel.image)) {
            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            Picasso.with(getActivity()).load(dataModel.image).into(new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                    img_loading.setVisibility(View.GONE);
                    if (bitmap != null) {
                        cimg_user.setImageBitmap(bitmap);
                        OriginalPhoto = bitmap;
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }
            });
        } else {
            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
            } else {
                OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
            }
            cimg_user.setImageBitmap(OriginalPhoto);
        }
    }

    private void execute_updateProfileApi() {
        if (OriginalPhoto == null) {
            AppDelegate.showAlert(getActivity(), "Please select image.");
        } else if (AppDelegate.haveNetworkConnection(getActivity())) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.first_name, dataModel.first_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, dataModel.last_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, dataModel.email);
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.password, dataModel.password);
            if (AppDelegate.isValidString(dataModel.dob))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, dataModel.dob);
            if (AppDelegate.isValidString(dataModel.str_Gender))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gender, dataModel.str_Gender);
            if (AppDelegate.isValidString(dataModel.city_name))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, "");
            if (AppDelegate.isValidString(dataModel.fav_cat_id))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.fav_cat_id, "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProfileLandingFragment.this, ServerRequestConstants.EDIT_PROFILE,
                    mPostArrayList, ProfileLandingFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_continue:
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new HomeFragment());
                execute_updateProfileApi();
                break;

            case R.id.ll_c_upload:
                showImageSelectorList();
                break;

            case R.id.txt_c_right:
                prefs.clearTempPrefs();
                prefs.clearSharedPreference();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.IS_SPLASH, Tags.FALSE);
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();
                break;

            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Avatar", "  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        showAvatarAlert();
                        break;
                    case 1:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 2:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void showAvatarAlert() {
        try {
            final AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(true);
            mAlert.setTitle("Use image");
            LinearLayout ll_c_img_01, ll_c_img_02;
            ImageView img_01, img_02;
            View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_default_user_image, null, false);

            img_01 = (ImageView) view.findViewById(R.id.img_01);
            img_02 = (ImageView) view.findViewById(R.id.img_02);
            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                img_01.setImageResource(R.drawable.user_male_1);
                img_02.setImageResource(R.drawable.user_male_2);
            } else {
                img_01.setImageResource(R.drawable.user_female_1);
                img_02.setImageResource(R.drawable.user_female_2);
            }

            ll_c_img_01 = (LinearLayout) view.findViewById(R.id.ll_c_img_01);
            ll_c_img_02 = (LinearLayout) view.findViewById(R.id.ll_c_img_02);
            mAlert.setView(view);
            mAlert.setNegativeButton(
                    Tags.CANCEL,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = mAlert.show();

            ll_c_img_01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
                    }

                    cimg_user.setImageBitmap(OriginalPhoto);
                }
            });
            ll_c_img_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_2);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_2);
                    }

                    cimg_user.setImageBitmap(OriginalPhoto);
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void writeImageFile() {
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            parseUpdateProfileResult(result);
        }
    }

    private void parseUpdateProfileResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);

                ((MainActivity) getActivity()).updateUserDetail();
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new HomeFragment());

            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppDelegate.LogT("onActivityResult Profile");
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case AppDelegate.SELECT_PICTURE:
                    if (resultCode == Activity.RESULT_OK) {
                        Uri uri = data.getData();
                        imageUri = uri;
                        AppDelegate.LogT("uri=" + uri);
                        if (uri != null) {
                            // User had pick an image.
                            Cursor cursor = getActivity().getContentResolver()
                                    .query(uri, new String[]{MediaStore.Images.ImageColumns.DATA},
                                            null, null, null);
                            cursor.moveToFirst();

                            // Link to the image
                            final String imageFilePath = cursor.getString(0);
                            cursor.close();
                            int orientation = 0;
                            if (imageFilePath != null && !imageFilePath.equalsIgnoreCase("")) {
                                try {
                                    ExifInterface ei = new ExifInterface(imageFilePath);
                                    orientation = ei.getAttributeInt(
                                            TAG_ORIENTATION,
                                            ORIENTATION_NORMAL);
                                } catch (IOException e) {
                                    AppDelegate.LogE(e);
                                }
                                capturedFile = new File(imageFilePath);
                                Bitmap OriginalPhoto = AppDelegate.decodeFile(capturedFile);
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                    default:
                                        break;

                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);
                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    AppDelegate.LogT("Gallery => " + android.os.Build.VERSION.SDK_INT + " <= " + Build.VERSION_CODES.LOLLIPOP_MR1);
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                                        cimg_user.setImageBitmap(OriginalPhoto);
                                    }
                                } else {
                                    if (imageUri != null)
                                        performCrop(imageUri);
                                    else
                                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                    }
                    break;
                case AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE:
                    AppDelegate.LogT("onActivityResult str_file_path = " + str_file_path + ", imageUri = " + imageUri);

                    //Get our saved file into a bitmap object:
                    try {
                        if (resultCode == Activity.RESULT_OK) {
                            int orientation = 0;
                            Uri uri = Uri.fromFile(new File(capturedFile.getAbsolutePath()));

                            BitmapFactory.Options o = new BitmapFactory.Options();
                            o.inJustDecodeBounds = true;
                            Bitmap OriginalPhoto = AppDelegate.decodeSampledBitmapFromFile(capturedFile.getAbsolutePath(), 1000, 700);
                            Log.d("OriginalPhoto", "OriginalPhoto is " + OriginalPhoto);
                            try {
                                ExifInterface ei = new ExifInterface(
                                        uri.getPath());

                                orientation = ei.getAttributeInt(
                                        ExifInterface.TAG_ORIENTATION,
                                        ExifInterface.ORIENTATION_NORMAL);

                                Log.d("image", "orientation is " + orientation);
                                Matrix matrix = new Matrix();

                                switch (orientation) {
                                    case ExifInterface.ORIENTATION_NORMAL:
                                        System.out.println("ORIENTATION_NORMAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                                        matrix.setScale(-1, 1);
                                        System.out.println("ORIENTATION_FLIP_HORIZONTAL" + orientation);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_180:
                                        matrix.setRotate(180);
                                        break;
                                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                                        matrix.setRotate(180);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSPOSE:
                                        matrix.setRotate(90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_90:
                                        matrix.setRotate(90);
                                        break;
                                    case ExifInterface.ORIENTATION_TRANSVERSE:
                                        matrix.setRotate(-90);
                                        matrix.postScale(-1, 1);
                                        break;
                                    case ExifInterface.ORIENTATION_ROTATE_270:
                                        matrix.setRotate(-90);
                                        break;
                                }
                                Log.d("OriginalPhoto", "after OriginalPhoto is " + OriginalPhoto);
                                if (OriginalPhoto == null) {
                                    Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                    return;
                                }

                                OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0, OriginalPhoto.getWidth(),
                                        OriginalPhoto.getHeight(), matrix, true);

                                Uri rotateduri = getImageUri(getActivity(), OriginalPhoto);
                                if (rotateduri != null) {
                                    if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
                                        performCrop(rotateduri);
                                    else {
                                        this.OriginalPhoto = OriginalPhoto;
                                        cimg_user.setImageBitmap(OriginalPhoto);
                                    }

                                } else {
                                    AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                                }

                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                                AppDelegate.showToast(getActivity(), "Failure delivery result, please try again later.");
                            }
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    break;
                case AppDelegate.PIC_CROP: {
                    AppDelegate.LogT("at onActivicTyResult cropimageUri = " + cropimageUri);
                    try {

                        if (data.getData() == null) {
                            OriginalPhoto = (Bitmap) data.getExtras().get("data");
                        } else {
                            OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        }

//                    cropimageUri = data.getData();
                        if (resultCode == Activity.RESULT_OK) {
                            if (OriginalPhoto != null) {
                                OriginalPhoto.recycle();
                                OriginalPhoto = null;
                            }
                            OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), cropimageUri);
                            AppDelegate.LogT("at onActivicTyResult selectedBitmap = " + OriginalPhoto);
                            if (OriginalPhoto == null) {
                                Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
                                return;
                            }
                            OriginalPhoto = Bitmap.createScaledBitmap(
                                    OriginalPhoto, 240, 240, true);
                            AppDelegate.LogT("at onActivicTyResult OriginalPhoto = " + OriginalPhoto);
                            cimg_user.setImageBitmap(AppDelegate.getRoundedCornerBitmap(OriginalPhoto, AppDelegate.convertdp(getActivity(), 100)));


                            capturedFile = new File(getNewFile());
                            FileOutputStream fOut = null;
                            try {
                                fOut = new FileOutputStream(capturedFile);
                            } catch (FileNotFoundException e) {
                                AppDelegate.LogE(e);
                            }
                            OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                            try {
                                fOut.flush();
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            try {
                                fOut.close();
                            } catch (IOException e) {
                                AppDelegate.LogE(e);
                            }
                            // getActivity().getContentResolver().delete(cropimageUri, null, null);
                        } else {
                            AppDelegate.LogE("at onActivicTyResult failed to crop");
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Some Error Occurred! Please Try Again!", Toast.LENGTH_LONG).show();
            AppDelegate.LogE(e);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    private void performCrop(Uri picUri) {
        try {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = getActivity().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("scale", true);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);

            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        } catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }
}
