package com.stux.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;

import com.google.android.gms.maps.MapsInitializer;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.MoviesDetailListAdapter;
import com.stux.AppDelegate;
import com.stux.Models.MovieTime;
import com.stux.Models.MoviesModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.LargeImageActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class MoviesDetailsFragment extends Fragment implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse {

    private TextView txt_c_movies_name, txt_c_genres, txt_c_viewer, txt_c_timer, txt_c_starring, txt_c_synopsis;
    private carbon.widget.ImageView img_c_movies_banner;
    private ImageView img_loading;
    private ScrollView scrollView;

    private ListView list_view;
    private MoviesDetailListAdapter listAdapter;

    private MoviesModel moviesModel;
    private Prefs prefs;
    private UserDataModel dataModel;

//    public ArrayList<String> arrayString = new ArrayList<>();
//    private android.widget.LinearLayout pager_indicator;
//    private ViewPager view_pager;
//    private ProductPagerAdapter mPagerAdapter;

    private Handler mHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.movies_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        moviesModel = getArguments().getParcelable(Tags.movies);
        initView(view);
        setValues();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                }
            }
        };
    }

    private void setValues() {
        if (moviesModel != null) {
            txt_c_movies_name.setText(moviesModel.title);
            txt_c_viewer.setText(moviesModel.total_movie_views);
            txt_c_genres.setText("GENRES: " + moviesModel.genre);
            txt_c_timer.setText(moviesModel.run_time + " ");
            txt_c_starring.setText("Starring: " + moviesModel.starring);
            txt_c_synopsis.setText(moviesModel.synopsis);

            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            Picasso.with(getActivity()).load(moviesModel.banner_image_thumb).into(img_c_movies_banner, new Callback() {
                @Override
                public void onSuccess() {
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    img_loading.setVisibility(View.GONE);
                }
            });
            img_c_movies_banner.setVisibility(View.VISIBLE);
        }
    }

    public static void setListViewHeight(Context mContext, ListView listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < itemCount; i++) {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            totalHeight += AppDelegate.dpToPix(mContext, 10);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Movies Detail");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);


        txt_c_movies_name = (TextView) view.findViewById(R.id.txt_c_movies_name);
        txt_c_genres = (TextView) view.findViewById(R.id.txt_c_genres);
        txt_c_viewer = (TextView) view.findViewById(R.id.txt_c_viewer);
        txt_c_timer = (TextView) view.findViewById(R.id.txt_c_timer);
        txt_c_starring = (TextView) view.findViewById(R.id.txt_c_starring);
        txt_c_synopsis = (TextView) view.findViewById(R.id.txt_c_synopsis);

        img_c_movies_banner = (carbon.widget.ImageView) view.findViewById(R.id.img_c_movies_banner);
        img_c_movies_banner.setOnClickListener(this);
        img_c_movies_banner.setOnClickListener(this);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        list_view = (ListView) view.findViewById(R.id.list_view);
        try {
            JSONArray jsonArray = new JSONArray(moviesModel.cinema_type_with_time);
            ArrayList<MovieTime> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                MovieTime movieTime = new MovieTime();
                movieTime.cinema_type = jsonArray.getJSONObject(i).getString(Tags.cinema_type);
                movieTime.cinema_time = jsonArray.getJSONObject(i).getString(Tags.cinema_time);
                arrayList.add(movieTime);
            }
            listAdapter = new MoviesDetailListAdapter(getActivity(), -1, arrayList, this);
            list_view.setAdapter(listAdapter);
            setListViewHeight(getActivity(), list_view, listAdapter);
            list_view.smoothScrollToPosition(0);
            list_view.setSelection(0);
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }


//        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);
//        if (arrayString.size() == 0)
//            if (AppDelegate.isValidString(dealModel.image_4_thumb)) {
//                arrayString.add(dealModel.image_1_thumb);
//                arrayString.add(dealModel.image_2_thumb);
//                arrayString.add(dealModel.image_3_thumb);
//                arrayString.add(dealModel.image_4_thumb);
//            } else if (AppDelegate.isValidString(dealModel.image_3_thumb)) {
//                arrayString.add(dealModel.image_1_thumb);
//                arrayString.add(dealModel.image_2_thumb);
//                arrayString.add(dealModel.image_3_thumb);
//            } else if (AppDelegate.isValidString(dealModel.image_2_thumb)) {
//                arrayString.add(dealModel.image_1_thumb);
//                arrayString.add(dealModel.image_2_thumb);
//            } else if (AppDelegate.isValidString(dealModel.image_1_thumb)) {
//                arrayString.add(dealModel.image_1_thumb);
//            } else {
////                arrayString.add("");
//            }
//        setUiPageViewController();
//        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
//        mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
//        view_pager.setAdapter(mPagerAdapter);
//        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                switchBannerPage(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.orange_radius_square);
        }
    }

    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;

//    private void setUiPageViewController() {
//        pager_indicator.removeAllViews();
//        dotsCount = arrayString.size();
//        if (dotsCount > 0) {
//            dots = new ImageView[dotsCount];
//            for (int i = 0; i < dotsCount; i++) {
//                dots[i] = new ImageView(getActivity());
//                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
//                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
//                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
//                    params = new LinearLayout.LayoutParams(15, 15);
//                    params.setMargins(7, 0, 7, 0);
//                } else
//                    params.setMargins(10, 0, 10, 0);
//                pager_indicator.addView(dots[i], params);
//            }
//            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.img_c_movies_banner:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Tags.image, moviesModel.banner_image);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                break;
        }
    }


    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image, moviesModel.banner_image);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GRAB_COUPONS)) {
            mHandler.sendEmptyMessage(11);
            parseGrabResult(result);
        }
    }

    private void parseGrabResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
