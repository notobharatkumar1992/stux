package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.stux.Adapters.DealListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.DealModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/27/2016.
 */
public class DealsListFragment extends Fragment implements OnReciveServerResponse {

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    private ArrayList<DealModel> dealArray = new ArrayList<>();
    private ListView list_view;
    private DealListAdapter mDealAdapter;
    private int dealCounter = 1, dealTotalPage = -1;
    private TextView txt_c_no_list;

    private boolean dealAsyncExcecuting = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.campus_list_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        if (dealArray.size() == 0)
            callCampusListAsync();
        else mHandler.sendEmptyMessage(3);
    }

    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, dealCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.DEALS_LIST,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 3) {
                    AppDelegate.LogT("mDealAdapter notified = " + dealArray.size());
                    txt_c_no_list.setVisibility(dealArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No deal available");
                    mDealAdapter.notifyDataSetChanged();
                    list_view.invalidate();
//                    mDealList.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            AppDelegate.setListViewHeight(getActivity(), mDealList, mDealAdapter, footerView2);
//                            mDealList.smoothScrollToPosition(0);
//                            mDealList.setSelection(0);
//                        }
//                    }, 1000);
                }
            }
        };
    }


    private int preLast;


    private void initView(View view) {
        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        mDealAdapter = new DealListAdapter(getActivity(), R.id.txt_line1, dealArray);
        list_view = (ListView) view.findViewById(R.id.list_view);
        list_view.setAdapter(mDealAdapter);
        list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                AppDelegate.LogT("onScroll => counter = " + dealCounter + ", totalPage = " + dealTotalPage);
                if (dealCounter <= dealTotalPage)
                    if (lastItem == totalItemCount) {
                        if (preLast != lastItem) { //to avoid multiple calls for last item
                            preLast = lastItem;
                            callCampusListAsync();
                        }
                    }
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AppDelegate.LogT("event_list_view onItemClick");
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.deal, dealArray.get(position));
                Fragment fragment = new DealDetailsFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
            }
        });

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.DEALS_LIST)) {
            if (dealCounter > 1) {
                dealAsyncExcecuting = false;
            }
            parseDealListResult(result);
        }
    }

    private void parseDealListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (jsonObject.has(Tags.nextPage)) {
                dealTotalPage = Integer.parseInt(jsonObject.getString(Tags.nextPage));
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    DealModel dealModel = new DealModel();
                    dealModel.id = object.getString(Tags.id);
                    dealModel.user_id = object.getString(Tags.user_id);
                    dealModel.deal_area = object.getString(Tags.deal_area);
                    dealModel.deal_catid = object.getString(Tags.deal_catid);
                    dealModel.institute_id = object.getString(Tags.institute_id);
                    dealModel.title = object.getString(Tags.title);
                    dealModel.details = object.getString(Tags.details);
                    dealModel.venue = object.getString(Tags.venue);
                    dealModel.price = object.getString(Tags.price);
                    dealModel.discount = object.getString(Tags.discount);
                    dealModel.discount_price = object.getString(Tags.discount_price);
                    dealModel.coupon_code = object.getString(Tags.coupon_code);
                    dealModel.product_category = object.getString(Tags.product_category);
                    dealModel.image_1 = object.getString(Tags.banner_image);
                    dealModel.image_1_thumb = object.getString(Tags.banner_image_thumb);

                    dealModel.created = object.getString(Tags.created);
                    dealArray.add(dealModel);
                }
            } else {
                dealTotalPage = 0;
            }
            dealCounter++;
            mHandler.sendEmptyMessage(3);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }
}
