package com.stux.fragments;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.EventModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.activities.LargeImageActivity;
import com.stux.constants.Tags;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 06/03/2016.
 */
public class EventDetailFragment extends Fragment implements View.OnClickListener {

    MapView mapview;
    private CircleImageView cimg_event;
    private ImageView img_c_event_banner;
    private android.widget.ImageView img_loading;
    private TextView txt_c_event_name, txt_c_campus, txt_c_address_1, txt_c_address_2, txt_c_alarm, txt_c_tickets, txt_c_contact, txt_c_theme, txt_c_description, txt_c_views;
    private RelativeLayout rl_c_img_layout;
    private ScrollView scrollView;
    private Handler mHandler;
    private GoogleMap map_business;
    //    private SupportMapFragment fragment;
//    private WorkaroundMapFragment fragment;
    private EventModel eventModel;

    private View view_banner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.event_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        eventModel = getArguments().getParcelable(Tags.EVENT);
        mapview = (MapView) view.findViewById(R.id.mapview);
        mapview.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map_business = mapview.getMap();
        initView(view);
        setHandler();
        setValus();
    }

    private void setValus() {
        if (eventModel != null) {
            if (AppDelegate.isValidString(eventModel.user_image)) {
                img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                frameAnimation.setCallback(img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                Picasso.with(getActivity()).load(eventModel.user_image).into(cimg_event, new Callback() {
                    @Override
                    public void onSuccess() {
                        img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        img_loading.setVisibility(View.GONE);
                    }
                });
            }
            Picasso.with(getActivity()).load(eventModel.banner_image_thumb).into(img_c_event_banner);
            txt_c_event_name.setText(eventModel.event_name + " at " + eventModel.location);
            txt_c_address_1.setText(eventModel.location);
            txt_c_address_2.setText(eventModel.venue);

            txt_c_views.setText("Views: ");

            if (AppDelegate.isValidString(eventModel.location_type) && Integer.parseInt(eventModel.location_type) == 1) {
                txt_c_campus.setText("On Campus");
            } else {
                txt_c_campus.setText("Off Campus");
            }

            String time = eventModel.created.substring(0, eventModel.created.lastIndexOf("+"));
            try {
//            AppDelegate.LogT("time before = " + time); 2016-06-27T06:32
                time = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                txt_c_alarm.setText(time);
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            txt_c_tickets.setText("Tickets : " + eventModel.tickets);
            txt_c_contact.setText(eventModel.contact_no);
            txt_c_theme.setText("Theme: " + eventModel.theme);
            txt_c_description.setText(Html.fromHtml("<b>Description : </b>" + (AppDelegate.isValidString(eventModel.description) ? eventModel.description : "") + ""));

            if (map_business != null && AppDelegate.isValidString(eventModel.latitude) && AppDelegate.isValidString(eventModel.longitude)) {
                map_business.addMarker(AppDelegate.getMarkerOptions(getActivity(), new LatLng(Double.parseDouble(eventModel.latitude), Double.parseDouble(eventModel.longitude))));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(Double.parseDouble(eventModel.latitude), Double.parseDouble(eventModel.longitude)))
                        .zoom(14).build();
                //Zoom in and animate the camera.
                map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                }
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        fragment = SupportMapFragment.newInstance();
//        getChildFragmentManager().beginTransaction()
//                .replace(R.id.map_Frame, fragment, "MAP1").addToBackStack(null)
//                .commit();
//        new Handler().postDelayed(new Runnable() {
//                                      @Override
//                                      public void run() {
        showMap();
//                                      }
//                                  }
//                , 1500);
    }

    private void showMap() {
//        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        AppDelegate.setMyLocationBottomRightButton(getActivity(), fragment);

        if (map_business != null && AppDelegate.isValidString(eventModel.latitude) && AppDelegate.isValidString(eventModel.longitude)) {
            AppDelegate.LogT("location showing => " + eventModel.latitude + "," + eventModel.longitude);
            map_business.addMarker(AppDelegate.getMarkerOptions(getActivity(), new LatLng(Double.parseDouble(eventModel.latitude), Double.parseDouble(eventModel.longitude))));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(eventModel.latitude), Double.parseDouble(eventModel.longitude)))
                    .zoom(14).build();
            //Zoom in and animate the camera.
            map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private void initView(View view) {
        cimg_event = (CircleImageView) view.findViewById(R.id.cimg_event);
        img_c_event_banner = (ImageView) view.findViewById(R.id.img_c_event_banner);

        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Event Details");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);

        txt_c_event_name = (TextView) view.findViewById(R.id.txt_c_event_name);
        txt_c_campus = (TextView) view.findViewById(R.id.txt_c_campus);
        txt_c_address_1 = (TextView) view.findViewById(R.id.txt_c_address_1);
        txt_c_address_2 = (TextView) view.findViewById(R.id.txt_c_address_2);
        txt_c_alarm = (TextView) view.findViewById(R.id.txt_c_alarm);
        txt_c_tickets = (TextView) view.findViewById(R.id.txt_c_tickets);
        txt_c_contact = (TextView) view.findViewById(R.id.txt_c_contact);
        txt_c_theme = (TextView) view.findViewById(R.id.txt_c_theme);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        txt_c_views = (TextView) view.findViewById(R.id.txt_c_views);

        rl_c_img_layout = (RelativeLayout) view.findViewById(R.id.rl_c_img_layout);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);

        img_loading = (android.widget.ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        rl_c_img_layout.getLayoutParams().height = AppDelegate.getDeviceWith(getActivity()) / 2;
        rl_c_img_layout.requestLayout();

        view_banner = (View) view.findViewById(R.id.view_banner);
        view_banner.setOnClickListener(this);
//        map_business = ((WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map)).getMap();
//        ((WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
//            @Override
//            public void onTouch() {
//                scrollView.requestDisallowInterceptTouchEvent(true);
//            }
//        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;

            case R.id.view_banner:
                if (AppDelegate.haveNetworkConnection(getActivity())) {
                    Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Tags.image, eventModel.banner_image);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                }
                break;
        }
    }
}
